{foreach $tags as $tag}
<a class="tag tag-{$tag.Count}" title="{$tag.Name}" href="{$tag.Url}">{$tag.Name}<span class="tag-count is-hidden"> ({$tag.Count})</span></a>
{/foreach}
