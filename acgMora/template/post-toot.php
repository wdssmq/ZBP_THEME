{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
// $description = preg_replace('/[\r\n\s]+/', ' ', trim(SubStrUTF8(TransferHTML($article->Intro,'[nohtml]'),149)).'...');
$time = acgMora_Time($article->PostTime);
// var_dump($article->GetData());
{/php}
<div class="post cate{$article.Category.ID} auth{$article.Author.ID} box">
  <div class="media post-head">
    <figure class="media-left">
      <p class="image is-48x48">
        <img class="is-rounded" src="{$article.Author.Avatar}" alt="{$article.Author.StaticName}头像">
      </p>
    </figure>
    <div class="media-content">
      <p class="post-author">
        <a class="a-color" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a>
      </p>
      <p class="post-meta is-size-8">
        <a class="post-time a-pink" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$time}</a>
      </p>
    </div>
  </div>
  <div class="intro">
    {$article->Content}
    <p>{foreach $article.Tags as $tag}<a class="a-tag" href="{$tag.Url}" title="{$tag.Name}" rel="tag">#{$tag.Name}</a> {/foreach}<span title="请为本文设置合适的标签" class="c-help a-tag">#设置Tag是个好习惯</span></p>
  </div>
  <p class="post-meta meta-2 is-size-8">
    <a class="meta-item a-pink" href="{$article.Url}#comment" title="留言">
      <i class="icon icon-comment"></i> {$article.CommNums}
    </a>
    <span class="meta-item a-pink c-pointer">
      <i class="icon icon-eye"></i> {$article.ViewNums}
    </span>
  </p>
</div>
