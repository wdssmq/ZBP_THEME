<nav class="navbar box is-paddingless">
  <div class="container is-widescreen">
    <div class="navbar-brand">
      <h1 class="title none-border navbar-item"><a href="{$host}" title="{$name}"><span class="is-sr-only">{$name}</span><img src="{$host}zb_users/theme/acgMora/usr/logo.png" alt="{$name}"></a></h1>
      <div class="navbar-burger" data-target=".navbar-menu">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <ul class="navbar-menu">{module:navbar}</ul>
  </div>
</nav>
