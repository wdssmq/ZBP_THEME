<!DOCTYPE html>
<html lang="{$language}">

<head>
  {template:header}
</head>

<body class="{$type}">
  {template:hero}
  <div class="container is-widescreen">
    <div class="columns is-desktop main">
      <!-- Middle -->
      <div class="column is-three-fifths-desktop order-2">
        <div class="tabs-or-bread box is-paddinglessY">{template:n-tabs}</div>
        {if $article.Type==ZC_POST_TYPE_ARTICLE}
        {template:post-single}
        {else}
        {template:post-page}
        {/if}
      </div>
      <!-- Left -->
      <div class="column is-one-fifth-desktop order-1">{template:m-usr}{template:sidebar2}</div>
      <!-- Right -->
      <div class="column is-one-fifth-desktop order-3">{template:sidebar}</div>
    </div>
  </div>
  {template:footer}
</body>

</html>
