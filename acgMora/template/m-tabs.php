{if $type == 'index'}
<div class="tabs is-small">
  <ul>
    <li class="is-active"><a href="{$host}" title="全部">全部</a></li>
    <li class="is-toot"><a href="-tootURL-" title="说说">说说</a></li>
    {module:acgMora-tabs}
  </ul>
</div>
{elseif $type=='article'}
<nav class="breadcrumb is-small">
  <ul>
    <li><a href="{$host}" title="首页">首页</a></li>
    <li><a href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a></li>
    <li class="is-active"><a href="{$article.Url}" title="{$article.Title}">{$article.Title}</a></li>
  </ul>
</nav>
{else}
<nav class="breadcrumb is-small">
  <ul>
    <li><a href="{$host}" title="首页">首页</a></li>
    <li class="is-active"><a href="{$zbp.fullcurrenturl}" title="{$title}">{$title}</a></li>
  </ul>
</nav>
{/if}
