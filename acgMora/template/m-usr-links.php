<ul>
  <!-- QQ -->
  {if $zbp->Config('acgMora')->HasKey('QQ') && !empty($zbp->Config('acgMora')->QQ)}
  <li><i class="iconfont icon icon-QQ2" style="color: rgb(83, 136, 255);"></i><a target="_blank" href="tencent://message/?uin={$zbp.Config('acgMora').QQ}" rel="nofollow" title="{$zbp.Config('acgMora').QQ}">{$zbp.Config('acgMora').QQ}</a></li>
  {/if}
  <!-- QQ群 -->
  {if $zbp->Config('acgMora')->HasKey('QQGroup') && !empty($zbp->Config('acgMora')->QQGroup)}
  <li><i class="iconfont icon icon-QQqun" style="color: rgb(1, 179, 245);"></i><a target="_blank" href="{$zbp.Config('acgMora').QQGroup}" title="{$zbp.Config('acgMora').QQGroupT}" rel="nofollow">{$zbp.Config('acgMora').QQGroupT}</a></li>
  {/if}
  <!-- Bilibili -->
  {if $zbp->Config('acgMora')->HasKey('Bili') && !empty($zbp->Config('acgMora')->Bili)}
  <li><i class="iconfont icon icon-bilibili" style="color: rgb(18, 150, 219)"></i><a target="_blank" href="https://space.bilibili.com/{$zbp.Config('acgMora').Bili}" title="{$zbp.Config('acgMora').BiliT}" rel="nofollow">{$zbp.Config('acgMora').BiliT}</a></li>
  {/if}
  <!-- 知乎 -->
  {if $zbp->Config('acgMora')->HasKey('Zhihu') && !empty($zbp->Config('acgMora')->Zhihu)}
  <li><i class="iconfont icon icon-zhihu" style="color: rgb(15, 132, 253)"></i><a target="_blank" href="https://www.zhihu.com/people/{$zbp.Config('acgMora').Zhihu}" title="{$zbp.Config('acgMora').ZhihuT}" rel="nofollow">{$zbp.Config('acgMora').ZhihuT}</a></li>
  {/if}
  <!-- Git -->
  {if $zbp->Config('acgMora')->HasKey('Git') && !empty($zbp->Config('acgMora')->Git)}
  <li><i class="iconfont icon icon-Git"></i><a target="_blank" href="https://github.com/{$zbp.Config('acgMora').Git}" title="{$zbp.Config('acgMora').GitT}" rel="nofollow">{$zbp.Config('acgMora').Git}</a></li>
  {/if}
</ul>
