<!DOCTYPE html>
<html lang="{$language}">

<head>
{template:header}
</head>

<body class="{$type}">
  {template:hero}
  <div class="container is-widescreen">
    <div class="columns is-desktop main">
      <!-- Middle -->
      <div class="column is-three-fifths-desktop order-2">
        <div class="tabs-or-bread box is-paddinglessY">{template:n-tabs}</div>
        {if count($articles) == 0}
        <div class="box has-text-centered">
          <h2 class="is-size-4-desktop">
            <span>当前列表为空 - {$name}</span>
          </h2>
          <p class="has-text-centered">
            <img src="{$host}zb_users/theme/acgMora/var/moe-fuuko.gif" alt="moe-fuuko" width="236" height="236" />
          </p>
          <h3 class="is-size-5-desktop">
            <a class="a-ccc" href="{$host}" title="返回首页">返回首页</a>
            <span>或者</span>
            <a class="a-ccc" href="javascript:;" onclick="history.go(-1);" title="返回上页">返回上页</a>
          </h3>
        </div>
        {else}
        {foreach $articles as $article}
        {if $article.Type == ZC_POST_TYPE_TWEET || $article.Category.ID == $zbp.Config("acgMora").tootID}
        {template:post-toot}
        {else}
        {template:post-bulti}
        {/if}
        {/foreach}
        {template:pagebar}
        {/if}
      </div>
      <!-- Left -->
      <div class="column is-one-fifth-desktop order-1">{template:m-usr}{template:sidebar2}</div>
      <!-- Right -->
      <div class="column is-one-fifth-desktop order-3">{template:sidebar}</div>
    </div>
  </div>
  {template:footer}
</body>

</html>
