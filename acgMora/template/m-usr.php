{php}<?php
      if ($zbp->user->ID > 0) {
        $curUser = $zbp->user;
      } else {
        $curUser = $zbp->members[1];
      }
      if ($zbp->Config("acgMora")->tootOn && $zbp->Config("acgMora")->HasKey('tootID')) {
        $tooCount = $zbp->categories[$zbp->Config("acgMora")->tootID]->Count;
      } else {
        $tooCount = 0;
      }
      // var_dump($curUser->GetData());
      ?>{/php}
<div class="user-panel box">
  <div class="user-pic"></div>
  <h3 class="user-name">{$curUser.StaticName}</h3>
  <div class="user-info level is-size-7">
    <a class="level-item has-text-centered" href="javascript:;" title="文章数"><span class="num">{$curUser.Articles}</span><span class="text">文章</span></a>
    <a class="level-item has-text-centered" href="javascript:;" title="说说"><span class="num">{$tooCount}</span><span class="text">说说</span></a>
    <a class="level-item has-text-centered" href="javascript:;" title="评论数"><span class="num">{$curUser.Comments}</span><span class="text">评论</span></a>
  </div>
  <img src="{$curUser.Avatar}" alt="{$curUser.StaticName}" class="user-head">
</div>

<div class="user-about box">
    {template:n-usr-links}
</div>
