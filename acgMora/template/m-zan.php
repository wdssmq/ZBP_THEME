<!-- <link rel="stylesheet" href="//at.alicdn.com/t/font_1251728_rb50gmtmg3d.css"> -->
<div class="box post-tool">
  <div class="act-bar level">
    <!-- <a class="act level-item like" data-target="like" data-cid="{$article.ID}"><i class="iconfont icon icon-zan"></i> 点赞<span class="likes-num">7</span></a> -->
    <a class="act level-item share" data-target="share" title="分享"><i class="iconfont icon icon-share1"></i> 分享本文</a>
    <a class="act level-item donate" data-target="donate" title="打赏"><i class="iconfont icon icon-creditcard"></i> 打赏作者</a>
  </div>
</div>
{php}
$moraTitle = str_replace(" ","%20",$article->Title);
$summary = preg_replace('/[\r\n\s\[\]]+/', '%20', trim(SubStrUTF8(TransferHTML($article->Intro,'[nohtml]'),149)));
$qrList = acgMora_QR();
{/php}
<div class="target-bar has-text-centered box">
  <div class="target share">
    <div class="modal-header">
      <h3>分享这篇文章</h3>
    </div>
    <div>
      <a title="复制链接" href="javascript:;">
        <i class="iconfont icon icon-fuzhilianjie" style="color: #7a7a7a"></i>
      </a>
      <a title="QQ" rel="nofollow" href="https://connect.qq.com/widget/shareqq/index.html?url={$article.Url}&amp;title={$moraTitle}&amp;pics={$article->FistImg}&amp;summary={$summary}" target="_blank">
        <!-- <i class="iconfont icon icon-QQ2" style="background: #3498db"></i> -->
        <i class="iconfont icon icon-QQ" style="color: #3498db"></i>
      </a>
      <a title="新浪微博" rel="nofollow" href="https://service.weibo.com/share/share.php?url={$article.Url}&amp;title={$moraTitle}&amp;pic={$article->FistImg}&amp;loginRnd" target="_blank">
        <i class="iconfont icon icon-weibo" style="color: #db1b17"></i>
      </a>
      <a title="微信" class="wechat-link" href="javascript:;"><i class="iconfont icon icon-weixin1" style="color: #40db4f"></i></a>
      <a title="QQ 空间" rel="nofollow" href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url={$article.Url}&amp;title={$moraTitle}&amp;desc=&amp;summary={$summary}&amp;site=&amp;pics={$article->FistImg}" target="_blank">
        <i class="iconfont icon icon-kongjian" style="color: #ffec00"></i>
      </a>
    </div>
    <div class="modal-footer">
    </div>
  </div>
  <!-- /target share -->
  <div class="target donate">
    <div class="modal-header">
      <h3>请我喝杯茶吧</h3>
    </div>
    <div class="level">
      <div class="level-item has-text-centered">
        <div>
          <img src="{$qrList['ali']}" alt="支付宝">
          <p>支付宝</p>
        </div>
      </div>
      <div class="level-item has-text-centered">
        <div>
          <img src="{$qrList['wx']}" alt="微信支付">
          <p>微信</p>
        </div>
      </div>
      <div class="level-item has-text-centered">
        <div>
          <img src="{$qrList['qq']}" alt="QQ 钱包">
          <p>QQ 钱包</p>
        </div>
      </div>
    </div>
    <div class="modal-footer is-flex is-align-items-center is-justify-content-center">
      <div class="switch-slide">
        <input id="skip_donate_auto" type="checkbox" hidden>
        <label for="skip_donate_auto" class="switch-slide-label"></label>
      </div>
      <span>
        点亮此处不再自动提示，感谢您的支持！
      </span>
    </div>
  </div>
  <!-- /target donate -->
</div>
<!-- /target-bar -->
<div class="wechat-qrcode">
  <h4>分享到微信</h4>
  <div class="qrcode" title="{$article.Url}">
  </div>
  <div class="wechat-help">
    <p>扫描二维码</p>
    <p>可在微信查看或分享至朋友圈。</p>
  </div>
</div>
