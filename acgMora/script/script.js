/* jshint esversion: 6 */
$(function() {
  "use strict";
  $(".navbar-menu > li").each(function() {
    $(this).addClass("navbar-item");
  });
  $(".navbar-burger").click(function() {
    let target = $(this).data("target");

    $(`${target}`).toggleClass("is-active");
    $(this).toggleClass("is-active");
  });

  $("div.post-body a").click(function() {
    let href = $(this).attr("href");

    if (
      href.indexOf(location.host) === -1 &&
      href.indexOf("javascript:") === -1
    ) {
      $(this).attr("target", "_blank");
    }
  });

  $(".post-bulti")
    .mouseenter(function() {
      const title = $(this).data("title");

      $(this).addClass("c-pointer").attr("title", title);
    })
    .on("click", function(e) {
      if (e.target.nodeName !== "A") {
        const link = $(this).data("link");

        console.log(e);
        location.href = link;
        return false;
      }
    });

  $(".post-body iframe").each(function() {
    $(this).css({ width: "100%" });
    const width = $(this).width(),
      height = width / (640 / 512);

    $(this).css({ height: `${height}px` });

    // console.log(width,height);
  });

  const $sidebar = $(".column.order-3"),
    $mod = $(".column.order-3 .mod:last-of-type"),
    mapCache = {
      min: 0,
      max: 0,
      height: 0,
      scrollt: 0,
    };

  $(window).scroll(function() {
    if ($mod.length === 0) {
      return;
    }

    mapCache.scrollt = $(window).scrollTop();
    if ($(window).width() < 768) {
      return false;
    }

    if (mapCache.min === 0 || mapCache.height !== $sidebar.outerHeight()) {
      mapCache.height = $sidebar.outerHeight();
      mapCache.min = $mod.offset().top;
      mapCache.max =
        $sidebar.offset().top + mapCache.height - $mod.outerHeight();
    }

    // console.log(mapCache, scrollt);

    if (mapCache.scrollt > mapCache.max - 137) {
      return;
    }

    if (mapCache.scrollt > mapCache.min - 20) {
      $mod.stop().animate({
        marginTop: mapCache.scrollt - mapCache.min + 40,
      });
      $(".backTop").fadeIn();
    } else {
      $mod.stop().animate({
        marginTop: 0,
      });
      $(".backTop").fadeOut();
    }

    return false;
  });
  $(".backTop").click(function() {
    $("html,body")
      .animate(
        {
          scrollTop: "0px",
        },
        500,
      )
      .fadeIn();
  });
  function fnSrollToEL(target) {
    // console.log(target);
    $("body,html").animate(
      {
        scrollTop: $(target).offset().top - 59,
      },
      500,
    );
  }

  // 评论重写
  window.zbp.plugin.unbind("comment.reply.start", "system-default");
  window.zbp.plugin.on("comment.reply.start", "MizuJS", function(id) {
    const _this = this;

    this.$("#inpRevID").val(id);
    this.$("#cancel-reply").unbind("click");
    this.$("#cancel-reply")
      .show()
      .bind("click", function() {
        _this.$("#inpRevID").val(0);
        _this.$(this).hide();
        fnSrollToEL("#cmt" + id);

        // window.location.hash = '#cmt' + id;
        return false;
      });
    fnSrollToEL("#comment");

    // window.location.hash = '#comment';
  });
  window.zbp.plugin.unbind("comment.post.success", "system-default");
  window.zbp.plugin.on(
    "comment.post.success",
    "MizuJS",
    function(formData, retString) {
      // console.log(arguments);
      const objSubmit = $("#inpId").parent("form").find(":submit"),
        data = retString;

      objSubmit
        .removeClass("loading")
        .removeAttr("disabled")
        .val(objSubmit.data("orig"));

      if (data.err.code !== 0) {
        alert(data.err.msg);
        throw "ERROR - " + data.err.msg;
      }

      if (formData.replyid === "0") {
        this.$(data.data.html).insertAfter("#AjaxCommentBegin");
      } else {
        this.$(data.data.html).insertAfter("#MizuComment" + formData.replyid);
      }

      fnSrollToEL("#cmt" + data.data.ID);

      // location.hash = '#cmt' + data.data.ID;
      this.$("#txaArticle").val("");
      if (typeof this.userinfo.readFromHtml === "function") {
        this.userinfo.readFromHtml();
      } else {
        this.userinfo.saveFromHtml();
        this.userinfo.save();
      }
    },
  );
  window.zbp.plugin.on("comment.got", "MizuJS", function() {
    fnSrollToEL("#AjaxCommentBegin");
  });
});
;/* jshint esversion: 6 */
$(function() {
  // 向页面添加背景蒙版
  const $modalBackdrop = $("<div class=\"modal-backdrop fade\"></div>");
  $("body").append($modalBackdrop);

  const fnModalBackdropToggle = function(action) {
    if (action === "show") {
      $modalBackdrop.fadeIn("fast").addClass("in");
    } else {
      $modalBackdrop.removeClass("in").fadeOut("slow");
    }
  };

  const fnModal = function(action, $target = null) {
    if (action === "show" && $target.length > 0) {
      fnModalBackdropToggle("show");
      $(".target-bar").addClass("active").removeClass("remove");
      $target.addClass("active").siblings().removeClass("active");
    } else {
      fnModalBackdropToggle("hide");
      $(".target-bar").addClass("remove");
    }
  };

  $modalBackdrop.click(function() {
    fnModal("hide");
  });

  // act-bar 按钮点击事件
  $(".act-bar a.act").click(function() {
    let target = $(this).data("target");
    let $target = $(`.target.${target}`);
    if ("like" === target) {
      return;
    }
    $(".wechat-qrcode").addClass("remove");
    fnModal("show", $target);
  });

  $(".target.share a")
    .mouseenter(function() {
      $(this).find("i").addClass("fadeInDown animated");
    })
    .mouseleave(function() {
      $(this).find("i").removeClass("fadeInDown animated");
    });

  $(".wechat-link").click(function() {
    if (!$(this).data("done")) {
      $(".wechat-qrcode").addClass("active").removeClass("remove");
      $(this).data("done", 1);
      let $qr = $(".wechat-qrcode .qrcode");
      let url = $qr.attr("title");

      $qr.qrcode({
        render: "canvas",
        size: 137,
        minVersion: 1,
        ecLevel: "M",
        text: url,
      });
    } else {
      $(".wechat-qrcode").toggleClass("remove");
    }
  });

  // 自动弹出赞助提示
  let intVisit = parseInt(zbp.cookie.get("MizuJS_Visit")) || 1;
  let bolSkipDSAuto = zbp.cookie.get("MizuJS_SkipDSAuto") === "true";
  let isBoxShowed = false;
  // console.log(intVisit, isBoxShowed, bolSkipDSAuto);
  const fnSaveCookie = function() {
    zbp.cookie.set("MizuJS_Visit", intVisit, 365);
    zbp.cookie.set("MizuJS_SkipDSAuto", bolSkipDSAuto ? "true" : "false", 365);
  };

  // 切换回窗口时自动弹出赞助提示，只弹一次
  $(window).focus(function() {
    // console.log(intVisit, isBoxShowed, bolSkipDSAuto);
    if (isBoxShowed) {
      return;
    }
    if (intVisit % 4 === 0 && !bolSkipDSAuto) {
      fnModal("show", $(".target.donate"));
      isBoxShowed = true;
    }
    intVisit += 1;
  });

  // 每次访问 +1 并保存
  intVisit += 1;
  fnSaveCookie();

  // 设置当前的开关状态
  $("#skip_donate_auto").prop("checked", bolSkipDSAuto);
  // 开关状态改变时保存
  $("#skip_donate_auto").on("change", function() {
    bolSkipDSAuto = $(this).is(":checked");
    fnSaveCookie();
  });

});
