
import browsersync from 'rollup-plugin-browsersync'
import copy from 'rollup-plugin-copy'
import postcss from "rollup-plugin-postcss";
import resolve from "@rollup/plugin-node-resolve";

// 获取当前及父级目录
const curPath = process.cwd();
const parentPath = curPath.replace(/\/[^/]+$/, "");
// 父级文件夹名
const parentName = parentPath.replace(/.*\//, "");
// 产物复制到的目录
const distJS = `${parentPath}/script/`;
const distCSS = `${parentPath}/style/`;

const defConfig = {
  input: `src/main.js`,
  output: {
    file: "dist/script.js",
    format: "esm",
    banner: "/* eslint-disable */\n",
    // sourcemap: true,
  },
  plugins: [
    postcss({
      // 该路径相对于上边的 output.file
      extract: `${parentName}.css`,
    }),
    copy({
      targets: [
        {
          src: "dist/script.js",
          dest: distJS,
        },
        {
          src: `dist/${parentName}.css`,
          dest: distCSS,
        },
      ]
    }),
    // resolve(),
  ],
};

if (process.env.NODE_ENV === "dev") {
  defConfig.plugins.push(
    browsersync({
      proxy: "http://127.0.0.1:8081/",
      files: [
        `${parentPath}/**/*.php`,
        `${parentPath}/style/**/*.css`,
        `${parentPath}/script/**/*.js`,
      ],
    }),
  );
}

export default defConfig;
