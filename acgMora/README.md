## 投喂支持

爱发电：[https://afdian.com/a/wdssmq](https://afdian.com/a/wdssmq "沉冰浮水正在创作和 z-blog 相关或无关的各种有用或没用的代码 | 爱发电")

哔哩哔哩：[https://space.bilibili.com/44744006/video](https://space.bilibili.com/44744006/video "沉冰浮水的个人空间\_哔哩哔哩\_bilibili")「投币或充电」「[大会员卡券领取 - bilibili](https://account.bilibili.com/account/big/myPackage "大会员卡券领取 - bilibili")」

RSS 订阅：[https://feed.wdssmq.com/](https://feed.wdssmq.com/ "沉冰浮水博客的 RSS 订阅地址") 「[「言说」RSS 是一种态度！！](https://www.wdssmq.com/post/20201231613.html "「言说」RSS 是一种态度！！")」

在更多平台关注我：[https://www.wdssmq.com/guestbook.html#其他出没站点和信息](https://www.wdssmq.com/guestbook.html#%E5%85%B6%E4%BB%96%E5%87%BA%E6%B2%A1%E5%9C%B0%E7%82%B9%E5%92%8C%E4%BF%A1%E6%81%AF "在更多平台关注我")

更多「小代码」：[https://cn.bing.com/search?q=小代码+沉冰浮水](https://cn.bing.com/search?q=%E5%B0%8F%E4%BB%A3%E7%A0%81+%E6%B2%89%E5%86%B0%E6%B5%AE%E6%B0%B4 "小代码 沉冰浮水 - 必应搜索")

<!-- ##################################### -->

## 说明

目前自己在用的主题：

[https://www.wdssmq.com/](<https://www.wdssmq.com/> "沉冰浮水_置百丈玄冰而崩裂，掷须臾池水而漂摇。")


## 更新记录

2024-05-18：用 PurgeCSS 干掉了 80% 体积的 CSS； **所以有样式问题请反馈**；

2024-05-17：模板内文本的盘古之白；相关文章后的时间在手机端隐藏；

2023-11-08：社交平台链接可选；首次启用时为二维码设置占位图；

2023-08-15：自建模块名大小写问题；

2022-04-20：赞赏弹出逻辑；

2022-04-15：CSS z-index；

2022-04-14：赞赏信息自动弹出；「后台需要加个开关？」

2021-04-28：iframe 高度错误；

2021-04-26：按文章修改时间排序；

2021-03-16：配合评论 RSS 插件更新；

2021-01-01：文章内 iframe 自适应；[真野惠里菜--お愿いだから（Close-up Ver）](<https://www.wdssmq.com/post/ZhenYeHuiLi-Yuan-Close-up-Ver.html> "真野惠里菜--お愿いだから（Close-up Ver）_奇趣网文_沉冰浮水")

2020-12-23：文章列表显示 Tag；

2020-12-22：搜索改为 list；

2020-12-20：评论 JS 更新；

2020-11-23：修了两个手机端的 flex 样式问题；

2020-09-06：文章下的版权声明；

2020-07-17：JS 升级；

2020-05-20：说说模板头像属性，字号调整；

2020-05-07：HTML 规范；

2020-05-01：JS 侧栏跟随改进；侧栏分类模块增加 title；

2020-03-19：link[rel="canonical"]设置；

2020-02-12：手机端效果调整，CSS 缓存应对；

2020-02-11：集成分享和打赏功能；

2020-01-02：说说相关错误修复；

2019-11-25：Logo 细节等；

2019-11-24：字体图标调整；

2019-11-23：增加 QQ、B 站等平台信息填写及展示

## 截图

![001.png](<https://app.zblogcn.com/zb_users/upload/2019/11/201911041572859177654841.png> "001.png")
