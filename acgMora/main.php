<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action = 'root';
if (!$zbp->CheckRights($action)) {
  $zbp->ShowError(6);
  die();
}
if (!$zbp->CheckPlugin('acgMora')) {
  $zbp->ShowError(48);
  die();
}
$act = GetVars('act', 'GET');
$suc = GetVars('suc', 'GET');
if (GetVars('act', 'GET') == 'save') {
  CheckIsRefererValid();
  foreach ($_POST as $key => $val) {
    $zbp->Config('acgMora')->$key = trim($val);
  }
  $extList = "png|jpg|jpeg|ico|webp";
  foreach ($_FILES as $key => $value) {
    if ($_FILES[$key]['error'] === 0) {
      $ext = GetFileExt($_FILES[$key]['name']);
      $file = acgMora_Path("u-{$key}");
      if (!HasNameInString($extList, $ext) || !is_file($file)) {
        continue;
      }
      if (is_uploaded_file($_FILES[$key]['tmp_name'])) {
        move_uploaded_file($_FILES[$key]['tmp_name'], $file);
      }
    }
  }
  if (!$zbp->Config('acgMora')->tootOn) {
    $zbp->Config("acgMora")->tootID = 99;
  }
  $zbp->SaveConfig('acgMora');
  $zbp->BuildTemplate();
  $zbp->SetHint('good');
  Redirect('./main.php' . ($suc == null ? '' : '?act=$suc'));
}
InstallPlugin_acgMora();
$blogtitle = '这就是咸鱼吗？';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
$logo = acgMora_Path("u-logo", "host") . "?" . time();
$favicon = acgMora_Path("u-fav", "host") . "?" . time();
$qrImgList = acgMora_QR();
?>
<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle; ?></div>
  <div class="SubMenu">
    <a href="main.php" title="首页"><span class="m-left m-now">首页</span></a>
    <?php require "about.php"; ?>
  </div>
  <div id="divMain2">
    <form action="<?php echo BuildSafeURL("main.php?act=save"); ?>" method="post" enctype="multipart/form-data">
      <table width="100%" class="tableBorder">
        <tr>
          <th width="10%">项目</th>
          <th>内容</th>
          <th width="45%">说明</th>
        </tr>
        <tr>
          <td>说说分类</td>
          <td><select style="width:180px;" name="tootID">
              <?php echo OutputOptionItemsOfCategories($zbp->Config("acgMora")->tootID); ?>
            </select> <input type="text" id="tootOn" name="tootOn" class="checkbox" value="<?php echo $zbp->Config("acgMora")->tootOn; ?>"></td>
          <td>指定为说说的分类</td>
        </tr>
        <tr>
          <td>logo</td>
          <td>
            <input type="file" name="logo" size="60">&nbsp;&nbsp;<img src="<?php echo $logo; ?>" alt="logo" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
          </td>
          <td>建议高度 100px，宽度不限;</td>
        </tr>
        <tr>
          <td>favicon.ico</td>
          <td>
            <input type="file" name="fav" size="60">&nbsp;&nbsp;<img src="<?php echo $favicon; ?>" alt="favicon" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
          </td>
          <td><b>注意：如果博客根目录下存在 favicon.ico 则此处上传无效</b></td>
        </tr>
        <tr>
          <td>打赏二维码</td>
          <td>
            微信：<input type="file" name="qr-wx" size="60">&nbsp;&nbsp;<img src="<?php echo $qrImgList["wx"]; ?>" alt="微信" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;"><br>
            QQ 钱包：<input type="file" name="qr-qq" size="60">&nbsp;&nbsp;<img src="<?php echo $qrImgList["qq"]; ?>" alt="QQ" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;"><br>
            支付宝：<input type="file" name="qr-ali" size="60">&nbsp;&nbsp;<img src="<?php echo $qrImgList["ali"]; ?>" alt="支付宝" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
          </td>
          <td></td>
        </tr>
        <tr>
        <tr>
          <td>关键词</td>
          <td>
            <?php
            ZbpForm::text("keywords", str_replace("，", ",", $zbp->Config("acgMora")->keywords), "89%");
            ?>
          </td>
          <td></td>
        </tr>
        <tr>
          <td>站点描述</td>
          <td>
            <?php
            ZbpForm::textarea("description", $zbp->config("acgMora")->description, "89%", "5em");
            ?>
          </td>
          <td></td>
        </tr>
        <tr>
          <td style="text-align: right;">
            QQ：<br>
            QQ 群：<br>
            Bilibili：<br>
            GitHub：<br>
            知乎：<br>
          </td>
          <td>
            <!-- QQ -->
            <?php
            ZbpForm::text("QQ", $zbp->config("acgMora")->QQ, "49%", "5em");
            ZbpForm::text("QQT", $zbp->config("acgMora")->QQT, "49%", "5em");
            ?><br>
            <!-- QQ群 -->
            <?php
            ZbpForm::text("QQGroup", $zbp->config("acgMora")->QQGroup, "49%", "5em");
            ZbpForm::text("QQGroupT", $zbp->config("acgMora")->QQGroupT, "49%", "5em");
            ?><br>
            <!-- Bilibili -->
            <?php
            ZbpForm::text("Bili", $zbp->config("acgMora")->Bili, "49%", "5em");
            ZbpForm::text("BiliT", $zbp->config("acgMora")->BiliT, "49%", "5em");
            ?><br>
            <!-- GitHub -->
            <?php
            ZbpForm::text("Git", $zbp->config("acgMora")->Git, "49%", "5em");
            ZbpForm::text("GitT", $zbp->config("acgMora")->GitT, "49%", "5em");
            ?><br>
            <!-- 知乎 -->
            <?php
            ZbpForm::text("Zhihu", $zbp->config("acgMora")->Zhihu, "49%", "5em");
            ZbpForm::text("ZhihuT", $zbp->config("acgMora")->ZhihuT, "49%", "5em");
            ?><br>
          </td>
          <td>
            QQ 群第一格请写链接，示例：https://jq.qq.com/?_wv=1027&k=50639TG <br>
            其他项第一格写各自平台的字符或纯数字 ID <br>
            第二格为链接显示文本；<br>
            某一项第一格留空则不显示该项；<br>
            <a href="var/help-20191123112814.png" target="_blank">查看图片示例</a><br>
          </td>
        </tr>
        <tr>
          <td><input type="submit" value="提交" /></td>
          <td colspan="2"></td>
        </tr>
      </table>
    </form>
    <p>字体图标类：icon icon-home3</p>
    <!-- <p><?php echo htmlspecialchars('<i class="icon icon-home3"></i>'); ?></p> -->
    <p>见：<?php echo acgMora_Path("css/demo_index.html", "host"); ?></p>
  </div>
</div>
<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
