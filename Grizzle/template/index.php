<!DOCTYPE html>
<html lang="{$language}" xml:lang="{$language}">

<head>
  {template:header}
</head>

<body class="{$type}">
  <div id="wrap">
    {template:topnav}
    <main id="content">
      {foreach $articles as $article}
      {if $article.IsTop}
      {template:post-istop}
      {else}
      {template:post-multi}
      {/if}
      {/foreach}
      <div class="post page-navigator">{template:pagebar}</div>
      <div class="clearfix"></div>
    </main>
    {template:footer}
  </div>
</body>

</html>
