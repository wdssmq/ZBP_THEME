<article class="post">
  <div class="info a-ccc">
    <a href="{$article.Url}#respond" title="{$article.Title}"><span>{$article.CommNums} 条评论</span></a>
  </div>
  <div class="date">
    <span class="day">{$article.Time('d')}</span>
    <span class="month">{$article.Time('m')}</span>
    <span class="year">{$article.Time('Y')}</span>
  </div>
  <header class="hr-bottom">
    <h2 class="post-title">{$article.Title}</h2>
  </header>
  <div class="con hr-bottom">{$article.Content}</div>
  <!-- <hr> -->
  <p class="post-meta tags a-ccc hr-bottom">Tags: {foreach $article.Tags as $tag}<a class="post-tag tag" href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>{/foreach}<span title="请为本文设置合适的标签" class="post-tag tag c-help">设置Tag是个好习惯</span></p>
  <!-- <hr> -->
  <div class="prev_and_next hr-bottom">
    <div><strong>上一篇：{if $article.Prev}<a href="{$article.Prev.Url}" title="上一篇 {$article.Prev.Title}">{$article.Prev.Title}</a>{else}这个真没有{/if}</strong></div>
    <div><strong>下一篇：{if $article.Next}<a href="{$article.Next.Url}" title="下一篇 {$article.Next.Title}">{$article.Next.Title}</a>{else}这个真没有{/if}</strong></div>
  </div>
</article>

{if !$article.IsLock}
{template:comments}
{/if}
