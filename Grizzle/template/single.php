<!DOCTYPE html>
<html lang="{$language}" xml:lang="{$language}">

<head>
  {template:header}
</head>

<body class="{$type}">

  <div id="wrap">
    {template:topnav}
    <main id="content">
      {if $article.Type==ZC_POST_TYPE_ARTICLE}
      {template:post-single}
      {else}
      {template:post-page}
      {/if}
      <div class="clearfix"></div>
    </main>
    {template:footer}
  </div>

</body>

</html>
