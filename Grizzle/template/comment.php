<div class="media" id="cmt{$comment.ID}">
  <div class="msgavatar pull-left"><img class="avatar" src="{$comment.Author.Avatar}" alt="{$comment.Author.StaticName}" width="54" /></div>
  <div class="media-body">
    <b class="commentname"><a href="{$comment.Author.HomePage}" rel="nofollow" target="_blank" title="{$comment.Author.StaticName}">{$comment.Author.StaticName}</a></b>
    <div class="msgarticle">{$comment.Content}</div>
    <span class="msgtime">发布于&nbsp;{$comment.Time()}&nbsp;&nbsp;</span><span class="revertcomment"><a href="#respond" onclick="RevertComment('{$comment.ID}')" title="回复">回复</a></span>
    {foreach $comment.Comments as $comment}{template:comment}{/foreach}
  </div>
</div>
