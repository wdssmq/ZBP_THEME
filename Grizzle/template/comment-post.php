<div class="post" id="divCommentPost">
  <h3 class="posttop" id="respond">{if $user.ID>0}{$user.StaticName}{/if}发表评论:<a rel="nofollow" id="cancel-reply" href="#divCommentPost" style="display:none;"><small>取消回复</small></a></h3>
  <form id="frmSubmit" action="{$article.CommentPostUrl}">
    <input type="hidden" name="inpId" id="inpId" value="{$article.ID}" />
    <input type="hidden" name="inpRevID" id="inpRevID" value="0" />
    {if $user.ID>0}
    <input type="hidden" name="inpName" id="inpName" value="{$user.Name}" />
    <input type="hidden" name="inpEmail" id="inpEmail" value="{$user.Email}" />
    <input type="hidden" name="inpHomePage" id="inpHomePage" value="{$user.HomePage}" />
    {else}
    <div class="form-group">
      <label for="inpName" class="sr-only">名称(*)</label>
      <input type="text" name="inpName" id="inpName" class="form-control" value="{$user.Name}" placeholder="Name" size="33" tabindex="1" />
    </div>
    <div class="form-group">
      <label for="inpEmail" class="sr-only">邮箱</label>
      <input type="text" name="inpEmail" id="inpEmail" class="form-control" value="{$user.Email}" placeholder="Email" size="33" tabindex="2" />
    </div>
    <div class="form-group">
      <label for="inpHomePage" class="sr-only">网址</label>
      <input type="text" name="inpHomePage" id="inpHomePage" class="form-control" value="{$user.HomePage}" placeholder="HomePage" size="33" tabindex="3" />
    </div>
    {if $option['ZC_COMMENT_VERIFY_ENABLE']}
    <div class="form-group"><label for="inpVerify" class="sr-only">验证码(*)</label>
      <input type="text" name="inpVerify" id="inpVerify" class="form-control" value="" placeholder="验证码" size="33" tabindex="4" />
      <img width="{$option['ZC_VERIFYCODE_WIDTH']}" height="{$option['ZC_VERIFYCODE_HEIGHT']}" src="{$article.ValidCodeUrl}" alt="验证码" title="验证码" onclick="javascript:this.src='{$article.ValidCodeUrl}&amp;tm='+Math.random();" />
    </div>
    {/if}
    {/if}
    <div class="form-group clearfix">
      <label for="txaArticle" class="sr-only">正文(*)</label>
      <textarea name="txaArticle" id="txaArticle" class="form-control" cols="60" rows="5" tabindex="5"></textarea>
      <input class="btn btn-default pull-left" name="submit" id="submit" type="submit" tabindex="6" value="提交" onclick="return zbp.comment.post();" />
      <p class="postbottom pull-left">◎欢迎参与讨论，请在这里发表您的看法、交流您的观点。</p>
    </div>
  </form>
</div>
