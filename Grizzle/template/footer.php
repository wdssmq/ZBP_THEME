<footer id="footer">
  <div class="foot clearfix">
    <a class="pull-right" href="javascript:;" title="返回顶部" id="gotop">TOP</a>
    <div class="copy pull-left">{$copyright} | Powered By <a href="http://www.zblogcn.com/" rel="nofollow" title="RainbowSoft Z-BlogPHP" target="_blank">Z-BlogPHP</a> | Theme By <a href="http://www.wdssmq.com" target="_blank" title="置百丈玄冰而崩裂，掷须臾池水而漂摇。" rel="nofollow">沉冰浮水</a></div>
  </div>
</footer>
<script src="{$host}zb_system/script/jquery-2.2.4.min.js?v=3093114560"></script>
<script src="{$host}zb_system/script/zblogphp.js?v=3093114560"></script>
<script src="{$host}zb_system/script/c_html_js_add.php"></script>
<script src="{$host}zb_users/theme/{$theme}/script/script.min.js?v=3093114560"></script>
{$footer}
