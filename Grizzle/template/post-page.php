<article class="post">
  <div class="info a-ccc">
    <a href="{$article.Url}#respond" title="{$article.Title}"><span>{$article.CommNums} 条评论</span></a>
  </div>
  <div class="date">
    <span class="day">{$article.Time('d')}</span>
    <span class="month">{$article.Time('m')}</span>
    <span class="year">{$article.Time('Y')}</span>
  </div>
  <header class="hr-bottom">
    <h2 class="post-title"><a href="{$article.Url}" rel="bookmark" title="{$article.Title}">{$article.Title}</a></h2>
  </header>
  <div class="con hr-bottom">{$article.Content}</div>
</article>

{if !$article.IsLock}
{template:comments}
{/if}
