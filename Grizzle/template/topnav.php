<div id="header">
  <header id="blog_title">
    <h1><a href="{$host}" title={$name}>{$name}</a></h1>
  </header>
  <div id="search">
    <form action="{$host}search.php" method="get" class="search" name="search">
      <input type="search" lang="zh-CN" placeholder="输入后回车直接搜索" class="text" name="q"><input class="sr-only" type="submit" value="搜索">
    </form>
  </div>
  <nav id="menu">
    <ul class="header-nav clearfix">
      {module:navbar}
      <li class="back">
        <div class="pull-left"></div>
      </li>
    </ul>
  </nav>
</div>
