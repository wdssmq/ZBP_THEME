<?php
RegisterPlugin("Grizzle", "ActivePlugin_Grizzle");

function ActivePlugin_Grizzle()
{
  Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu', 'Grizzle_AddMenu');
}

function Grizzle_AddMenu(&$m)
{
  global $zbp;
  $m[] = MakeTopMenu("root", '主题配置', $zbp->host . "zb_users/theme/Grizzle/main.php", "", "topmenu_Grizzle");
}

function InstallPlugin_Grizzle()
{
  global $zbp;
  if (!$zbp->HasConfig('Grizzle')) {
    $zbp->Config('Grizzle')->Version = '1.0';
    $zbp->Config('Grizzle')->keywords = '自定义关键词,半角逗号分隔';
    $zbp->Config('Grizzle')->description = '自定义描述';
    $zbp->SaveConfig('Grizzle');
  }
}

function UninstallPlugin_Grizzle()
{
  global $zbp;
  #$zbp->DelConfig('Grizzle');
}
