<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';

$zbp->Load();
$action = 'root';
if (!$zbp->CheckRights($action)) {
  $zbp->ShowError(6);
  die();
}
if (!$zbp->CheckPlugin('Grizzle')) {
  $zbp->ShowError(48);
  die();
}
$blogtitle = 'Grizzle 主题配置';

require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';

$act = GetVars('act', 'GET');
$suc = GetVars('suc', 'GET');
if ($act == 'save') {
  CheckIsRefererValid();
  $zbp->Config('Grizzle')->keywords = $_POST['keywords'];
  $zbp->Config('Grizzle')->description = $_POST['description'];
  $zbp->SaveConfig('Grizzle');
  $zbp->BuildTemplate();
  $zbp->SetHint('good');
  Redirect('./main.php' . ($suc == null ? '' : '?act=$suc'));
}
if ($zbp->Config('Grizzle')->HasKey("KeyWords")) {
  $zbp->Config('Grizzle')->keywords = $zbp->Config('Grizzle')->KeyWords;
}
?>

<style>
  .divHeader {
    background-position: 0 11px !important;
  }
</style>


<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle; ?></div>
  <div class="SubMenu">
    <a href="main.php" title="首页"><span class="m-left m-now">首页</span></a>
    <?php require "about.php"; ?>
  </div>
  <div id="divMain2">
    <form method="post" action="<?php echo BuildSafeURL("main.php?act=save"); ?>">
      <table class="tableFull tableBorder">
        <tbody>
          <tr class="color1">
            <th scope="col" height="32" width="150px">配置项</th>
            <th scope="col">配置内容</th>
          </tr>
          <tr class="color3">
            <th scope="row">关键词</th>
            <td><textarea style="width:98%" name="keywords" rows="2"><?php echo $zbp->Config('Grizzle')->keywords; ?></textarea></td>
          </tr>
          <tr class="color2">
            <th scope="row">描述</th>
            <td><textarea style="width:98%" name="description" rows="2"><?php echo $zbp->Config('Grizzle')->description; ?></textarea></td>
          </tr>
        </tbody>
      </table>
      <input value="提交" class="button" name="ok" type="submit" />
    </form>
  </div>

</div>
<script type="text/javascript">
  ActiveTopMenu("topmenu_Grizzle");
</script>
<script type="text/javascript">
  AddHeaderIcon("<?php echo $bloghost . 'zb_users/theme/Grizzle/screenshot.png'; ?>");
</script>
<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
