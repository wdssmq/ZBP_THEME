## 投喂支持

爱发电：[https://afdian.com/a/wdssmq](https://afdian.com/a/wdssmq "沉冰浮水正在创作和 z-blog 相关或无关的各种有用或没用的代码 | 爱发电")

哔哩哔哩：[https://space.bilibili.com/44744006](https://space.bilibili.com/44744006 "沉冰浮水的个人空间\_哔哩哔哩\_bilibili")「投币或充电」「[大会员卡券领取 - bilibili](https://account.bilibili.com/account/big/myPackage "大会员卡券领取 - bilibili")」

RSS 订阅：[https://feed.wdssmq.com](https://feed.wdssmq.com "沉冰浮水博客的 RSS 订阅地址") 「[「言说」RSS 是一种态度！！](https://www.wdssmq.com/post/20201231613.html "「言说」RSS 是一种态度！！")」

在更多平台关注我：[https://www.wdssmq.com/guestbook.html#其他出没站点和信息](https://www.wdssmq.com/guestbook.html#%E5%85%B6%E4%BB%96%E5%87%BA%E6%B2%A1%E5%9C%B0%E7%82%B9%E5%92%8C%E4%BF%A1%E6%81%AF "在更多平台关注我")

更多「小代码」：[https://cn.bing.com/search?q=小代码+沉冰浮水](https://cn.bing.com/search?q=%E5%B0%8F%E4%BB%A3%E7%A0%81+%E6%B2%89%E5%86%B0%E6%B5%AE%E6%B0%B4 "小代码 沉冰浮水 - 必应搜索")

<!-- ##################################### -->

## 说明

当年从 Hexo 移植的主题，虽然用了 less 但是当时并不会用。。

给 Grunt 加了 PurgeCSS 用于清理实际没用到的 CSS；

## 图示

![001](<https://app.zblogcn.com/zb_users/upload/2015/01/201501311422709569631118.png> "001.png")

![002](<https://app.zblogcn.com/zb_users/upload/2015/01/201501311422709569355456.png> "002.png")

## 更新日志

2024-05-18：引入 PurgeCSS 清理；部分 CSS 改为了 less 写法；小修了部分样式细节；

2021-04-02：`add about.php`；

2020-12-03：诈尸更新；

2019-06-07：整合【Grunt 插件】[https://app.zblogcn.com/?id=2110](https://app.zblogcn.com/?id=2110 "「开发工具」Grunt - Z-Blog 应用中心")

2015-02-05：图片宽度，页面主体宽度；谁知道为什么搜索表单 `method="post "` 要加空格才能用？

2015-02-04：`.post .con` 样式重复；

2015-02-01：搜索框修复（修复了个鬼，GET 写成 POST 了）。
