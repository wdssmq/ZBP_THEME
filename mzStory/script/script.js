$(function() {
  "use strict";
  // 导航类名
  $(".navbar-menu > li").each(function() {
    $(this).addClass("navbar-item");
  });
  // 手机端导航切换
  $(".navbar-burger").click(function() {
    let target = $(this).data("target");
    $(`${target}`).toggleClass("is-active");
    $(this).toggleClass("is-active");
  });
  // 侧栏跟随
  let $sidebar = $("#sidebar.column .mod:last-of-type"),
    offsetTop = 0,
    // copyTop = 0,
    scrollt = 0;
  $(window).scroll(function() {
    if ($sidebar.length === 0) return;
    scrollt = $(window).scrollTop();
    if (scrollt >= 137) {
      $("#fixed-box,.backTop").addClass("is-active");
    } else {
      $("#fixed-box,.backTop").removeClass("is-active");
    }
    // console.log(scrollt);
    if ($(window).width() < 768) return false;
    if (offsetTop == 0) {
      offsetTop = $sidebar.offset().top;
    }
    let copyTopN = $(".footer").offset().top;
    if (scrollt + $sidebar.outerHeight() > copyTopN - 137) {
      let marTop = parseInt($sidebar.css("marginTop"));
      let rdyTop = copyTopN - offsetTop - $sidebar.outerHeight() - 137;
      $sidebar.stop().animate({
        marginTop: marTop > rdyTop ? marTop : rdyTop,
      });
      return false;
    }
    if (scrollt > offsetTop) {
      $sidebar.stop().animate({
        marginTop: scrollt - offsetTop + 45,
      });
    } else {
      $sidebar.stop().animate({
        marginTop: 15,
      });
    }
    return false;
  });
  $(".backTop").click(function() {
    return (
      $("html,body")
        .animate(
          {
            scrollTop: "0px",
          },
          500,
        )
        .fadeIn(),
      !1
    );
  });
  function fnSrollToEL(target) {
    $("body,html").animate(
      {
        scrollTop: $(target).offset().top - 59,
      },
      500,
    );
  }
  // 评论重写
  zbp.plugin.unbind("comment.reply.start", "system");
  zbp.plugin.on("comment.reply.start", "MizuJS", function(id) {
    var me = this;
    this.$("#inpRevID").val(id);
    this.$("#cancel-reply")
      .show()
      .bind("click", function() {
        me.$("#inpRevID").val(0);
        me.$(this).hide();
        fnSrollToEL("#cmt" + id);
        // window.location.hash = "#cmt" + id;
        return false;
      });
    fnSrollToEL("#comment");
    // window.location.hash = "#comment";
  });
  zbp.plugin.unbind("comment.post.success", "system");
  zbp.plugin.on(
    "comment.post.success",
    "MizuJS",
    function(formData, retString, textStatus, jqXhr) {
      var objSubmit = $("#inpId").parent("form").find(":submit");
      objSubmit
        .removeClass("loading")
        .removeAttr("disabled")
        .val(objSubmit.data("orig"));
      var data = $.parseJSON(retString);
      if (data.err.code !== 0) {
        alert(data.err.msg);
        throw "ERROR - " + data.err.msg;
      }
      if (formData.replyid == "0") {
        this.$(data.data.html).insertAfter("#AjaxCommentBegin");
      } else {
        this.$(data.data.html).insertAfter("#MizuComment" + formData.replyid);
      }
      fnSrollToEL("#cmt" + data.data.ID);
      // location.hash = "#cmt" + data.data.ID;
      this.$("#txaArticle").val("");
      this.userinfo.readFromHtml();
      // this.userinfo.save();
    },
  );
  if ($(".commentpost").length === 0) return;
  let $str = $("#inpName").val().trim();
  if (!$str) {
    zbp.userinfo.output();
  }
});
;/* global Swiper */
$(document).ready(function() {
  // eslint-disable-next-line no-unused-vars
  const swiper = new Swiper(".swiper-container", {
    pagination: {
      el: ".swiper-pagination",
    },
  });
});
