<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action = 'root';
if (!$zbp->CheckRights($action)) {
  $zbp->ShowError(6);
  die();
}
if (!$zbp->CheckPlugin('mzStory')) {
  $zbp->ShowError(48);
  die();
}

InstallPlugin_mzStory();

$act = GetVars('act', 'GET');
$suc = GetVars('suc', 'GET');

// 基础配置保存
if ($act == 'save') {
  CheckIsRefererValid();
  foreach ($_POST as $key => $val) {
    if (substr($key, 0, 5) == 'read_') {
      continue;
    }
    $zbp->Config('mzStory')->$key = trim($val);
  }
  $zbp->SaveConfig('mzStory');
  $zbp->BuildTemplate();
  $zbp->SetHint('good');
  Redirect('./main.php' . ($suc == null ? '' : "?act={$suc}"));
}
// 幻灯片配置读取
$uFile = mzStory_Path("u-slide");
$uData = json_decode(file_get_contents($uFile));

// 幻灯片配置保存
if ($act === "slides-save") {
  CheckIsRefererValid();
  $strData = GetVars("data", "POST");
  $objData = json_decode($strData);
  if (!empty($objData)) {
    $uData->slides = $objData->slides;
    // $uData->labels = $objData->labels;
    file_put_contents($uFile, json_encode($uData));
  }
  $zbp->BuildTemplate();
  $zbp->SetHint('good');
  die();
}

// 幻灯片配置解析
$slides = $uData->slides;
foreach ($slides as $k1 => &$v1) {
  foreach ($v1 as $k2 => &$v2) {
    $v2 = str_replace('{$host}', $zbp->host, $v2);
    $v2 = str_replace('{$name}', $zbp->name, $v2);
  }
}

$logo = mzStory_Path("u-logo", "host") . "?" . time();
$favicon = mzStory_Path("u-fav", "host") . "?" . time();

$blogtitle = 'mzStory()';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
?>

<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle; ?></div>
  <div id="divMain2">
    <div class="content-box">
      <ul class="content-box-tabs">
        <li><a href="#base" class="default-tab current">基础</a></li>
        <li><a href="#slide">幻灯片设置</a></li>
      </ul>
      <div style="clear: both"></div>
      <div class="content-box-content">
        <!-- tab - base -->
        <div class="tab-content default-tab" id="base">
          <form action="<?php echo BuildSafeURL("main.php?act=save"); ?>" method="post">
            <table width="100%" class="tableBorder">
              <tr>
                <th width="15%">项目</th>
                <th>内容</th>
                <th width="50%">说明</th>
              </tr>
              <tr>
                <td>logo</td>
                <td>
                  <input type="file" name="logo" size="60">&nbsp;&nbsp;<img src="<?php echo $logo; ?>" alt="logo" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
                </td>
                <td></td>
              </tr>
              <tr>
                <td>favicon.ico</td>
                <td>
                  <input type="file" name="fav" size="60">&nbsp;&nbsp;<img src="<?php echo $favicon; ?>" alt="favicon" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
                </td>
                <td><b>注意：如果博客根目录下存favicon.ico则此处上传无效</b></td>
              </tr>
              <tr>
              <tr>
                <td>关键词</td>
                <td>
                  <?php
                  ZbpForm::text("keywords", $zbp->Config("mzStory")->keywords, "89%");
                  ?>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>站点描述</td>
                <td>
                  <?php
                  ZbpForm::textarea("description", $zbp->config("mzStory")->description, "89%", "5em");
                  ?>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>首页-全站热门文章</td>
                <td><?php zbpform::text("focusNums", $zbp->Config("mzStory")->focusNums, "90%"); ?></td>
                <td>推荐值 10，然后将「网站设置」「页面设置」「列表页显示文章的数量」也设置为 15</td>
              </tr>
              <tr>
                <td></td>
                <td colspan="2"><input type="submit" value="提交" /></td>
              </tr>
            </table>
          </form>
        </div>
        <!-- tab - slide -->
        <div class="tab-content" id="slide">
          <table id="slides" class="tableFull tableBorder-thcenter">
            <thead>
              <tr>
                <th style="width:3em;">序列</th>
                <th class="td25">标题</th>
                <!-- <th class="tdEdit" style="width:4em;">编辑</th> -->
                <th class="td25">链接</th>
                <th>图片</th>
              </tr>
            </thead>
            <tbody class="ui-sortable">
              <?php foreach ($slides as $key => $v) { ?>
                <?php $id = $key + 1; ?>
                <tr class="js-tr<?php echo $id; ?>">
                  <td class="tdCenter"><?php echo $id; ?></td>
                  <td><?php ZbpForm::text("Title", $v->Title, "100%"); ?></td>
                  <!-- <td class="tdCenter"><a data-id="<?php echo $id; ?>" href="javascript:;" class="js-fill"><img src="../../../zb_system/image/admin/page_edit.png" alt="辅助工具"></a></td> -->
                  <td><?php ZbpForm::text('Url', $v->Url, '100%'); ?></td>
                  <td><?php ZbpForm::text("Img", $v->Img, "100%"); ?></td>
                </tr>
              <?php
              } ?>
            </tbody>
          </table>
          <p><input id="slides-save" type="button" class="button" value="更新"> <a href="javascript:;" onclick="location.reload();">刷新</a></p>
        </div>
      </div>
    </div>


  </div>
</div>

<script src="admin/script.js"></script>

<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
