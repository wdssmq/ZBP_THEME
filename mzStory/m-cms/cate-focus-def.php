{php}<?php
      $first = $articles[0];
      $first->Intro = preg_replace('/[\r\n\s]+/', ' ', trim(SubStrUTF8(FormatString($first->Content, '[nohtml]'), 137)) . '...');
      ?>{/php}
<div class="container">
  <div class="columns cate-focus">
    <div class="column is-three-quarters cate-focus-left">
      <div class="title-wrapper">
        <h4 class="title title-style title-red">
          <a href="{$cate->Url}" title="{$cate.Name}">{$cate.Name}</a>
        </h4>
      </div>

      <div class="first-post">
        <h2 class="first-post-title has-text-centered post-title"><a class="link-base" href="{$first.Url}" title="{$first.Title}">{$first.Title}</a></h2>
        <p class="first-post-intro">{$first.Intro}</p>
        <p class="first-meta is-clearfix">
          <a class="btn-more button is-custom is-small is-pulled-right" href="{$first.Url}" title="{$first.Title}">阅读更多</a>
        </p>
      </div>

      <div class="columns is-multiline">
        {foreach $articles as $article}
        {if $first.ID !== $article.ID}
        {template:p-post-list-item}
        {/if}
        {/foreach}
      </div>
    </div>
    <div class="column is-one-quarter cate-focus-right">
      <div class="title-wrapper">
        <h4 class="title title-style title-bold">{$cate->Name}</h4>
      </div>
      {php}
      $num = count($articles)
      {/php}
      {mzStory_GenHot(10, $cate->ID)}
    </div>
  </div>
</div>
