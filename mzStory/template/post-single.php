{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
{/php}
<!-- <span data-id="{$article.ID}" class="is-px-12 js-edt is-pulled-right"></span> -->
<section class="post cate{$article.Category.ID} auth{$article.Author.ID} box content">
  <h2 class="post-title title is-5 has-text-centered noborder"><a class="a-color a-shadow" href="{$article.Url}" title="{$article.Title}">{$article.Title}</a></h2>
  <p class="post-meta info has-text-grey-light has-text-centered">
    <span class="author"><a class="has-text-grey-light" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a></span> |
    <span class="date"><a class="post-time has-text-grey-light" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$article.Time('Y-m-d')}</a></span> |
    <span class="category"><a class="has-text-grey-light" href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a></span> |
    <span class="comment"><a class="has-text-grey-light" href="{$article.Url}#comment" title="{$article.Title}">{$article.CommNums} 条留言</a></span> |
    <span class="views">{$article.ViewNums}</span>
  </p>
  <hr class="post-hr">
  <div class="post-body">{$article.Content}</div>
  <hr class="post-hr">
  <p class="post-meta tags">
    {if count($article.Tags)}
    {foreach $article.Tags as $tag}<a class="post-tag tag" href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>{/foreach}
    {else}
    <span title="请为本文设置合适的标签" class="post-tag tag c-help">设置Tag是个好习惯</span>
    {/if}
  </p>
  <hr class="post-hr">
</section>

<div class="box post-nav is-clearfix">
  {if $article.Prev}
  <div class="is-pulled-left">
    <span>上一篇：</span>
    <a class="a-ccc link-base a-shadow" href="{$article.Prev.Url}" title="上一篇 {$article.Prev.Title}">{$article.Prev.Title}</a>
  </div>
  {/if}
  {if $article.Next}
  <div class="is-pulled-right has-text-right">
    <span>下一篇：</span>
    <a class="a-ccc link-base a-shadow" href="{$article.Next.Url}" title="下一篇 {$article.Next.Title}">{$article.Next.Title}</a>
  </div>
  {/if}
</div>

<section class="box">
  <div class="title-wrapper">
    <h4 class="title title-style title-bold">相关文章</h4>
  </div>
  <ul class="related-list">
    {foreach $article.RelatedList as $related}
    <li><a class="a-color a-shadow" href="{$related.Url}" title="{$related.Title}">{$related.Title}</a> (<span>{$related.Time('Y-m-d')}</span>)</li>
    {/foreach}
  </ul>
</section>

{if !$article.IsLock}
{template:comments}
{/if}
