<!-- 导航容器 -->
<div class="swiper-container">
  <div class="swiper-wrapper">
    {foreach $slides as $slide}
    <div class="swiper-slide">
      <a class="has-text-centered noborder" href="{$slide->Url}" title="{$slide->Title}">
        <img src="{$slide.Img}" alt="{$slide.Title}">
        <!-- <span>{$slide.Title}</span> -->
      </a>
    </div>
    {/foreach}
  </div>
  <!-- /.swiper-wrapper -->
  <!-- 导航 -->
  <div class="swiper-pagination"></div>
</div>

