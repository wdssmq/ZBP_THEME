{* Template Name: 单页 * Template Type: single *}
<!DOCTYPE html>
<html lang="{$language}">

<head>
  {template:header}
</head>

<body class="{$type}">
  <!-- include:hero -->
  {template:hero}
  <!-- main -->
  <main class="section">
    <!-- 面包屑  -->
    {template:p-breadcrumb}
    <div class="single">
      <div class="container">
        <div class="columns">
          <div class="column is-three-quarters">
            {if $article.Type==ZC_POST_TYPE_ARTICLE}
            {template:post-single}
            {else}
            {template:post-page}
            {/if}
          </div>
          <div class="column is-one-quarter">
            {if $type=='article'}
            <div class="mod-box">
              <div class="title-wrapper">
                <h4 class="title title-style">{$article.Category.Name} 热门文章</h4>
              </div>
              {mzStory_GenHot($zbp->Config("mzStory")->focusNums, $article.Category.ID)}
            </div>
            {/if}
            {template:sidebar}
          </div>
        </div>
      </div>
    </div>
  </main>
  <!-- include:footer -->
  {template:footer}
</body>

</html>
