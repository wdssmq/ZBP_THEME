﻿{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
{/php}
<!-- <span data-id="{$article.ID}" class="is-px-12 js-edt is-pulled-right"></span> -->
<section class="post cate{$article.Category.ID} auth{$article.Author.ID} box content">
  <h2 class="post-title title is-5 has-text-centered noborder"><a class="a-color a-shadow" href="{$article.Url}" title="{$article.Title}">{$article.Title}</a></h2>
  <p class="post-meta info has-text-grey-light has-text-centered">
    <span class="author"><a class="has-text-grey-light" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a></span> |
    <span class="date"><a class="post-time has-text-grey-light" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$article.Time('Y-m-d')}</a></span> |
    <span class="comment"><a class="has-text-grey-light" href="{$article.Url}#comment" title="{$article.Title}">{$article.CommNums}条留言</a></span> |
    <span class="views">{$article.ViewNums}</span>
  </p>
  <hr class="post-hr">
  <div class="post-body">{$article.Content}</div>
  <hr class="post-hr">
</section>


{if !$article.IsLock}
{template:comments}
{/if}
