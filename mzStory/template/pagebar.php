{if $pagebar}
<nav class="pagination is-small is-custom">
<ul class="pagination-list">
{foreach $pagebar.buttons as $k=>$v}
  {if $pagebar.PageNow==$k}
    <li><span class="pagination-link is-current">{$k}</span></li>
  {else}
    <li><a class="pagination-link" href="{$v}" title="{$k}">{$k}</a></li>
  {/if}
{/foreach}
</ul>
</nav>
{/if}
