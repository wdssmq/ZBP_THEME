<div class="post-list-item column is-half is-flex">
  <a class="link-red post-cate" href="{$article.Category.Url}" title="{$article.Category.Name}">[{$article.Category.Name}]</a>
  <a class="link-base post-title" href="{$article.Url}" title="{$article.Title}">{$article.Title}</a>
</div>
