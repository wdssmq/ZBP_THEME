{foreach $comments as $comment}
<li class="is-flex is-align-items-center">
  <i class="icon-stroty icon-stroty-lingxing is-px-15"></i>
  <a class="link-base" href="{$comment->Post->Url}#cmt{$comment->ID}" title="{htmlspecialchars($comment->Author->StaticName . ' @ ' . $comment->Time())}">{FormatString($comment->Content, '[noenter]')}</a>
</li>
{/foreach}
