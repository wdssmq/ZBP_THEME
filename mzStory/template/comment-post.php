<div class="box comment-post">
  <div class="title-wrapper">
    <h3 class="title title-style title-bold" id="comment">{if $user.ID>0}{$user.Name}{/if}发表评论：</h3>
  </div>
  <form id="frmSubmit" target="_self" method="post" action="{$article.CommentPostUrl}">
    <input type="hidden" name="inpId" id="inpId" value="{$article.ID}">
    <input type="hidden" name="inpRevID" id="inpRevID" value="0">
    {if $user.ID>0}
    <input type="hidden" name="inpName" id="inpName" value="{$user.Name}">
    <input type="hidden" name="inpEmail" id="inpEmail" value="{$user.Email}">
    <input type="hidden" name="inpHomePage" id="inpHomePage" value="{$user.HomePage}">
    {else}
    <div class="field">
      <label class="label is-hidden" for="inpName">名称(*)</label>
      <div class="control">
        <input class="input is-info" type="text" name="inpName" id="inpName" value="{if $user.Name!='访客'}{$user.Name}{/if}" placeholder="昵称">
      </div>
    </div>
    <div class="field">
      <label class="label is-hidden" for="inpEmail">邮箱</label>
      <div class="control">
        <input class="input is-info" type="text" name="inpEmail" id="inpEmail" value="{$user.Email}" placeholder="邮箱">
      </div>
    </div>
    <div class="field">
      <label class="label is-hidden" for="inpHomePage">网址</label>
      <div class="control">
        <input class="input is-info" type="text" name="inpHomePage" id="inpHomePage" value="{$user.HomePage}" placeholder="网址">
      </div>
    </div>
    {if $option['ZC_COMMENT_VERIFY_ENABLE']}
    <div class="field has-addons">
      <label class="label is-hidden" for="inpVerify">验证码(*)</label>
      <div class="control">
        <input class="input is-info" type="text" name="inpVerify" id="inpVerify" value="{$user.HomePage}" placeholder="输入右边图片中的数字">
      </div>
      <div class="control">
        <img style="height: 100%;cursor:pointer;" src="{$article.ValidCodeUrl}" alt="验证码" title="验证码" onclick="javascript:this.src='{$article.ValidCodeUrl}&amp;tm='+Math.random();" />
      </div>
    </div>
    {/if}
    {/if}
    <div class="field">
      <label for="txaArticle" class="label">正文(*)</label>
      <div class="control">
        <textarea class="textarea is-info" placeholder="说点什么……" name="txaArticle" id="txaArticle"></textarea>
      </div>
    </div>
    <div class="field is-grouped">
      <div class="control">
        <input class="button is-custom" name="submit" type="button" value="提交" onclick="return zbp.comment.post();" title="提交">
      </div>
      <div class="control">
        <button type="button" class="button" rel="nofollow" id="cancel-reply" style="display:none;" title="取消回复">取消回复</button>
      </div>
    </div>
  </form>
  <p class="postbottom">◎欢迎参与讨论，请在这里发表您的看法、交流您的观点。</p>
</div>
