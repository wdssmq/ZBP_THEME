{foreach $catalogs as $catalog}
<li class="is-flex is-align-items-center">
  <i class="icon-stroty icon-stroty-lingxing is-px-15"></i>
  <a class="link-base" title="{$catalog.Name}" href="{$catalog.Url}">{$catalog.Name}</a>
</li>
{/foreach}
