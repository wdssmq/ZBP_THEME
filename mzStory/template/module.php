<section class="mod-box" id="{$module.HtmlID}">
  {if !$module.IsHideTitle && $module.Name}
  <div class="title-wrapper">
    <h4 class="title title-style">{$module.Name}</h4>
  </div>
  {else}
  <h4 class="is-sr-only">{$module.Name}</h4>
  {/if}
  {if $module.Type=='div'}
  <div class="mod-bd {$module.FileName}">
    {if $module.FileName=='searchpanel'}
    {template:module-searchpanel}
    {else}
    {$module.Content}
    {/if}
  </div>
  {/if}
  {if $module.Type=='ul'}<ul class="mod-list {$module.FileName}">{$module.Content}</ul>{/if}
</section>
<!-- {$module.FileName} -->
