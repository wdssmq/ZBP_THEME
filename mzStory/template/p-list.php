<div class="list">
  <div class="container">
    <div class="columns">
      <div class="column is-three-quarters">
        <div class="title-wrapper">
          <h4 class="title title-style title-red">{$title}</h4>
        </div>
        {foreach $articles as $article}
        {template:post-multi}
        {/foreach}
        {template:pagebar}
      </div>
      <div class="column is-one-quarter">
        {if $type=='category'}
        <div class="mod-box">
          <div class="title-wrapper">
            <h4 class="title title-style">{$title} 热门文章</h4>
          </div>
          {mzStory_GenHot($zbp->Config("mzStory")->focusNums, $category->ID)}
        </div>
        {/if}
        {template:sidebar}
      </div>
    </div>
  </div>
</div>
