{* Template Name: 首页及列表页 * Template Type: index|list|search *}

<!DOCTYPE html>
<html lang="{$language}">

<head>
  {template:header}
</head>

<body class="{$type}">
  <!-- include:hero -->
  {template:hero}
  <!-- main -->
  <main class="section">
    <!-- 面包屑  -->
    {template:p-breadcrumb}
    <!-- 判断首页 -->
    {if $type=='index'&&$page=='1'}
    <!-- 焦点区域 -->
    {template:focus}
    <!-- 分类展示 -->
    {template:n-cms-cate-focus}
    {else}
    {template:p-list}
    {/if}
  </main>
  <!-- include:footer -->
  {template:footer}
</body>

</html>
