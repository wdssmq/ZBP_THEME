{php}<?php
$first = $articles[0];
$first->Intro = preg_replace('/[\r\n\s]+/', ' ', trim(SubStrUTF8(FormatString($first->Content,'[nohtml]'),235)).'...');
?>{/php}
<div class="warp-focus">
  <div class="container">
    <div id="focus" class="columns">
      <!-- 左则，占 2/3 -->
      <div class="column is-three-quarters focus-left">
        <div class="columns">
          <div class="column is-two-fifths left">
            {template:n-slide}
          </div>

          <div class="column is-three-fifths right">
            <h4 class="focus-area-title">
              <i class="icon-stroty icon-stroty-guanzhu"></i>最新文章
            </h4>
            <div class="first-post">
              <h2 class="first-post-title has-text-centered post-title"><a class="link-base" href="{$first.Url}" title="{$first.Title}">{$first.Title}</a></h2>
              <p class="first-post-intro">{$first.Intro}</p>
            </div>

            <div class="after-first columns is-multiline">
              {foreach $articles as $article}
              {if $first.ID !== $article.ID}
              <div class="post-list-item column is-half is-flex">
                <a class="link-red post-cate" href="{$article.Category.Url}" title="{$article.Category.Name}">[{$article.Category.Name}]</a>
                <a class="link-base post-title" href="{$article.Url}" title="{$article.Title}">{$article.Title}</a>
              </div>
              {/if}
              {/foreach}
            </div>
          </div>

        </div>
      </div>

      <!-- 右则，1/3 -->
      <div class="column is-one-quarter focus-right">
        <h4 class="focus-area-title">
          <i class="icon-stroty icon-stroty-huomiao"></i>大家都在看
        </h4>
        {template:n-hot}
      </div>
    </div>
    <!-- #focus -->
  </div>
  <!-- /.container -->
</div>
