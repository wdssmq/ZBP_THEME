<nav class="navbar is-light has-background-white-bis" id="sub-navbar">
  <div class="container">
    <div class="navbar-brand">
      <a class="navbar-item" href="{$host}" title="{$name}">
        欢迎访问「{$name}」！
      </a>
      <a role="button" aria-label="menu" class="navbar-burger" data-target="sub-navbar-menu">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>
    <div id="sub-navbar-menu" class="navbar-menu">
      <div class="navbar-end">
        {module:sub-navbar-AGA}
      </div>
    </div>
  </div>
</nav>

<header class="hero is-small">
  <div class="hero-body">
    <div class="container level">
      <div class="level-left">
        <h1 class="title is-marginless noborder level-item">
          <!-- <a href="{$host}" title="{$name}">{$name}</a> -->
          <a href="{$host}" title="{$name}"><span class="is-sr-only">{$name}</span><img src="{$host}zb_users/theme/{$theme}/usr/logo.png" alt="{$name}"></a>
        </h1>
        <p class="subtitle is-sr-only">
          {$subname}
        </p>
      </div>
      <!--/.level-left-->
      <div class="level-right hero-search">
        <form class="level-item" name="search" method="post" action="{$host}zb_system/cmd.php?act=search">
          <div id="top-search-field" class="field has-addons">
            <div class="control is-expanded has-icons-left">
              <input class="input is-custom" type="text" placeholder="搜索喜欢的内容吧" name="q">
              <span class="icon is-small is-left level-item">
                <i class="icon-stroty icon-stroty-sousuo"></i>
              </span>
            </div>
            <div class="control">
              <input type="submit" class="button is-custom" value="搜索">
            </div>
          </div>
        </form>
      </div>
      <!--/.level-right -->
    </div>
  </div>
</header><!-- /header -->

<nav class="navbar noborder is-custom">
  <div class="container">
    <div class="navbar-brand">
      <div class="navbar-burger" data-target=".navbar-menu">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <ul class="navbar-menu">{module:navbar}</ul>
  </div>
</nav><!-- /.navbar-->
