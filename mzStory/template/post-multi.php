{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
$article->Intro = preg_replace('/[\r\n\s]+/', ' ', trim(SubStrUTF8(FormatString($article->Content,'[nohtml]'),235)).'...');
{/php}
<section class="post post-multi cate{$article.Category.ID} auth{$article.Author.ID} off-is-clearfix content">
  <h2 class="post-title title noborder"><a class="link-base a-shadow" href="{$article.Url}" title="{$article.Title}">{$article.Title}</a><span data-id="{$article.ID}" class="is-size-7 js-edt is-pulled-right"></span></h2>
  <div class="post-intro">{$article.Intro}</div>
  <div class="post-footer is-clearfix"><a class="btn-more button is-custom is-small is-pulled-right" href="{$article.Url}" title="{$article.Title}">阅读更多</a></div>
</section>
