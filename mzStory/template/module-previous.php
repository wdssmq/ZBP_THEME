{foreach $articles as $article}
<li class="is-flex is-align-items-center">
  <i class="icon-stroty icon-stroty-lingxing is-px-15"></i>
  <a class="link-base" href="{$article.Url}" title="{$article.Title}">{$article.Title}</a>
</li>
{/foreach}
