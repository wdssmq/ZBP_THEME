{php}
$i = 1;
{/php}
<ul class="mod-list hot">
  {foreach $articles as $article}
  <li class="is-flex is-align-items-center"><span>{$i}</span> <a class="link-base" href="{$article->Url}" title="{$article->Title}">{$article->Title}</a></li>
  {php}
  $i++;
  {/php}
  {/foreach}
</ul>
