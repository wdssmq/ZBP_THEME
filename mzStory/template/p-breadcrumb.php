<div class="bread-wrapper">
  <div class="container is-flex is-align-items-center">
    <nav class="breadcrumb is-px-14 columns">
      <ul class="column">
        {if $type=='article'}
        <li class="lead-text">当前位置 ：</li>
        <li><a href="{$host}" title="首页">首页</a></li>
        <li><a href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a></li>
        <li class="is-active"><a href="{$article.Url}" title="{$article.Title}">{$article.Title}</a></li>
        {elseif $type=='index'}
        <li class="lead-text">当前位置 ：</li>
        <li><a href="{$host}" title="首页">首页</a></li>
        {else}
        <li class="lead-text">当前位置 ：</li>
        <li><a href="{$host}" title="首页">首页</a></li>
        <li class="is-active"><a href="{$zbp.fullcurrenturl}" title="{$title}">{$title}</a></li>
        {/if}
      </ul>
    </nav>
  </div>
</div>
