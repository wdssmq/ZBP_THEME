/* global Swiper */
$(document).ready(function() {
  // eslint-disable-next-line no-unused-vars
  const swiper = new Swiper(".swiper-container", {
    pagination: {
      el: ".swiper-pagination",
    },
  });
});
