$(function() {

  // 刷新时保持当前 tab
  if (location.hash !== "") {
    $(".tab-content.default-tab").hide();
    $(".content-box-tabs .current").removeClass("current");
    $(location.hash).show();
    $("a[href=\"" + location.hash + "\"]").addClass("current");
  }

  // 构建幻灯片提交数据
  function fnGenPostData(elId) {
    const data = [];
    $(`#${elId} tbody tr`).each(function() {
      const arrWEV = {};
      $(this)
        .find("input")
        .each(function() {
          // arrWEV.push($(this).val());
          arrWEV[$(this).attr("name")] = $(this).val();
        });
      data.push(arrWEV);
    });
    return data;
  }

  function fnSaveSlides(
    fnback = function(n) {
      console.log(n);
    },
  ) {
    const arrRTB = { slides: [], labels: [] };
    for (const key in arrRTB) {
      if (Object.hasOwnProperty.call(arrRTB, key)) {
        arrRTB[key] = fnGenPostData(key);
      }
    }
    console.log(arrRTB);
    const csrfToken = $("meta[name='csrfToken']").attr("content");
    // return false;
    $.post(
      "main.php?act=slides-save",
      {
        data: JSON.stringify(arrRTB),
        csrfToken,
      },
      function(data) {
        fnback(data);
        // alert("Data Loaded: " + data);
      },
    );
  }

  // 幻灯片提交保存
  $("#slides-save").click(function() {
    $(this).val("提交中");
    fnSaveSlides((e) => {
      location.reload();
    });
    return false;
  });
});
