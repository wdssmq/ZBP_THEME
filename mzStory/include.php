<?php
#注册插件
RegisterPlugin("mzStory", "ActivePlugin_mzStory");

function ActivePlugin_mzStory()
{
  Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu', 'mzStory_AddMenu');
  Add_Filter_Plugin('Filter_Plugin_ViewList_Core', 'mzStory_canonical');
  Add_Filter_Plugin('Filter_Plugin_ViewComment_Template', 'mzStory_ViewCMT');
  Add_Filter_Plugin('Filter_Plugin_Zbp_BuildTemplate', 'mzStory_GenTpl');
}

function mzStory_AddMenu(&$m)
{
  global $zbp;
  $m[] = MakeTopMenu("root", '<b>主题配置</b>', $zbp->host . "zb_users/theme/mzStory/main.php", "", "mzStory");
}

function mzStory_canonical()
{
  global $zbp;
  $fpargs = func_get_args();
  $url = $fpargs[7]->UrlRule;
  $zbp->fullcurrenturl = $url->Make();
}

// 评论并没有用传统的嵌套模式，看不懂的话可以不用
function mzStory_ViewCMT(&$template)
{
  global $zbp;
  $type = $template->GetTags('type');
  // $comments = $template->GetTags('comments');
  $comment = $template->GetTags('comment');
  if ($comment->ParentID > 0) {
    $MizuPaN = array();
    $commentParent = $zbp->GetCommentByID($comment->ParentID);
    $MizuPaN[$comment->ParentID] = $commentParent->Author->StaticName;
    $template->SetTags('MizuPaN', $MizuPaN);
    $template->SetTemplate("comment-reply");
  }
}

function mzStory_GenTpl(&$templates)
{
  global $zbp;
  $templates['n-slide'] = "";
  $templates['n-hot'] = "";
  $templates['n-cms-cate-focus'] = "";
  $uFile = mzStory_Path("u-slide");
  // 幻灯片生成
  if ($zbp->template->HasTemplate("m-slide") && is_file($uFile)) {
    $obj = json_decode(file_get_contents($uFile));
    $zbp->template->SetTags('slides', $obj->slides);
    $templates['n-slide'] = $zbp->template->Output("m-slide");
    $templates['n-slide'] .= <<<html
    {php}
      \$footer .= "<script src=\"{\$zbp->host}zb_users/theme/mzStory/swiper/swiper.min.js\"></script>\n";
      \$footer .= "<!-- {\$zbp->host}zb_users/theme/mzStory/swiper/doc.html -->\n";
    {/php}
    html;
    // $templates['n-slide'] .= '{php}$footer .= \'<script src="' . $zbp->host . 'zb_users/theme/mzStory/swiper/swiper-act.js"></script>\'{/php}';
  }
  // 热门文章获取
  // $zbp->Config('mzStory')->focusNums = 18;
  // $zbp->SaveConfig('mzStory');
  if ($zbp->template->HasTemplate("p-mod-list")) {
    $templates['n-hot'] = mzStory_GenHot($zbp->config("mzStory")->focusNums);
  }
}

function mzStory_Path($file, $t = 'path')
{
  global $zbp;
  $result = $zbp->$t . 'zb_users/theme/mzStory/';
  switch ($file) {
    case 'u-logo':
      return $result . 'usr/logo.png';
      break;
    case 'v-logo':
      return $result . 'var/logo.png';
      break;
    case 'u-fav':
      return $result . 'usr/favicon.ico';
      break;
    case 'v-fav':
      return $result . 'var/favicon.ico';
      break;
    case 'u-slide':
      return $result . 'usr/slide.json';
      break;
    case 'v-slide':
      return $result . 'var/slide.json';
      break;
    case 'usr':
      return $result . 'usr/';
      break;
    case 'var':
      return $result . 'var/';
      break;
    case 'main':
      return $result . 'main.php';
      break;
    default:
      return $result . $file;
  }
}

// 热门文章
function mzStory_GenHot($nums, $cateID = null)
{
  global $zbp;
  $w = array();
  $w[] = array('=', 'log_Type', 0);
  if (is_numeric($cateID)){
    $w[] = array('=', 'log_CateID', $cateID);
  }
  $order = array('log_ViewNums' => 'DESC');
  $limit = $nums;

  $articles = $zbp->GetArticleList(
    array('*'),
    $w,
    $order,
    $limit,
    null
  );
  $zbp->template->SetTags('articles', $articles);
  $rlt = $zbp->template->Output("p-mod-list");
  return $rlt;
}

function InstallPlugin_mzStory()
{
  global $zbp;

  $zbp->BuildTemplate();

  $zbp->LoadCategories();
  $zbp->AddBuildModule("catalog");
  // $zbp->AddBuildModule("tags");
  $zbp->BuildModule();

  $mod = $zbp->modulesbyfilename["tags"];
  $mod->Type = "div";
  $mod->Build();
  $mod->Save();

  // 初始化图标、logo
  $filesList = array("logo", "fav", "slide");
  foreach ($filesList as $key => $value) {
    $uFile = mzStory_Path("u-{$value}");
    $vFile = mzStory_Path("v-{$value}");
    if (!is_file($uFile)) {
      @mkdir(dirname($uFile));
      copy($vFile, $uFile);
    }
  }
  // 初始化主题配置
  if (!$zbp->HasConfig('mzStory')) {
    $zbp->Config('mzStory')->version = 1;
    $zbp->Config('mzStory')->description = "请在[主题配置]中设置首页描述";
    $zbp->Config('mzStory')->keywords = "";
    $zbp->Config('mzStory')->focusNums = 10;
    $zbp->SaveConfig('mzStory');
  }
}

function UninstallPlugin_mzStory()
{
  global $zbp;
  // 切换其他主题时恢复tag模块类型
  $mod = $zbp->modulesbyfilename["tags"];
  $mod->Type = "ul";
  $mod->Build();
  $mod->Save();
}
