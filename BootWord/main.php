<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';

$zbp->Load();
$action = 'root';
if (!$zbp->CheckRights($action)) {
  $zbp->ShowError(6);
  die();
}
if (!$zbp->CheckPlugin('BootWord')) {
  $zbp->ShowError(48);
  die();
}
$blogtitle = 'Bootstrap主题配置';

if (GetVars("act", "GET") === "save") {
  $zbp->Config('BootWord')->KeyWords = $_POST['KeyWords'];
  $zbp->Config('BootWord')->description = $_POST['description'];
  $zbp->Config('BootWord')->SNS = $_POST['SNS'];
  $zbp->Config('BootWord')->ME = str_replace('{$host}', $bloghost, $_POST['ME']);
  $zbp->SaveConfig('BootWord');
  $zbp->SetHint('good');
  Redirect("main.php");
}
$zbp->Config('BootWord')->ME = str_replace($bloghost, '{$host}', $zbp->Config('BootWord')->ME);

require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
InstallPlugin_BootWord();
?>

<style>
  .divHeader {
    background-position: 0 11px !important;
  }

  textarea {
    padding: 5px
  }
</style>
<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle; ?></div>
  <div class="SubMenu">
    <a href="main.php"><span class="m-left m-now">首页</span></a>
    <?php require "about.php" ?>
  </div>
  <div id="divMain2">
    <form method="post" action="main.php?act=save">
      <table class="tableFull tableBorder">
        <tbody>
          <tr class="color1">
            <th scope="col" height="32" width="150px">配置项</th>
            <th scope="col">配置内容</th>
          </tr>
          <tr class="color3">
            <th scope="row">关键词</th>
            <td><textarea style="width:98%" name="KeyWords" rows="2"><?php echo $zbp->Config('BootWord')->KeyWords; ?></textarea></td>
          </tr>
          <tr class="color2">
            <th scope="row">描述</th>
            <td><textarea style="width:98%" name="description" rows="2"><?php echo TransferHTML($zbp->Config('BootWord')->description, '[html-format]'); ?></textarea></td>
          </tr>
          <tr class="color3">
            <th scope="row">SNS</th>
            <td>
              <textarea style="width:98%" name="SNS" rows="3"><?php echo $zbp->Config('BootWord')->SNS; ?></textarea>
            </td>
          </tr>
          <tr class="color2">
            <th scope="row">关于我</th>
            <td><textarea onfocus="this.style.height='47px';this.style.height = this.scrollHeight + 'px'" style="width:98%" name="ME" rows="3"><?php echo TransferHTML($zbp->Config('BootWord')->ME, '[html-format]'); ?></textarea></td>
          </tr>
        </tbody>
      </table>
      <input value="提交" class="button" name="ok" type="submit" />
    </form>
  </div>
</div>
<script type="text/javascript">
  ActiveTopMenu("topmenu_BootWord");
</script>
<script type="text/javascript">
  AddHeaderIcon("<?php echo $bloghost . 'zb_users/theme/BootWord/screenshot.png'; ?>");
</script>
<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
