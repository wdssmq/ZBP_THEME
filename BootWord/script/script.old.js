﻿$(function() {
  // 响应式视频
  $("embed,object").parent().addClass("embed-responsive embed-responsive-4by3");
  // 侧栏跟随
  var $sidebar = $(".sidebar .widget:last-of-type"),
    offsetTop = 0,
    copyTop = 0,
    scrollt = 0;
  $(window).scroll(function() {
    scrollt = $(window).scrollTop();
    if (scrollt < 150) {
      $(".backTop").fadeOut("slow");
    } else {
      $(".backTop").fadeIn("slow");
    }
    if ($(window).width() < 768) return false;
    if (offsetTop == 0) {
      offsetTop = $sidebar.offset().top;
      copyTop = $(".copy").offset().top;
    }
    if (scrollt + $sidebar.height() + 380 > copyTop) {
      $sidebar.stop().animate({
        marginTop: copyTop - 380 - offsetTop - $sidebar.height() + 20,
      });
      return false;
    }
    if (scrollt > offsetTop) {
      $sidebar.stop().animate({
        marginTop: scrollt - offsetTop + 20,
      });
    } else {
      $sidebar.stop().animate({
        marginTop: 10,
      });
    }
    return false;
  });

  // backTop
  $(".backTop").click(function() {
    $("html,body").animate(
      {
        scrollTop: "0px",
      },
      500,
    );
    return false;
  });
});
