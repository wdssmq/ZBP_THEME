﻿$(function() {
  // 响应式视频
  $("embed,object").parent().addClass("embed-responsive embed-responsive-4by3");
  // 侧栏跟随
  let $sidebar = $("aside.sidebar .widget:last-of-type"),
    offsetTop = 0,
    // copyTop = 0,
    scrollt = 0;
  $(window).scroll(function() {
    if ($sidebar.length === 0) return;
    scrollt = $(window).scrollTop();
    if (scrollt >= 137) {
      $("#fixed-box,.backTop").addClass("is-active");
    } else {
      $("#fixed-box,.backTop").removeClass("is-active");
    }
    // console.log(scrollt);
    if ($(window).width() < 768) return false;
    if (offsetTop == 0) {
      offsetTop = $sidebar.offset().top;
    }
    let copyTopN = $(".footer").offset().top;
    if (scrollt + $sidebar.outerHeight() > copyTopN - 137) {
      let marTop = parseInt($sidebar.css("marginTop"));
      let rdyTop = copyTopN - offsetTop - $sidebar.outerHeight() - 137;
      $sidebar.stop().animate({
        marginTop: marTop > rdyTop ? marTop : rdyTop,
      });
      return false;
    }
    if (scrollt > offsetTop) {
      $sidebar.stop().animate({
        marginTop: scrollt - offsetTop + 45,
      });
    } else {
      $sidebar.stop().animate({
        marginTop: 15,
      });
    }
    return false;
  });
  $(".backTop").click(function() {
    return (
      $("html,body")
        .animate(
          {
            scrollTop: "0px",
          },
          500,
        )
        .fadeIn(),
      !1
    );
  });
});
