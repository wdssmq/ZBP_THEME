<?php
RegisterPlugin("BootWord", "ActivePlugin_BootWord");
function ActivePlugin_BootWord()
{
  Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu', 'BootWord_AddMenu');
  Add_Filter_Plugin('Filter_Plugin_ViewPost_Template', 'BootWord_CP');
  Add_Filter_Plugin('Filter_Plugin_ViewList_Template', 'BootWord_CP');
}
function BootWord_CP(&$template)
{
  global $zbp;
  $copyright = $template->GetTags('copyright') . " | Powered By " . $template->GetTags('zblogphpabbrhtml');
  $template->SetTags('copyright', $copyright);
}
function BootWord_AddMenu(&$m)
{
  global $zbp;
  $m[] = MakeTopMenu("root", '主题配置', $zbp->host . "zb_users/theme/BootWord/main.php", "", "topmenu_BootWord");
}
function InstallPlugin_BootWord()
{
  global $zbp;
  if (!$zbp->HasConfig('BootWord')) {
    $zbp->Config('BootWord')->KeyWords    = '自定义关键词,半角逗号分隔';
    $zbp->Config('BootWord')->description = '自定义描述';
    $zbp->Config('BootWord')->SNS         = '<a href="https://weibo.com/wdssmq" title="新浪微博" target="_blank" rel="nofollow"><i class="icon-sina"></i></a><a href="/feed.php" title="FEED订阅" target="_blank" rel="nofollow"><i class="icon-rss"></i></a>';
    $zbp->Config('BootWord')->ME          = '<p><img src="{$host}zb_users/avatar/0.png" height="40" width="40" alt="沉冰浮水"> 沉冰浮水 <a class="btn btn-default btn-xs nosw" title="给我留言" href="{$host}?id=2">留言本</a><br>置百丈玄冰而崩裂，掷须臾池水而漂摇。<br>QQ：<a href="http://wpa.qq.com/msgrd?v=3&uin=349467624&site=qq&menu=yes" title="QQ" target="_blank">349467624 <img src="//pub.idqqimg.com/qconn/wpa/button/button_11.gif" alt="QQ" title="QQ"></a><br>GitHub：<a href="https://github.com/wdssmq" title="GitHub" target="_blank">https://github.com/wdssmq</a></p>';
    $zbp->Config('BootWord')->ME          = str_replace('{$host}', $zbp->host, $zbp->Config('BootWord')->ME);
    $zbp->SaveConfig('BootWord');
  }
  $mod          = $zbp->GetModuleByFileName("searchpanel");
  $mod->Content = '<form class="form-inline" action="' . $zbp->host . 'zb_system/cmd.php?act=search" method="post"><div class="form-group"><label for="search" class="sr-only">search</label><input class="form-control" id="search" name="q" placeholder="搜索" type="text"></div> <input class="btn btn-default" value="搜索" type="submit"></form>';
  $mod->Save();
  // var_dump($mod->GetData());
}
function UninstallPlugin_BootWord()
{
  global $zbp;
  $mod          = $zbp->GetModuleByFileName("searchpanel");
  $mod->Content = '<form name="search" method="post" action="' . $zbp->host . 'zb_system/cmd.php?act=search"><input type="text" name="q" size="11" /> <input type="submit" value="搜索" /></form>';
  $mod->Save();
  // $zbp->DelConfig('BootWord');
}
