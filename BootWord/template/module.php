<section class="box widget clearfix" id="{$module.HtmlID}">
  {if (!$module.IsHideTitle)&&($module.Name)}
  <h4 class="box-title">{$module.Name}</h4>
  {/if}
  {if $module.Type=='div'}
  <div>{$module.Content}</div>
  {/if}
  {if $module.Type=='ul'}
  <ul>{$module.Content}</ul>
  {/if}
</section>
