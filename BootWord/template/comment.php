<div class="media box" id="cmt{$comment.ID}">
  <span class="msgname pull-left"><img class="avatar" src="{$comment.Author.Avatar}" alt="{$comment.Author.StaticName}" width="54" /></span>
  <div class="media-body">
    <b class="commentname"><a href="{$comment.Author.HomePage}" rel="nofollow" target="_blank" title="{$comment.Author.StaticName}">{$comment.Author.StaticName}</a></b>
    {if isset($comment.c2rTips)}{$comment.c2rTips}{/if}
    <br />
    <small>发布于&nbsp;{$comment.Time()}&nbsp;&nbsp;</small><a class="color" href="#respond" onclick="zbp.comment.reply('{$comment.ID}')" title="回复">回复</a>
    <div class="msgarticle">{$comment.Content}</div>
    {foreach $comment.Comments as $comment}{template:comment}{/foreach}
  </div>
</div>
