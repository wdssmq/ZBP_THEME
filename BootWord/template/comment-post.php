<div class="box" id="divCommentPost">
  <h3 class="box-title" id="respond">{if $user.ID>0}{$user.StaticName}{/if}发表评论:<a rel="nofollow" id="cancel-reply" href="#divCommentPost" style="display:none;" class="color"><small>取消回复</small></a></h3>
  <form id="frmSubmit" target="_self" method="post" action="{$article.CommentPostUrl}">
    <input type="hidden" name="inpId" id="inpId" value="{$article.ID}" />
    <input type="hidden" name="inpRevID" id="inpRevID" value="0" />
    {if $user.ID>0}
    <input type="hidden" name="inpName" id="inpName" value="{$user.Name}" />
    <input type="hidden" name="inpEmail" id="inpEmail" value="{$user.Email}" />
    <input type="hidden" name="inpHomePage" id="inpHomePage" value="{$user.HomePage}" />
    {else}
    <label for="inpName">名称(*)</label>
    <input type="text" name="inpName" id="inpName" class="form-control" value="{$user.Name}" size="33" tabindex="1" />
    <label for="inpEmail">邮箱</label>
    <input type="text" name="inpEmail" id="inpEmail" class="form-control" value="{$user.Email}" size="33" tabindex="2" />
    <label for="inpHomePage">网址</label>
    <input type="text" name="inpHomePage" id="inpHomePage" class="form-control" value="{$user.HomePage}" size="33" tabindex="3" />
    {if $option['ZC_COMMENT_VERIFY_ENABLE']}
    <label for="inpVerify">验证码(*)</label>
    <div><input type="text" name="inpVerify" id="inpVerify" class="form-control" value="" size="33" tabindex="4" />
      <img width="{$option['ZC_VERIFYCODE_WIDTH']}" height="['ZC_VERIFYCODE_HEIGHT']}" src="{$article.ValidCodeUrl}" alt="验证码" title="验证码" onclick="javascript:this.src='{$article.ValidCodeUrl}&amp;tm='+Math.random();" />
    </div>
    {/if}
    {if $zbp->CheckPlugin('xnxf_geetest')}
    {$geetest}
    {/if}
    {/if}
    <label for="txaArticle">正文(*)</label>
    <textarea name="txaArticle" id="txaArticle" class="form-control" cols="60" rows="5" tabindex="5"></textarea>
    <input name="submit" type="submit" tabindex="6" value="提交" onclick="return zbp.comment.post();" class="btn btn-default" />
  </form>
  <p class="postbottom">◎欢迎参与讨论，请在这里发表您的看法、交流您的观点。</p>
</div>
