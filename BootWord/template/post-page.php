<article class="post auth{$article.Author.ID} box">
  <h2 class="post-title">{$article.Title}</h2>
  <div class="post-meta-wrap clearfix">
    <p class="post-meta pull-right"><i class="glyphicon glyphicon-calendar"></i> <time>{$article.Time('Y年m月d日')}</time> - <i class="glyphicon glyphicon-user"></i> <a href="{$article.Author.Url}" title="{$article.Author.StaticName}"> {$article.Author.StaticName}</a> - <i class="glyphicon glyphicon-comment"></i> <a href="{$article.Url}#respond" title="发表评论">{$article.CommNums} 条留言</a> - <i class="glyphicon glyphicon-eye-open"></i> {$article.ViewNums} 次浏览</p>
  </div>
  <div class="post-body">{$article.Content}</div>
  <hr>
  <p class="post-link"><i class="glyphicon glyphicon-hand-right"></i> <b>原文链接：</b><a href="{$article.Url}" title="{$article.Title}">{$article.Url}</a></p>
</article>

{if !$article.IsLock}
{template:comments}
{/if}
