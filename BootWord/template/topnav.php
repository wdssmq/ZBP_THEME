<!-- Header start -->
<header>
  <div class="container">
    <div class="row">

      <div class="logo col-sm-6">
        <h1 id="BlogTitle"><a href="{$host}" title="{$name}">{$name}</a></h1>
        <h2 class="h4" id="BlogSubTitle">{$subname}</h2>
      </div>
      <div class="contact col-sm-4 col-sm-offset-2">{$zbp->Config('BootWord')->SNS}</div>

    </div>
  </div>
</header>
<!-- Header end -->

<!-- Navbar -->
<nav class="navbar navbar-inverse">
  <div class="container">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-list" aria-expanded="false">
      <span class="sr-only">导航折叠切换</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <div class="clearfix"></div>
    <div class="collapse navbar-collapse" id="navbar-list">
      <ul class="nav navbar-nav noborder">{module:navbar}</ul>
    </div>
  </div>
</nav>
<!-- Navbar end -->
