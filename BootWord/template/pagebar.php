{if $pagebar}
<ul class="pagination">
  {foreach $pagebar.buttons as $k=>$v}
  <li>
    {if $pagebar.PageNow==$k}
    <span class="page now-page active">{$k}</span>
    {else}
    <a href="{$v}" title="{$k}"><span class="page">{$k}</span></a>
    {/if}
  </li>
  {/foreach}
</ul>
{/if}
