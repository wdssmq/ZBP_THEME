<footer class="footer">
  <div class="container">
    <!--row start-->
    <div class="row">
      <div class="col-sm-4">
        <div class="box">
          <h4 class="box-title">搜索</h4>
          <div>
            <form method="get" action="{$host}search.php" class="form-inline">
              <div class="form-group"><label class="sr-only" for="search">search</label><input type="text" placeholder="搜索" name="q" class="form-control"></div> <input type="submit" value="搜索" class="btn btn-default">
            </form>
          </div>
        </div>
        {template:sidebar2}
      </div>
      <div class="col-sm-4">{template:sidebar3}</div>
      <div class="col-sm-4">
        <div class="box">
          <h4 class="box-title">关于我</h4>
          <div class="about">{$zbp->Config('BootWord')->ME}</div>
        </div>
      </div>
    </div>
    <!--row end-->
    <hr>
    <p class="copy">{$copyright}</p>
  </div>
</footer>
<div class="backTop"><a href="javascript:;"><span class="sr-only">返回顶部</span></a></div>
<script src="{$host}zb_system/script/jquery-2.2.4.min.js?v=168450727"></script>
<script src="{$host}zb_system/script/zblogphp.js?v=168450727"></script>
<script src="{$host}zb_system/script/c_html_js_add.php"></script>
<script src="{$host}zb_users/theme/BootWord/script/script.min.js?v=168450727"></script>
{$footer}
