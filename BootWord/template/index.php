<!DOCTYPE html>
<html lang="{$language}" xml:lang="{$language}">

<head>
  {template:header}
</head>

<body class="{$type}">
  {template:topnav}
  <div class="blog container">
    <div class="row">

      <div class="posts col-sm-8">
        {foreach $articles as $article}
        {if $article.IsTop}
        {template:post-istop}
        {else}
        {template:post-multi}
        {/if}
        {/foreach}
        <div class="clearfix">{template:pagebar}</div>
      </div>

      <aside class="sidebar col-sm-4">{template:sidebar}</aside>

    </div>
  </div>
  {template:footer}
</body>

</html>
