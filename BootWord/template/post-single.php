<article class="post cate{$article.Category.ID} auth{$article.Author.ID} box">
  <h2 class="post-title">{$article.Title}</h2>
  <div class="post-meta-wrap clearfix">
    <p class="post-meta pull-right"><i class="glyphicon glyphicon-calendar"></i> <time>{$article.Time('Y年m月d日')}</time> - <i class="glyphicon glyphicon-folder-open"></i> <a href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a> - <i class="glyphicon glyphicon-user"></i> <a href="{$article.Author.Url}" title="{$article.Author.StaticName}"> {$article.Author.StaticName}</a> - <i class="glyphicon glyphicon-comment"></i> <a href="{$article.Url}#respond" title="发表评论">{$article.CommNums} 条留言</a> - <i class="glyphicon glyphicon-eye-open"></i> {$article.ViewNums} 次浏览</p>
  </div>
  <div class="post-body">{$article.Content}</div>
  <hr>
  <p class="post-link"><i class="glyphicon glyphicon-hand-right"></i> <b>原文链接：</b><a href="{$article.Url}" title="{$article.Title}">{$article.Url}</a></p>
  {if $article.Tags}
  <p class="post-tags"><i class="glyphicon glyphicon-tags"></i> <b>Tags：</b>{foreach $article.Tags as $tag}<a href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>{/foreach}</p>
  {/if}
</article>

<nav class="post-pager box">
  <ul class="pager">
    {if $article.Prev}
    <li class="previous"><a href="{$article.Prev.Url}" title="上一篇 {$article.Prev.Title}">{$article.Prev.Title}</a></li>
    {/if}
    {if $article.Next}
    <li class="next"><a href="{$article.Next.Url}" title="下一篇 {$article.Next.Title}">{$article.Next.Title}</a></li>
    {/if}
  </ul>
</nav>
<section class="box widget clearfix" id="divMutuality">
  <h3 class="box-title">相关文章</h3>
  <ul>
    {foreach GetList(12,$article.Category.ID) as $related}
    <li><a href="{$related.Url}" title="{$related.Title}">{$related.Title}</a>
      <!--(<span>{$related.Time('Y-m-d')}</span>)-->
    </li>
    {/foreach}
  </ul>
</section>

{if !$article.IsLock}
{template:comments}
{/if}
