<section class="post box cate{$article.Category.ID}  auth{$article.Author.ID} clearfix">
  <h2 class="post-title"><a href="{$article.Url}" rel="bookmark" title="{$article.Title}">{$article.Title}</a></h2>
  <div class="post-body">{$article.Intro}</div>
  <!-- <hr> -->
  <footer>
    <!-- {if $article.Tags}
    <i class="glyphicon glyphicon-tags"></i> {foreach $article.Tags as $tag}<a href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>{/foreach}
    {/if} -->
    <div class="post-meta-wrap">
      <p class="post-meta"><i class="glyphicon glyphicon-calendar"></i> {$article.Time('Y年m月d日')} - <i class="glyphicon glyphicon-folder-open"></i> <a href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a> - <i class="glyphicon glyphicon-user"></i> <a href="{$article.Author.Url}" title="{$article.Author.StaticName}"> {$article.Author.StaticName}</a> - <i class="glyphicon glyphicon-comment"></i> <a href="{$article.Url}#respond" title="{$article.Title}">{$article.CommNums}条留言</a> - <i class="glyphicon glyphicon-eye-open"></i> {$article.ViewNums} 次浏览 <a href="{$article.Url}" title="查看全文" class="pull-right readmore">更多 &raquo;</a></p>
    </div>
  </footer>
</section>
