$(function() {
  $("embed,object")
    .parent()
    .addClass("embed-responsive embed-responsive-4by3");
  // 侧栏跟随
  var $sidebar = $(".sidebar .widget:last-of-type"),
    offsetTop = 0,
    copyTop = 0,
    scrollt = 0;
  $(window).scroll(function() {
    if ($sidebar.length === 0) return;
    scrollt = $(window).scrollTop();
    if ($(window).width() < 768) return false;
    if (offsetTop == 0) {
      offsetTop = $sidebar.offset().top;
      copyTop = $(".copy").offset().top;
    }
    if (scrollt + $sidebar.height() + 120 > copyTop) {
      $sidebar.stop().animate({
        marginTop: copyTop - 120 - offsetTop - $sidebar.height() + 45,
      });
      return false;
    }
    if (scrollt > offsetTop - $sidebar.height() * 2 - 120) {
      $(".backTop").fadeIn();
    } else {
      $(".backTop").fadeOut();
    }
    if (scrollt > offsetTop) {
      $sidebar.stop().animate({
        marginTop: scrollt - offsetTop + 45,
      });
    } else {
      $sidebar.stop().animate({
        marginTop: 15,
      });
    }
    return false;
  });
  $(".backTop").click(function() {
    return (
      $("html,body")
        .animate(
          {
            scrollTop: "0px",
          },
          500,
        )
        .fadeIn(),
      !1
    );
  });
  $.fn.flash = function(a) {
    $(this).animate(
      {
        opacity: "toggle",
      },
      500,
      function() {
        "none" == $(this).css("display") && $(this).flash(0);
      },
    ),
    a > 1 && $(this).flash(a - 1);
  };
  var s = document.location;
  $("#divNavBar a").each(function() {
    if (this.href == s.toString().split("#")[0]) {
      $(this).addClass("on");
      return false;
    }
  });
});
