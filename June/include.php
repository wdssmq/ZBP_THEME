<?php
RegisterPlugin("June", "ActivePlugin_June");
function ActivePlugin_June()
{
  Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu', 'June_AddMenu');
  // Add_Filter_Plugin('Filter_Plugin_ViewPost_Template', 'June_CP');
  // Add_Filter_Plugin('Filter_Plugin_ViewList_Template', 'June_CP');
}
// function June_CP(&$template)
// {
//   global $zbp;
//   $copyright = $template->GetTags('copyright') . "<br>Powered By " . $template->GetTags('zblogphpabbrhtml');
//   $template->SetTags('copyright', $copyright);
// }
function June_AddMenu(&$m)
{
  global $zbp;
  $m[] = MakeTopMenu("root", '主题配置', $zbp->host . "zb_users/theme/June/main.php", "", "topmenu_June");
}
function InstallPlugin_June()
{
  global $zbp;
  if (!$zbp->HasConfig('June')) {
    $zbp->Config('June')->KeyWords    = '自定义关键词,半角逗号分隔';
    $zbp->Config('June')->description = '自定义描述';
    $zbp->SaveConfig('June');
  }
  $mod          = $zbp->GetModuleByID(5);
  $mod->Content = '<form class="form-inline" action="' . $zbp->host . 'zb_system/cmd.php?act=search" method="post"><div class="form-group"><label for="search" class="sr-only">search</label><input class="form-control" id="search" name="q" placeholder="搜索" type="text"></div> <input class="btn btn-default" value="搜索" type="submit"></form>';
  $mod->Save();
}
function UninstallPlugin_June()
{
  global $zbp;
  $mod          = $zbp->GetModuleByID(5);
  $mod->Content = '<form name="search" method="post" action="' . $zbp->host . 'zb_system/cmd.php?act=search"><input type="text" name="q" size="11" /> <input type="submit" value="搜索" /></form>';
  $mod->Save();
  //$zbp->DelConfig('June');
}
?>
