<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';

$zbp->Load();
$action='root';
if (!$zbp->CheckRights($action)) {$zbp->ShowError(6);die();}
if (!$zbp->CheckPlugin('June')) {$zbp->ShowError(48);die();}
$blogtitle='June主题配置';

require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';

$act = GetVars('act', 'GET');
$suc = GetVars('suc', 'GET');
if (GetVars('act', 'GET') == 'save') {
  CheckIsRefererValid();
	$zbp->Config('June')->KeyWords = $_POST['KeyWords'];
	$zbp->Config('June')->description = $_POST['description'];
  $zbp->SaveConfig('June');
  $zbp->BuildTemplate();
  $zbp->SetHint('good');
  Redirect('./main.php' . ($suc == null ? '' : '?act=$suc'));
}
?>

<style>
.divHeader { background-position:0 11px !important; }
</style>


<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle;?></div>
  <div class="SubMenu"><?php require 'about.php';?></div>
  <div id="divMain2">
  <form method="post" action="<?php echo BuildSafeURL('main.php?act=save'); ?>">
    <table class="tableFull tableBorder tableBorder-thcenter">
      <tbody>
        <tr class="color1">
          <th scope="col" height="32" width="150px">配置项</th>
          <th scope="col">配置内容</th>
        </tr>
        <tr class="color3">
          <th scope="row">关键词</th>
          <td><textarea style="width:98%" name="KeyWords" rows="2"><?php echo $zbp->Config('June')->KeyWords;?></textarea></td>
        </tr>
        <tr class="color2">
          <th scope="row">描述</th>
          <td><textarea style="width:98%" name="description" rows="2"><?php echo $zbp->Config('June')->description;?></textarea></td>
        </tr>
      </tbody>
    </table>
    <input value="提交" class="button" name="ok" type="submit"/>
  </form>
  </div>

</div>
<script type="text/javascript">ActiveTopMenu("topmenu_June");</script>
<script type="text/javascript">AddHeaderIcon("<?php echo $bloghost . 'zb_users/theme/June/screenshot.png';?>");</script>
<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
