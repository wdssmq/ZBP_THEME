<!DOCTYPE html>
<html lang="{$language}" xml:lang="{$language}">
<head>
  {template:header}
</head>
<body class="{$type}">
  {template:topnav}
  <div class="main container">
    <div class="row">
      <div class="posts col-sm-8">
        {if $article.Type==ZC_POST_TYPE_ARTICLE}
        {template:post-single}
        {else}
        {template:post-page}
        {/if}
      </div>
      <aside class="sidebar col-sm-4">{template:sidebar}</aside>
    </div>
  </div>
  {template:footer}
</body>
</html>
