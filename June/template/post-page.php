<article class="post clearfix auth{$article.Author.ID}">
  <h2 class="post-title">{$article.Title}</h2>
{if $zbp.action !== "search"}
  <p class="post-meta"> Posted by <a href="{$article.Author.Url}" title="{$article.Author.StaticName}"> {$article.Author.StaticName}</a> | {$article.Time('Y-m-d')}</p>
{/if}
  <div class="post-body">{$article.Content}</div>
{if $zbp.action !== "search"}
  <p class="post-meta pull-right"> <a href="{$article.Url}#respond" title="{$article.Title}">{$article.CommNums}条留言</a> | {$article.ViewNums} 次浏览</p>
{/if}
</article>

{if !$article.IsLock}
{template:comments}
{/if}
