{if $pagebar}
{foreach $pagebar.buttons as $k=>$v}
  {if $pagebar.PageNow==$k}
    <a class="page now-page" href="{$v}" title="{$k}">{$k}</a>
  {else}
    <a class="page" href="{$v}" title="{$k}">{$k}</a>
  {/if}
{/foreach}
{/if}