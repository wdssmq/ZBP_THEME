<!DOCTYPE html>
<html lang="{$language}" xml:lang="{$language}">
<head>
  {template:header}
</head>
<body class="{$type}">
  {template:topnav}
  <div class="main container">
    <div class="row">
      <div class="posts col-sm-8">
        {foreach $articles as $article}
        {if $article.IsTop}
        {template:post-istop}
        {else}
        {template:post-multi}
        {/if}
        {/foreach}
        {if count($articles) == 0}
        <section class="post clearfix">
          <h3 class="post-title">当前列表为空</h3>
          <p>
            <a href="{$host}" title="返回首页">返回首页</a>
            <span>或者</span>
            <a href="javascript:;" onclick="history.go(-1);" title="返回上页">返回上页</a>
          </p>
        </section>
        {/if}
        <div class="pagination clearfix">{template:pagebar}</div>
      </div>
      <aside class="sidebar col-sm-4">{template:sidebar}</aside>
    </div>
  </div>
  {template:footer}
</body>
</html>
