<header>
  <nav class="navbar navbar-default">
    <div class="container">
      <ul class="nav navbar-nav navbar-toggle"><li><a href="{$host}" title="{$name}">首页</a></li></ul>
      <div class="clearfix"></div>
      <div class="collapse navbar-collapse text-center">
        <ul class="nav navbar-nav none-border">{module:navbar}</ul>
      </div>
    </div>
  </nav>
  <div class="container text-center">
      <h1 id="BlogTitle"><a href="{$host}" title="{$name}">{$name}</a></h1>
      <h3 id="BlogSubTitle"><span>{$subname}</span></h3>
  </div>
</header>


