{php}
$title = 404;
{/php}
{template:header}
<body class="{$type}">
{template:topnav}
<div class="container">
<div class="styl-404">
<div class="row">
<div style="padding-top: 5%;" class="col-sm-5 col-md-4 text-center col-sm-offset-1 col-md-offset-2">
<h2>404 - {$name}</h2>
<p>当前请求的页面无法浏览或不存在</p>
<p>
<a href="{$host}" title="返回首页">返回首页</a>
<span>或者</span>
<a href="javascript:;" id="js-history-back" title="返回上页">返回上页</a>
</p>
</div>
<div class="col-sm-5 col-md-4 text-center">
<img src="{$host}zb_users/theme/{$theme}/style/images/illus.png" alt="404" />
</div>
</div>
</div>
</div>
{template:footer}
<script type="text/javascript">
(function() {
  var backButton = document.getElementById('js-history-back')
  backButton.onclick = function() {
    history.go(-1)
  }
}());
</script>