<!-- <div class="pager">
  {if $article.Prev}<a class="pull-left" href="{$article.Prev.Url}"  title="上一篇 {$article.Prev.Title}">{$article.Prev.Title}</a>{/if}
  {if $article.Next}<a class="pull-right" href="{$article.Next.Url}" title="下一篇 {$article.Next.Title}">{$article.Next.Title}</a>{/if}
  <div class="clearfix"></div>
</div> -->

<article class="post clearfix cate{$article.Category.ID} auth{$article.Author.ID}">
  <h2 class="post-title">{$article.Title}</h2>
  <p class="post-meta"> Posted by <a href="{$article.Author.Url}" title="{$article.Author.StaticName}"> {$article.Author.StaticName}</a> | {$article.Time('Y-m-d')} | <a href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a> <span data-id="{$article.ID}" class="js-edt pull-right"></span></p>
  <div class="post-body">{$article.Content}</div>
  <p class="post-meta pull-right">{foreach $article.Tags as $tag}<a href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>&nbsp;{/foreach}| <a href="{$article.Url}#respond" title="{$article.Title}">{$article.CommNums}条留言</a> | {$article.ViewNums} 次浏览</p>
</article>

<section class="post">
  <h3 class="color-title">相关文章</h3>
  <ul class="list-unstyled">
    {foreach $article.RelatedList as $related}
      <li><a href="{$related.Url}" title="{$related.Title}">{$related.Title}</a> (<span>{$related.Time('Y-m-d')}</span>)</li>
    {/foreach}
  </ul>
  <div class="clearfix"></div>
</section>

{if !$article.IsLock}
{template:comments}
{/if}
