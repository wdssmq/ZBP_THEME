{if $comment.Author.HomePage == ""}{$comment.Author.HomePage = "#cmt" . $comment.ID}{/if}
<div class="media" id="cmt{$comment.ID}">
  <div class="media-hd clearfix">
    <span class="pull-right"><img class="avatar" src="{$comment.Author.Avatar}" alt="{$comment.Author.StaticName}" width="36"/></span>
    <div class="pull-left">
    <b class="cmt-name"><a href="{$comment.Author.HomePage}" rel="external nofollow noopener noreferrer" target="_blank" title="{$comment.Author.StaticName}">{$comment.Author.StaticName}</a></b><small>&nbsp;发布于&nbsp;{$comment.Time()}&nbsp;&nbsp;</small><br>
    {if isset($comment.Tips)}{$comment.Tips}{else}<a href="{$comment.Author.HomePage}" rel="external nofollow noopener noreferrer" target="_blank" title="{$comment.Author.StaticName}">{$comment.Author.HomePage}</a>{/if}
    </div>
  </div>
  <div class="media-body">
    <div class="cmt-content">{$comment.Content}</div>
    <span class="revertcomment"><a href="#respond" onclick="RevertComment('{$comment.ID}')" title="回复">回复</a></span>
    {foreach $comment.Comments as $comment}{template:comment}{/foreach}
  </div>
</div>