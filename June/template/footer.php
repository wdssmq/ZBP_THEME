<footer class="footer">
  <div class="container">
    <p class="copy text-center">{$copyright}<br>Powered By <a href="http://www.zblogcn.com/" title="RainbowSoft Z-BlogPHP" target="_blank">Z-BlogPHP</a> | THEME by <a href="http://www.fabthemes.com/June/" rel="nofollow" target="_blank" title="fabthemes">June</a> &amp; <a href="http://www.wdssmq.com" target="_blank" title="置百丈玄冰而崩裂，掷须臾池水而漂摇。" rel="nofollow">沉冰浮水</a><br></p>
  </div>
</footer>
<button class="backTop" title="回到顶部" type="button"><svg fill="currentColor" viewBox="0 0 24 24" width="24" height="24"><path d="M16.036 19.59a1 1 0 0 1-.997.995H9.032a.996.996 0 0 1-.997-.996v-7.005H5.03c-1.1 0-1.36-.633-.578-1.416L11.33 4.29a1.003 1.003 0 0 1 1.412 0l6.878 6.88c.782.78.523 1.415-.58 1.415h-3.004v7.005z"></path></svg></button>

<script src="{$host}zb_system/script/jquery-2.2.4.min.js?v=2230963045"></script>
<script src="{$host}zb_system/script/zblogphp.js?v=2230963045"></script>
<script src="{$host}zb_system/script/c_html_js_add.php"></script>

<script src="{$host}zb_users/theme/{$theme}/script/script.min.js?v=2230963045"></script>
{$footer}

