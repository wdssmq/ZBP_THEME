<section class="widget" id="{$module.HtmlID}">
  {if (!$module.IsHideTitle)&&($module.Name)}<h3>{$module.Name}</h3>{/if}
  {if $module.Type=='div'}<div>{$module.Content}</div>{/if}
  {if $module.Type=='ul'}<ul class="list-unstyled">{$module.Content}</ul>{/if}
</section>
