{php}
if (strpos($article->Intro, '<!--autointro-->') !== false){
  $article->Intro = preg_replace("/^((<p>((?!(<\/p>)).)+<\/p>){1,5}).+/",'${1}',$article->Content);
}
{/php}
<section class="post clearfix cate{$article.Category.ID}  auth{$article.Author.ID}">
  <h2 class="post-title"><a href="{$article.Url}" rel="bookmark" title="{$article.Title}">{$article.Title}</a></h2>
  <p class="post-meta"> Posted by <a href="{$article.Author.Url}" title="{$article.Author.StaticName}"> {$article.Author.StaticName}</a> | {$article.Time('Y-m-d')} | <a href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a></p>
  <div class="post-body">{$article.Intro}</div>
  <p class="post-meta pull-right">{foreach $article.Tags as $tag}<a href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>&nbsp;{/foreach} <a href="{$article.Url}#respond" title="{$article.Title}">{$article.CommNums}条留言</a> | {$article.ViewNums} 次浏览</p>
</section>
