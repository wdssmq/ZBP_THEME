## 投喂支持

爱发电：[https://afdian.com/a/wdssmq](https://afdian.com/a/wdssmq "沉冰浮水正在创作和 z-blog 相关或无关的各种有用或没用的代码 | 爱发电")

哔哩哔哩：[https://space.bilibili.com/44744006](https://space.bilibili.com/44744006 "沉冰浮水的个人空间\_哔哩哔哩\_bilibili")「投币或充电」「[大会员卡券领取 - bilibili](https://account.bilibili.com/account/big/myPackage "大会员卡券领取 - bilibili")」

RSS 订阅：[https://feed.wdssmq.com](https://feed.wdssmq.com "沉冰浮水博客的 RSS 订阅地址") 「[「言说」RSS 是一种态度！！](https://www.wdssmq.com/post/20201231613.html "「言说」RSS 是一种态度！！")」

在更多平台关注我：[https://www.wdssmq.com/guestbook.html#其他出没站点和信息](https://www.wdssmq.com/guestbook.html#%E5%85%B6%E4%BB%96%E5%87%BA%E6%B2%A1%E5%9C%B0%E7%82%B9%E5%92%8C%E4%BF%A1%E6%81%AF "在更多平台关注我")

更多「小代码」：[https://cn.bing.com/search?q=小代码+沉冰浮水](https://cn.bing.com/search?q=%E5%B0%8F%E4%BB%A3%E7%A0%81+%E6%B2%89%E5%86%B0%E6%B5%AE%E6%B0%B4 "小代码 沉冰浮水 - 必应搜索")

<!-- ##################################### -->

## 说明

又是一个远古坑，曾经还自用过一段时间；

主要重写了样式部分，模板文件部分可能有一些过时的地方；

用了最后一版使用 less 的 Bootstrap v3.4.0，v3.4.1 之后就换 sass 了；

> June · 沉冰浮水/ZBP\_THEME - 码云 - 开源中国
>
> [https://gitee.com/wdssmq/ZBP_THEME/tree/master/June](https://gitee.com/wdssmq/ZBP_THEME/tree/master/June "June · 沉冰浮水/ZBP\_THEME - 码云 - 开源中国")


## 更新日志

2024-05-19: 套用 Grunt 重构了，PurgeCSS 处理后干掉了 86% 的 CSS；Bootstrap 升级至 v3.4.0；

2019-08-18: 针对 Editor.md 的代码亮度调整，文章内表格样式；

2019-05-26: `<link rel="canonical" href="{$zbp.fullcurrenturl}" />`

2019-05-23: 自用调整；

2019-05-15: `https://validator.w3.org/` 欢迎反馈标准问题，会逐步修改；

2019-02-21: 合并了自用时的一些调整，然而这种大改肯定会出坑的吧。。肯定吧。。肯定。。

2016-12-07: 细节修改；

2014-10-19: 副标题强制不换行 + 超出隐藏。

## 图示

![001](<https://app.zblogcn.com/zb_users/upload/2014/10/201410191413684251983363.jpg> "001.jpg")

![002](<https://app.zblogcn.com/zb_users/upload/2014/10/201410191413684251806275.jpg> "002.jpg")

