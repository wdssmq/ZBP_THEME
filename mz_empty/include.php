<?php
#注册插件
RegisterPlugin("mz_empty", "ActivePlugin_mz_empty");

function ActivePlugin_mz_empty()
{
  Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu', 'mz_empty_AddMenu');
  Add_Filter_Plugin('Filter_Plugin_ViewList_Core', 'mz_empty_canonical');
  Add_Filter_Plugin('Filter_Plugin_ViewComment_Template', 'mz_empty_ViewCMT');
}

function mz_empty_AddMenu(&$m)
{
  global $zbp;
  $m[] = MakeTopMenu("root", '<b>主题配置</b>', $zbp->host . "zb_users/theme/mz_empty/main.php", "", "mz_empty");
}

function mz_empty_canonical()
{
  global $zbp;
  $fpargs = func_get_args();
  $url = $fpargs[7]->UrlRule;
  $zbp->fullcurrenturl = $url->Make();
}

// 评论并没有用传统的嵌套模式，看不懂的话可以不用
function mz_empty_ViewCMT(&$template)
{
  global $zbp;
  // $type = $template->GetTags('type');
  // $comments = $template->GetTags('comments');
  $comment = $template->GetTags('comment');
  if ($comment->ParentID > 0) {
    $MizuPaN = array();
    $commentParent = $zbp->GetCommentByID($comment->ParentID);
    $MizuPaN[$comment->ParentID] = $commentParent->Author->StaticName;
    $template->SetTags('MizuPaN', $MizuPaN);
    $template->SetTemplate("comment-reply");
  }
}

function mz_empty_Path($file, $t = 'path')
{
  global $zbp;
  $result = $zbp->$t . 'zb_users/theme/mz_empty/';
  switch ($file) {
    case 'u-logo':
      return $result . 'usr/logo.png';
      break;
    case 'u-fav':
      return $result . 'usr/favicon.ico';
      break;
    case 'v-logo':
      return $result . 'var/logo.png';
      break;
    case 'v-fav':
      return $result . 'var/favicon.ico';
      break;
    case 'usr':
      return $result . 'usr/';
      break;
    case 'var':
      return $result . 'var/';
      break;
    case 'main':
      return $result . 'main.php';
      break;
    default:
      return $result . $file;
  }
}

function InstallPlugin_mz_empty()
{
  global $zbp;

  $zbp->BuildTemplate();

  $zbp->LoadCategories();
  $zbp->AddBuildModule("catalog");
  // $zbp->AddBuildModule("tags");
  $zbp->BuildModule();

  $mod = $zbp->modulesbyfilename["tags"];
  $mod->Type = "div";
  $mod->Build();
  $mod->Save();

  // 初始化图标、logo
  $filesList = array("logo", "fav");
  foreach ($filesList as $key => $value) {
    $uFile = mz_empty_Path("u-{$value}");
    $vFile = mz_empty_Path("v-{$value}");
    if (!is_file($uFile)) {
      @mkdir(dirname($uFile));
      copy($vFile, $uFile);
    }
  }
  // 初始化主题配置
  if (!$zbp->HasConfig('mz_empty')) {
    $zbp->Config('mz_empty')->version = 1;
    $zbp->Config('mz_empty')->description = "请在[主题配置]中设置首页描述";
    $zbp->Config('mz_empty')->keywords = "";
    $zbp->SaveConfig('mz_empty');
  }
}

function UninstallPlugin_mz_empty()
{
  global $zbp;
  // 切换其他主题时恢复tag模块类型
  $mod = $zbp->modulesbyfilename["tags"];
  $mod->Type = "ul";
  $mod->Build();
  $mod->Save();
}
