// 导航类名
$(".navbar-menu > li").each(function() {
  $(this).addClass("navbar-item");
});
// 手机端导航切换
$(".navbar-burger").click(function() {
  let target = $(this).data("target");
  $(`${target}`).toggleClass("is-active");
  $(this).toggleClass("is-active");
});
// 侧栏跟随
let $sidebar = $("#sidebar.column .mod:last-of-type"),
  offsetTop = 0,
  // copyTop = 0,
  scrollt = 0;
$(window).scroll(function() {
  if ($sidebar.length === 0) return;
  scrollt = $(window).scrollTop();
  if (scrollt >= 137) {
    $("#fixed-box,.backTop").addClass("is-active");
  } else {
    $("#fixed-box,.backTop").removeClass("is-active");
  }
  // console.log(scrollt);
  if ($(window).width() < 768) return false;
  if (offsetTop == 0) {
    offsetTop = $sidebar.offset().top;
  }
  let copyTopN = $(".footer").offset().top;
  if (scrollt + $sidebar.outerHeight() > copyTopN - 137) {
    let marTop = parseInt($sidebar.css("marginTop"));
    let rdyTop = copyTopN - offsetTop - $sidebar.outerHeight() - 137;
    $sidebar.stop().animate({
      marginTop: marTop > rdyTop ? marTop : rdyTop,
    });
    return false;
  }
  if (scrollt > offsetTop) {
    $sidebar.stop().animate({
      marginTop: scrollt - offsetTop + 45,
    });
  } else {
    $sidebar.stop().animate({
      marginTop: 15,
    });
  }
  return false;
});
$(".backTop").click(function() {
  return (
    $("html,body")
      .animate(
        {
          scrollTop: "0px",
        },
        500,
      )
      .fadeIn(),
    !1
  );
});

