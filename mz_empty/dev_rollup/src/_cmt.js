import { fnScrollToEL } from "./__base.js";
// 评论重写
zbp.plugin.unbind("comment.reply.start", "system");
zbp.plugin.on("comment.reply.start", "MizuJS", function(id) {
  const me = this;
  this.$("#inpRevID").val(id);
  this.$("#cancel-reply")
    .show()
    .bind("click", function() {
      me.$("#inpRevID").val(0);
      me.$(this).hide();
      fnScrollToEL("#cmt" + id);
      // window.location.hash = "#cmt" + id;
      return false;
    });
  fnScrollToEL("#comment");
  // window.location.hash = "#comment";
});
zbp.plugin.unbind("comment.post.success", "system");
zbp.plugin.on(
  "comment.post.success",
  "MizuJS",
  function(formData, retString, textStatus, jqXhr) {
    const objSubmit = $("#inpId").parent("form").find(":submit");
    objSubmit
      .removeClass("loading")
      .removeAttr("disabled")
      .val(objSubmit.data("orig"));
    const data = $.parseJSON(retString);
    if (data.err.code !== 0) {
      alert(data.err.msg);
      throw "ERROR - " + data.err.msg;
    }
    if (formData.replyid == "0") {
      this.$(data.data.html).insertAfter("#AjaxCommentBegin");
    } else {
      this.$(data.data.html).insertAfter("#MizuComment" + formData.replyid);
    }
    fnScrollToEL("#cmt" + data.data.ID);
    // location.hash = "#cmt" + data.data.ID;
    this.$("#txaArticle").val("");
    this.userinfo.saveFromHtml();
    this.userinfo.save();
  },
);
if ($(".commentpost").length > 0) {
  let $str = $("#inpName").val().trim();
  if (!$str) {
    zbp.userinfo.output();
  }
}
