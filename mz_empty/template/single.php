{* Template Name: 单页 * Template Type: single*}

<!DOCTYPE html>
<html lang="{$lang['lang_bcp47']}">

<head>
  {template:header}
</head>

<body class="{$type}">
  <!-- include:hero -->
  {template:hero}
  <!-- main -->
  <main class="section">
    <div class="container">
      <div class="columns">
        <div id="main" class="column is-three-quarters">
          {if $article.Type==ZC_POST_TYPE_ARTICLE}
          {template:post-single}
          {else}
          {template:post-page}
          {/if}
        </div>
        <!-- /#main -->
        <div id="sidebar" class="column is-one-quarter">{template:sidebar}</div>
        <!-- /#sidebar -->
      </div>
    </div>
  </main>
  <!-- include:footer -->
  {template:footer}
</body>

</html>
