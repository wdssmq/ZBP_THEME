<section class="mod box" id="{$module.HtmlID}">
  {if (!$module.IsHideTitle)&&($module.Name)}
  <h3 class="mod-hd title">{$module.Name}</h3>
  {else}
  <h3 class="mod-hd title is-sr-only">{$module.Name}</h3>
  {/if}
  {if $module.Type=='div'}
  <div class="mod-bd {$module.FileName}">
    {if $module.FileName=='searchpanel'}
    {template:module-searchpanel}
    {else}
    {$module.Content}
    {/if}
  </div>
  {/if}
  {if $module.Type=='ul'}<ul class="mod-bd {$module.FileName}">{$module.Content}</ul>{/if}
</section>
<!-- {$module.FileName} -->
