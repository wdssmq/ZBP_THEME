{* Template Name: 首页及列表页 * Template Type: index|list|search *}

<!DOCTYPE html>
<html lang="{$lang['lang_bcp47']}">

<head>
  {template:header}
</head>

<body class="{$type}">
  <!-- include:hero -->
  {template:hero}
  <!-- main -->
  <main class="section">
    <div class="container">
      <div class="columns">
        <div id="main" class="column is-three-quarters">
          <!-- 文章为空时 -->
          {if count($articles) == 0}
          <div class="box has-text-centered">
            <h2 class="is-size-4-desktop">
              <span>当前列表为空 - {$name}</span>
            </h2>
            <p class="has-text-centered">
              <img src="{$host}zb_users/theme/{$theme}/var/moe-smile.gif" alt="moe-smile" width="236" height="236" />
            </p>
            <h3 class="is-size-5-desktop">
              <a href="{$host}" title="返回首页">返回首页</a>
              <span>或者</span>
              <a href="javascript:;" onclick="history.go(-1);" title="返回上页">返回上页</a>
            </h3>
          </div>
          {else}
          <!-- 文章列表 -->
          {foreach $articles as $article}
          {if $article.IsTop&&$page=='1'}
          {template:post-multi}
          {else}
          {template:post-multi}
          {/if}
          {/foreach}
          {template:pagebar}
          {/if}
        </div>
        <!-- /#main -->
        <div id="sidebar" class="column is-one-quarter">{template:sidebar}</div>
        <!-- /#sidebar -->
      </div>
    </div>
  </main>
  <!-- include:footer -->
  {template:footer}
</body>

</html>
