<header class="hero is-small">
  <div class="hero-body">
    <div class="container level">
      <div class="level-left">
        <h1 class="title is-marginless noborder">
          <!-- <a href="{$host}" title="{$name}">{$name}</a> -->
          <a href="{$host}" title="{$name}"><span class="is-sr-only">{$name}</span><img src="{$host}zb_users/theme/{$theme}/usr/logo.png" alt="{$name}"></a>
        </h1>
        <p class="subtitle is-sr-only">
          {$subname}
        </p>
      </div>
      <!--/.level-left-->
      <div class="level-right hero-search">
        <form name="search" method="post" action="{$host}zb_system/cmd.php?act=search">
          <div class="field is-grouped">
            <p class="control is-expanded has-icons-left">
              <input class="input is-info" type="text" placeholder="搜索喜欢的内容吧" name="q" />
              <span class="icon is-small is-left"></span>
            </p>
            <p class="control">
              <input type="submit" class="button is-info" value="搜索" />
            </p>
          </div>
        </form>
      </div>
      <!--/.level-right -->
    </div>
  </div>
</header><!-- /header -->

<!-- navbar -->
<nav class="navbar noborder">
  <div class="container">
    <div class="navbar-brand">
      <div class="navbar-burger" data-target=".navbar-menu">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <ul class="navbar-menu">{module:navbar}</ul>
  </div>
</nav><!-- /.navbar-->

<!-- breadcrumb -->
<nav class="breadcrumb">
  <div class="container">
    <ul class="is-px-14">
      {if $type=='article'}
      <li><a href="{$host}" title="首页">首页</a></li>
      <li><a href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a></li>
      <li class="is-active"><a href="{$article.Url}" title="{$article.Title}">{$article.Title}</a></li>
      {else}
      <li><a href="{$host}" title="首页">首页</a></li>
      <li class="is-active"><a href="{$zbp.fullcurrenturl}" title="{$title}">{$title}</a></li>
      {/if}
    </ul>
  </div>
</nav>
