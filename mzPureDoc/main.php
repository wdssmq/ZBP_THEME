<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action = 'root';
if (!$zbp->CheckRights($action)) {
  $zbp->ShowError(6);
  die();
}
if (!$zbp->CheckPlugin('mzPureDoc')) {
  $zbp->ShowError(48);
  die();
}

$act = GetVars("act", "GET");
if ($act === "save") {
  CheckIsRefererValid();
  foreach ($_POST as $key => $value) {
    $zbp->Config("mzPureDoc")->$key = trim($value);
  }

  // 上传相关开始
  $extList = "png|jpg|jpeg|ico"; // 允许上传的文件后缀
  foreach ($_FILES as $key => $value) {
    if ($_FILES[$key]['error'] === 0) {
      $ext = GetFileExt($_FILES[$key]['name']);
      $file = mzPureDoc_Path("u-{$key}");
      // 只允许覆盖已有文件，不允许增加新文件
      if (!HasNameInString($extList, $ext) || !is_file($file)) {
        continue;
      }
      // 移动文件到相应位置
      if (is_uploaded_file($_FILES[$key]['tmp_name'])) {
        move_uploaded_file($_FILES[$key]['tmp_name'], $file);
      }
    }
  }
  // 上传结束

  $zbp->SaveConfig("mzPureDoc");
  $zbp->BuildTemplate();
  $zbp->SetHint('good');
  Redirect('./main.php');
}

InstallPlugin_mzPureDoc();

$logo = mzPureDoc_Path("u-logo", "host") . "?" . time();
$favicon = mzPureDoc_Path("u-fav", "host") . "?" . time();

$blogtitle = '「自用」mzPureDoc - 主题配置';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
?>
<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle; ?></div>
  <div class="SubMenu">
  </div>
  <div id="divMain2" class="clearfix">
    <div class="content-box">
      <ul class="content-box-tabs clearfix">
        <li><a href="#base" class="current">基础设置</a></li>
        <li><a href="#about" class="xnxf-about-btn tab-btn">关于</a></li>
      </ul>
      <div class="content-box-content">
        <div class="tab-content default-tab" id="base">
          <form method="post" action="<?php echo BuildSafeURL("main.php?act=save"); ?>" enctype="multipart/form-data">
            <table class="tableFull tableBorder">
              <tr>
                <th class="td10">
                  <p><b>配置名称</b></p>
                </th>
                <th>配置选项</th>
                <th>备注</th>
              </tr>
              <tr>
                <td>Logo</td>
                <td>
                  <input type="file" name="logo" size="60">&nbsp;&nbsp;<img src="<?php echo $logo; ?>" alt="logo" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
                </td>
                <td>建议高度26px</td>
              </tr>
              <tr>
                <td>favicon.ico</td>
                <td>
                  <input type="file" name="fav" size="60">&nbsp;&nbsp;<img src="<?php echo $favicon; ?>" alt="favicon" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
                </td>
                <td></td>
              </tr>
              <tr>
              <tr>
                <td>关键词</td>
                <td>
                  <?php
                  ZbpForm::text("keywords", $zbp->Config("mzPureDoc")->keywords, "89%");
                  ?>
                </td>
                <td>英文逗号分隔</td>
              </tr>
              <tr>
                <td>站点描述</td>
                <td>
                  <?php
                  ZbpForm::textarea("description", $zbp->config("mzPureDoc")->description, "89%", "5em");
                  ?>
                </td>
                <td></td>
              </tr>
            </table>
            <input type="submit" class="button" value="<?php echo $lang['msg']['submit'] ?>" /><a href="javascript:;" onclick="location.reload();">刷新</a>
          </form>
          <p>提示：更新图片后请在前台按ctrl+f5更新缓存；</p>
        </div>
        <div class="tab-content" id="about">
          <?php require "about.php"; ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
