{* Template Name: 404 * Template Type: 404 *}
{php}
$title = "404";
{/php}
<!DOCTYPE html>
<html lang="{$lang['lang_bcp47']}">

<head>
  {template:header}
</head>

<body class="{$type}">
  <!-- include:hero -->
  {template:hero}
  <!-- main -->
  <main class="section">
    <div class="container">
      <div class="level mz-404 box a-ccc">
        <div class="level-item has-text-centered">
          <div>
            <h3 class="title">404 - {$name}</h3>
            <p>当前请求的页面无法浏览或不存在</p>
            <p><a href="{$host}" title="返回首页">返回首页</a>
              <span>或者</span>
              <a href="javascript:;" onclick="history.go(-1);" title="返回上页">返回上页</a>
            </p>
          </div>
        </div>
        <div class="level-item has-text-centered">
          <img src="{$host}zb_users/theme/{$theme}/var/moe-piu.png" alt="404" />
        </div>
      </div><!-- /.level -->
  </main>
  {template:footer}
</body>

</html>
