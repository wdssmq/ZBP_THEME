<footer class="footer">
  <div class="container is-widescreen">
    <div class="copyright content has-text-centered">
      <p class="a-ccc">{$copyright} | Powered By <a href="http://www.zblogcn.com/" rel="nofollow" title="RainbowSoft Z-BlogPHP" target="_blank">Z-BlogPHP</a> | Theme By <a href="http://www.wdssmq.com" target="_blank" title="置百丈玄冰而崩裂，掷须臾池水而漂摇。" rel="nofollow">沉冰浮水</a></p>
    </div>
  </div>
</footer>
<script src="{$host}zb_system/script/jquery-2.2.4.min.js?v=93131306"></script>
<script src="{$host}zb_system/script/zblogphp.js?v=93131306"></script>
<script src="{$host}zb_system/script/c_html_js_add.php"></script>
<script src="{$host}zb_users/theme/{$theme}/script/script.min.js?v=93131306"></script>
{$footer}
