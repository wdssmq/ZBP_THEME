<?php
#注册插件
RegisterPlugin("mzPureDoc", "ActivePlugin_mzPureDoc");

function ActivePlugin_mzPureDoc()
{
  Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu', 'mzPureDoc_AddMenu');
  Add_Filter_Plugin('Filter_Plugin_ViewList_Core', 'mzPureDoc_canonical');
  Add_Filter_Plugin('Filter_Plugin_ViewComment_Template', 'mzPureDoc_ViewCMT');
}

function mzPureDoc_AddMenu(&$m)
{
  global $zbp;
  $m[] = MakeTopMenu("root", '<b>主题配置</b>', $zbp->host . "zb_users/theme/mzPureDoc/main.php", "", "mzPureDoc");
}

function mzPureDoc_canonical()
{
  global $zbp;
  $fpargs = func_get_args();
  $url = $fpargs[7]->UrlRule;
  $zbp->fullcurrenturl = $url->Make();
}

// 评论并没有用传统的嵌套模式，看不懂的话可以不用
function mzPureDoc_ViewCMT(&$template)
{
  global $zbp;
  // $type = $template->GetTags('type');
  // $comments = $template->GetTags('comments');
  $comment = $template->GetTags('comment');
  if ($comment->ParentID > 0) {
    $MizuPaN = array();
    $commentParent = $zbp->GetCommentByID($comment->ParentID);
    $MizuPaN[$comment->ParentID] = $commentParent->Author->StaticName;
    $template->SetTags('MizuPaN', $MizuPaN);
    $template->SetTemplate("comment-reply");
  }
}

function mzPureDoc_Path($file, $t = 'path')
{
  global $zbp;
  $result = $zbp->$t . 'zb_users/theme/mzPureDoc/';
  switch ($file) {
    case 'u-logo':
      return $result . 'usr/logo.png';
      break;
    case 'u-fav':
      return $result . 'usr/favicon.ico';
      break;
    case 'v-logo':
      return $result . 'var/logo.png';
      break;
    case 'v-fav':
      return $result . 'var/favicon.ico';
      break;
    case 'usr':
      return $result . 'usr/';
      break;
    case 'var':
      return $result . 'var/';
      break;
    case 'main':
      return $result . 'main.php';
      break;
    default:
      return $result . $file;
  }
}

function InstallPlugin_mzPureDoc()
{
  global $zbp;

  $zbp->BuildTemplate();

  $zbp->LoadCategories();
  $zbp->AddBuildModule("catalog");
  // $zbp->AddBuildModule("tags");
  $zbp->BuildModule();

  $mod = $zbp->modulesbyfilename["tags"];
  $mod->Type = "div";
  $mod->Build();
  $mod->Save();

  // 初始化图标、logo
  $filesList = array("logo", "fav");
  foreach ($filesList as $key => $value) {
    $uFile = mzPureDoc_Path("u-{$value}");
    $vFile = mzPureDoc_Path("v-{$value}");
    if (!is_file($uFile)) {
      @mkdir(dirname($uFile));
      copy($vFile, $uFile);
    }
  }
  // 初始化主题配置
  if (!$zbp->HasConfig('mzPureDoc')) {
    $zbp->Config('mzPureDoc')->version = 1;
    $zbp->Config('mzPureDoc')->description = "请在[主题配置]中设置首页描述";
    $zbp->Config('mzPureDoc')->keywords = "";
    $zbp->SaveConfig('mzPureDoc');
  }
}

function UninstallPlugin_mzPureDoc()
{
  global $zbp;
  // 切换其他主题时恢复tag模块类型
  $mod = $zbp->modulesbyfilename["tags"];
  $mod->Type = "ul";
  $mod->Build();
  $mod->Save();
}
