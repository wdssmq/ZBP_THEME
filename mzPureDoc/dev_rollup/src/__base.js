function fnScrollToEL(target) {
  $("body,html").animate(
    {
      scrollTop: $(target).offset().top - 59,
    },
    500,
  );
}

export {
  fnScrollToEL,
};
