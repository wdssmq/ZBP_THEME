$(function() {
  "use strict";
  // 导航菜单
  $(".navbar-menu > li").each(function() {
    $(this).addClass("navbar-item");
  });
  $(".navbar-burger").click(function() {
    let target = $(this).data("target");
    $(`${target}`).toggleClass("is-active");
    $(this).toggleClass("is-active");
  });

  // 外部链接新窗口打开
  $("div.post-body a").click(function() {
    let href = $(this).attr("href");
    if (href.indexOf(location.host) == -1) {
      $(this).attr("target", "_blank");
    }
  });

  // tabs切换
  $(".tabs li").hover(function() {
    const tabID = $(this).data("tab");
    const $tab = $(`#${tabID}`);
    $tab.siblings().removeClass("is-active");
    $tab.addClass("is-active");
    $(this).siblings().removeClass("is-active");
    $(this).addClass("is-active");
  });

  const $sidebar = $("#sidebar"),
    $mod = $("#sidebar .mod:last-of-type"),
    mapCache = {
      min: 0,
      max: 0,
      height: 0,
    };
  let scrollt = 0;
  $(window).scroll(function() {
    if ($mod.length === 0) return;
    scrollt = $(window).scrollTop();
    if ($(window).width() < 768) return false;
    if (mapCache.min === 0 || mapCache.height !== $sidebar.outerHeight()) {
      mapCache.height = $sidebar.outerHeight();
      mapCache.min = $mod.offset().top;
      mapCache.max =
        $sidebar.offset().top + mapCache.height - $mod.outerHeight();
    }
    // console.log(mapCache, scrollt);

    if (scrollt > mapCache.max - 137) {
      return;
    }
    if (scrollt > mapCache.min - 20) {
      $mod.stop().animate({
        marginTop: scrollt - mapCache.min + 40,
      });
      $(".backTop").fadeIn();
    } else {
      $mod.stop().animate({
        marginTop: 0,
      });
      $(".backTop").fadeOut();
    }
    return false;
  });
});
