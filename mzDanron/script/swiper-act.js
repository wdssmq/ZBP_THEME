$(document).ready(function() {
  const t = $(".gallery-left .slide-info"),
    btns = $(".swiper-btns li"),
    e = new Swiper(".gallery-right .swiper-container", {
      autoplay: 5e3,
      mode: "vertical",
      loop: !0,
      onSlideChangeStart: function(e) {
        // console.log(e.activeIndex);
        let a = e.activeIndex - 1;
        (a = 4 == a ? 0 : a),
        t.removeClass("active on"),
        t.eq(a).addClass("active on");
        btns.removeClass("active on"),
        btns.eq(a).addClass("active on");
        // let tx = $(".gallery-right .swiper-slide-active img");
        // if (tx.hasClass("loading")) {
        //   tx.attr("src", tx.data("original")).removeClass("loading");
        //   return;
        // }
      },
    });
  t.hover(
    function() {
      const t = $(this).index();
      e.slideTo(t, 100), e.stopAutoplay();
    },
    function() {
      e.startAutoplay();
    },
  );
  btns.hover(
    function() {
      const t = $(this).index() + 1;
      e.slideTo(t, 100), e.stopAutoplay();
    },
    function() {
      e.startAutoplay();
    },
  );
});
