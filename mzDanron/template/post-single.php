{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
$time = mzDanron_Time($article->PostTime);
{/php}
<section class="post cate{$article.Category.ID} auth{$article.Author.ID} content box is-clearfix">
  <h2 class="post-title is-size-3">{$article.Title}</h2>
  <p class="post-meta meta-style is-px-14 is-clearfix">
    <a class="meta-item has-text-grey has-text-dark" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a>
    <a class="meta-item has-text-grey has-text-dark" href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a>
    <a class="meta-item has-text-grey has-text-dark" href="{$article.Url}#comment" title="留言">{$article.CommNums}条留言</a>
    <span class="meta-item has-text-grey">{$article.ViewNums} 次浏览</span>
    <a class="meta-item post-time has-text-grey has-text-dark" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$time}</a>
    <!-- 编辑入口 -->
    <span data-id="{$article.ID}" data-type="{$article.Type}" class="js-edt is-pulled-right is-hidden">[编辑]</span>
  </p>
  <div class="post-body">{$article.Content}</div>
  <!-- <p class="post-copyright">本文标题：《<a title="{$article.Title}" href="{$article.Url}">{$article.Title}</a>》作者：<a title="{$article.Author.StaticName}" href="javascript:;">{$article.Author.StaticName}</a><br>原文链接：<a title="{$article.Title}" href="{$article.Url}">{$article.Url}</a><br><span class="has-text-weight-bold">特别注明外均为原创，转载请注明。</span></p> -->
  <p class="post-meta tags ">{foreach $article.Tags as $tag}<a class="post-tag tag" href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>{/foreach}<span title="请为本文设置合适的标签" class="post-tag tag c-help">设置Tag是个好习惯</span></p>
</section>
<div class="box post-nav is-clearfix">
  {if $article.Prev}
  <div class="is-pulled-left">
    <span class="has-text-grey">上一篇：</span>
    <a class="a-color a-shadow" href="{$article.Prev.Url}" title="上一篇 {$article.Prev.Title}">{$article.Prev.Title}</a>
  </div>
  {/if}
  {if $article.Next}
  <div class="is-pulled-right">
    <span class="has-text-grey">下一篇：</span>
    <a class="a-color a-shadow" href="{$article.Next.Url}" title="下一篇 {$article.Next.Title}">{$article.Next.Title}</a>
  </div>
  {/if}
</div>

<section class="box post-related is-clearfix">
  <div class="title-wrapper">
    <h4 class="title title-style">相关文章</h4>
  </div>
  {if count($article.RelatedList)}
  <ul class="list-unstyled">
    {foreach $article.RelatedList as $related}
    <li><a href="{$related.Url}" title="{$related.Title}">{$related.Title}</a></li>
    <!-- (<span>{$related.Time('Y-m-d')}</span>) -->
    {/foreach}
  </ul>
  {else}
  {template:n-rand}
  {/if}
</section>

{if !$article.IsLock}
{template:comments}
{/if}
