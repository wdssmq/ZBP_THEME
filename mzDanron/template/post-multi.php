{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
$description = preg_replace('/[\r\n\s]+/', ' ', trim(SubStrUTF8(FormatString($article->Intro,'[nohtml]'),149)).'...');
$description = htmlspecialchars($description);
$time = mzDanron_Time($article->PostTime);
// var_dump($article->GetData());
{/php}
<div class="media post-info box-style">
  <figure class="media-left is-hidden-touch">
    <p class="image is-3by2">
      <a class="has-text-centered" href="{$article->Url}" title="{$article->Title}"><img class="lz-loading" src="{$article.FistImg}" alt="{$article.Title}"></a>
    </p>
  </figure>
  <div class="media-content is-flex is-flex-direction-column is-justify-content-space-between">
    <h2 class="post-title is-px-18 a-color a-shadow"><a href="{$article.Url}" rel="bookmark" title="{$article.Title}">{$article.Title}</a></h2>
    <p class="is-px-14 intro-content">{$description}</p>
    <p class="post-meta is-size-7 has-text-right tags">

      <a class="a-ccc tag" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a>

      <a class="post-time a-ccc tag" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$time}</a>

    </p>
  </div>
</div>
