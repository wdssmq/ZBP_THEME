<div id="focus" class="columns">
  <div class="slide-wraper column is-three-quarters">
    {template:n-slide}
  </div>
  <div class="column is-one-quarter">
    {template:focus-tabs}
  </div>
</div>
