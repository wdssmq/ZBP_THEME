<div class="slides columns">
  <div class="gallery-left column is-hidden-touch is-one-third">
    <div id="slide-info-boxs" class="box a-color is-flex is-flex-direction-column is-justify-content-space-between">
      <h3 class="title is-px-14">推荐</h3>
      {foreach $slides as $slide}
      <div class="media slide-info">
        <figure class="media-left">
          <p class="image is-3by2">
            <a class="has-text-centered" href="{$slide->Url}" title="{$slide->Title}" target="_blank"><img class="slideimg" src="{$slide.Img}" alt="{$slide.Title}"></a>
          </p>
        </figure>
        <div class="media-content">
          <p><a class="has-text-centered is-px-12" href="{$slide->Url}" title="{$slide->Title}" target="_blank">{$slide.Title}</a></p>
        </div>
      </div>
      {/foreach}
    </div>
  </div>
  <!-- Swiper -->
  <div class="gallery-right column is-two-thirds">
    <div class="swiper-container">
      <div class="swiper-wrapper">
        {foreach $slides as $slide}
        <div class="swiper-slide">
          <a class="has-text-centered" href="{$slide->Url}" title="{$slide->Title}" target="_blank">
            <img src="{$slide.Img}" alt="{$slide.Title}">
            <span>{$slide.Title}</span>
          </a>
        </div>
        {/foreach}
      </div><!-- /.swiper-container -->
      <div class="swiper-btns">
        <ul>
          <li class="active"></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="label-box columns noborder is-hidden-touch">
  {foreach $labels as $label}
  <div class="label-item column has-text-centered">
    <a href="{$label->Url}" title="{$label->Title}" target="_blank">
      <img src="{$label.Img}" alt="{$label.Title}">
    </a>
  </div>
  {/foreach}
</div>
