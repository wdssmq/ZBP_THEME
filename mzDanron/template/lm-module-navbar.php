{if count($item.subs)}
<li class="{$id}-item has-dropdown is-hoverable">
  <a href="{$item.href}" target="{$item.target}" title="{$item.title}">{$item.ico}{$item.text}</a>
  <ul class="{$id}-dropdown">{foreach $item.subs as $item}{template:lm-module-defend}{/foreach}</ul>
</li>
{else}
<li class="{$id}-item">
  <a href="{$item.href}" target="{$item.target}" title="{$item.title}">{$item.ico}{$item.text}</a>
</li>
{/if}
