{foreach $tags as $tag}
<a class="tag" title="{$tag.Name}" href="{$tag.Url}">{$tag.Name}<span class="tag-count"> ({$tag.Count})</span></a>
{/foreach}
