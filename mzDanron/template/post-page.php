﻿{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
$time = mzDanron_Time($article->PostTime);
{/php}
<section class="post cate{$article.Category.ID} auth{$article.Author.ID} content box is-clearfix">
  <h2 class="post-title is-size-4">{$article.Title}</h2>
  <p class="post-meta is-size-7 meta-1 is-clearfix">
    <a class="meta-item has-text-grey has-text-dark" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a>
    <a class="meta-item has-text-grey has-text-dark" href="{$article.Url}#comment" title="留言">{$article.CommNums}条留言</a>
    <span class="meta-item has-text-grey">{$article.ViewNums} 次浏览</span>
    <a class="meta-item post-time has-text-grey has-text-dark" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$time}</a>
    <!-- 编辑入口 -->
    <span data-id="{$article.ID}" data-type="{$article.Type}" class="js-edt is-pulled-right is-hidden">[编辑]</span>
  </p>
  <div class="post-body">{$article.Content}</div>
</section>

{if !$article.IsLock}
{template:comments}
{/if}
