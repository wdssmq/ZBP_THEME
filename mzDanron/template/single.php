<!DOCTYPE html>
<html lang="{$language}">

<head>
  {template:header}
</head>

<body class="{$type}">
  {template:hero}
  <div class="bread-wrapper">
    <div class="container is-flex is-align-items-center">
      {template:p-breadcrumb}
    </div>
  </div>
  <main class="section">
    <div class="container">
      <div class="columns main">
        <div class="column is-three-quarters post-wrapper">
          {if $article.Type==ZC_POST_TYPE_ARTICLE}
          {template:post-single}
          {else}
          {template:post-page}
          {/if}
        </div>
        <div class="column is-one-quarter">{template:sidebar}</div>
      </div>
    </div>
  </main>
  {template:footer}
</body>

</html>
