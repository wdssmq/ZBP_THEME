<section class="mod box" id="{$module.HtmlID}">
{if (!$module.IsHideTitle)&&($module.Name)}
<header class="title-wrapper">
  <h3 class="mod-hd title title-style">{$module.Name}</h3>
</header>
{else}
<header class="title-wrapper is-hidden">
  <h3 class="mod-hd title title-style">{$module.Name}</h3>
</header>
{/if}
{if $module.Type=='div'}<div class="mod-bd {$module.FileName}">
{if $module.FileName=='searchpanel'}
{template:module-searchpanel}
{else}
{$module.Content}
{/if}
</div>{/if}
{if $module.Type=='ul'}<ul class="mod-bd {$module.FileName}">{$module.Content}</ul>{/if}
</section>
<!-- {$module.FileName} -->
