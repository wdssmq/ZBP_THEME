  <meta charset="utf-8">
  <meta name="generator" content="{$zblogphp}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
{if $type=='article'}
  <title>{$title}_{$article.Category.Name}_{$name}</title>
  {php}
  $keywords = $article->TagsToNameString();
  $description = preg_replace('/[\r\n\s]+/', ' ', trim(SubStrUTF8(FormatString($article->Content,'[nohtml]'),135)).'...');
  {/php}
  <meta name="keywords" content="{$keywords}">
  <meta name="description" content="{$description}">
  <meta name="author" content="{$article.Author.StaticName}">
  <link rel="canonical" href="{$article.Url}">
{elseif $type=='page'}
  <title>{$title}_{$name}_{$subname}</title>
  <meta name="keywords" content="{$title},{$name}">
  {php}
  $description = preg_replace('/[\r\n\s]+/', ' ', trim(SubStrUTF8(FormatString($article->Content,'[nohtml]'),135)).'...');
  {/php}
  <meta name="description" content="{$description}">
  <meta name="author" content="{$article.Author.StaticName}">
  <link rel="canonical" href="{$article.Url}">
{elseif $type=='index'}
  <title>{$name}{if $page>'1'}_第{$pagebar.PageNow}页{/if}_{$subname}</title>
  <meta name="keywords" content="{$zbp->Config('mzDanron')->keywords},{$name}">
  <meta name="description" content="{$zbp->Config('mzDanron')->description}_{$name}_{$title}">
  <meta name="author" content="{$zbp.members[1].Name}">
  <link rel="canonical" href="{$zbp.fullcurrenturl}">
{else}
  {php}
  $title = preg_replace('/\s.+$/','',$title);
  $fixTitle = "";
  $fixDesc = "";
  if (isset($pagebar)){
  $fixTitle = "_第{$pagebar->PageNow}页";
  $fixDesc = "_当前是第{$pagebar->PageNow}页";
  }
  {/php}
  <title>{$title}_{$name}{$fixTitle}</title>
  <meta name="keywords" content="{$title},{$name}">
  <meta name="description" content="{$title}_{$name}{$fixDesc}">
  <meta name="author" content="{$zbp.members[1].Name}">
  <link rel="canonical" href="{$zbp.fullcurrenturl}">
{/if}
  <link rel="shortcut icon" href="{$host}zb_users/theme/mzDanron/usr/favicon.ico">
  <!-- <link rel="stylesheet" href="{$host}zb_users/theme/mzDanron/css/animate.min.css?v=1899094932">
  <link rel="stylesheet" href="{$host}zb_users/theme/mzDanron/css/iconfont.css?v=1899094932"> -->
  <link rel="stylesheet" href="{$host}zb_users/theme/mzDanron/style/{$style}.css?v=1899094932">
{if $type=='index' && $page=='1'}
  <link href="{$host}zb_users/theme/mzDanron/css/swiper.min.css?v=1899094932" rel="stylesheet" type="text/css">
  <link rel="alternate" type="application/rss+xml" href="{$feedurl}" title="{$name}">
  <link rel="EditURI" type="application/rsd+xml" title="RSD" href="{$host}zb_system/xml-rpc/?rsd">
  <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="{$host}zb_system/xml-rpc/wlwmanifest.xml">
{/if}
  {$header}
