<div id="focus-tabs" class="box is-height-100">
  <span class="icon"></span>
  <div class="tabs is-px-14">
    <ul>
      <li data-tab="ft-51" class="is-active"><a href="javascript:;">热门文章</a></li>
      <li data-tab="ft-52"><a href="javascript:;">随机文章</a></li>
    </ul>
  </div>
  <div class="tabs-content">
    <div id="ft-51" class="is-active">{template:n-hot}</div>
    <div id="ft-52">{template:n-rand}</div>
  </div>
</div>
