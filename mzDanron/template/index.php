<!DOCTYPE html>
<html lang="{$language}">

<head>
  {template:header}
</head>

<body class="{$type}">
  {template:hero}
  {if $type=='index'&&$page=='1'}
  <div class="focus-wrapper">
    <div class="container">{template:focus}</div>
  </div>
  {/if}
  <main class="section">
    <div class="container">
      <div class="columns main">
        <div class="column is-three-quarters">
          {if count($articles) == 0}
          <div class="box has-text-centered">
            <h2 class="is-size-4-desktop">
              <span>当前列表为空 - {$name}</span>
            </h2>
            <p class="has-text-centered">
              <img src="{$host}zb_users/theme/{$theme}/var/moe-smile.gif" alt="moe-smile" width="236" height="236" />
            </p>
            <h3 class="is-size-5-desktop">
              <a href="{$host}" title="返回首页">返回首页</a>
              <span>或者</span>
              <a href="javascript:;" onclick="history.go(-1);" title="返回上页">返回上页</a>
            </h3>
          </div>
          {else}

          {template:n-cms-cate-focus}

          <div class="title-wrapper">
            <h4 class="title title-style">最新文章</h4>
          </div>
          {foreach $articles as $article}
          {if $article.IsTop&&$page=='1'}
          {template:post-multi}
          {else}
          {template:post-multi}
          {/if}
          {/foreach}
          {/if}
          {template:pagebar}
        </div>
        <div id="sidebar" class="column is-one-quarter">{template:sidebar}</div>
      </div>
    </div>
  </main>
  {template:footer}
</body>

</html>
