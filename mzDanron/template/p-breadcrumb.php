<nav class="breadcrumb is-px-14">
  <ul>
    {if $type=='article'}
    <li><a href="{$host}" title="首页">首页</a></li>
    <li><a href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a></li>
    <li class="is-active is-hidden-mobile"><a href="{$article.Url}" title="{$article.Title}">{$article.Title}</a></li>
    {else}
    <li><a href="{$host}" title="首页">首页</a></li>
    <li class="is-active"><a href="{$zbp.fullcurrenturl}" title="{$title}">{$title}</a></li>
    {/if}
  </ul>
</nav>
