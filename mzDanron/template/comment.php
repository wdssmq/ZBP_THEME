{$MizuPaN[$comment.ID] = $comment.Author.StaticName}
{$MizuPaID[$comment.ID] = $comment.ID}
{if !$comment.Author.HomePage}
{$comment.HomePage = "#cmt" . $comment.ID}
{/if}
<div class="box cmt media" id="cmt{$comment.ID}">
  <figure class="cmt-head media-left">
    <p class="image is-48x48">
      <img class="avatar is-rounded" src="{$comment.Author.Avatar}" alt="{$comment.Author.StaticName}" width="38" height="38" />
    </p>
  </figure>
  <div class="cmt-body media-content">
    <p class="cmt-user">
      <b class="cmt-name"><a href="{$comment.Author.HomePage}" rel="external nofollow" target="_blank" title="{$comment.Author.StaticName}">{$comment.Author.StaticName}</a></b><small>&nbsp;发布于&nbsp;{$comment.Time()}&nbsp;&nbsp;</small>
      {if isset($comment.c2rTips)}{$comment.c2rTips}{/if}
    </p>
    <div class="cmt-content content is-clearfix">
      <!-- Content -->
      {$comment.Content}
      <!-- /Content -->
      <span class="cmt-reply is-pulled-right"><a href="javascipt:;" onclick="zbp.comment.reply('{$comment.ID}')" title="回复">回复</a></span>
    </div>
    <div class="cmt-replys">
      <label id="MizuComment{$comment.ID}"></label>
      {foreach $comment.Comments as $comment}{template:comment-reply}{/foreach}
    </div>
  </div>
</div>
