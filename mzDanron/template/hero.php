<header class="hero is-small">
  <div class="hero-body">
    <div class="container level">
      <div class="level-left">
        <h1 class="title noborder">
          <!-- <a href="{$host}" title="{$name}">{$name}</a> -->
          <a href="{$host}" title="{$name}"><span class="is-sr-only">{$name}</span><img src="{$host}zb_users/theme/mzDanron/usr/logo.png" alt="{$name}"></a>
        </h1>
        <p class="subtitle is-sr-only">
          {$subname}
        </p>
      </div>
      <div class="level-right hero-search">
        <form name="search" method="post" action="{$host}zb_system/cmd.php?act=search">
          <div class="field is-grouped">
            <p class="control is-expanded has-icons-left">
              <input class="input is-info" type="text" placeholder="搜索喜欢的内容吧" name="q" />
              <span class="icon is-small is-left"></span>
            </p>
            <p class="control">
              <input type="submit" class="button is-info" value="搜索" />
            </p>
          </div>
        </form>
      </div>
    </div>
  </div><!-- /.hero-body -->
  <nav class="navbar noborder">
    <div class="container">
      <div class="navbar-brand">
        <!-- <h1 class="title none-border navbar-item"><a href="{$host}" title="{$name}"><span class="is-sr-only">{$name}</span><img src="{$host}zb_users/theme/acgMora/usr/logo.png" alt="{$name}"></a></h1> -->
        <div class="navbar-burger" data-target=".navbar-menu">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      <ul class="navbar-menu">{module:navbar}</ul>
    </div>
  </nav><!-- /.navbar-->
  <div class="is-relative">
    <div class="navbar-fade"></div>
  </div>
</header>
