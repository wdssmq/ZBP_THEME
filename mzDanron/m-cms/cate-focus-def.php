<div class="title-wrapper">
  <h4 class="title title-style">{$cate->Name}</h4>
</div>
<ul class="columns is-multiline">
  {foreach $articles as $article}
  <li class="column is-half cate-focus-item is-px-14">
    <a href="{$article.Url}" title="{$article.Title}">{$article.Title}</a>
  </li>
  {/foreach}
</ul>
