## 投喂支持

爱发电：[https://afdian.com/a/wdssmq](https://afdian.com/a/wdssmq "沉冰浮水正在创作和 z-blog 相关或无关的各种有用或没用的代码 | 爱发电")

哔哩哔哩：[https://space.bilibili.com/44744006](https://space.bilibili.com/44744006 "沉冰浮水的个人空间\_哔哩哔哩\_bilibili")「投币或充电」「[大会员卡券领取 - bilibili](https://account.bilibili.com/account/big/myPackage "大会员卡券领取 - bilibili")」

RSS 订阅：[https://feed.wdssmq.com](https://feed.wdssmq.com "沉冰浮水博客的 RSS 订阅地址") 「[「言说」RSS 是一种态度！！](https://www.wdssmq.com/post/20201231613.html "「言说」RSS 是一种态度！！")」

在更多平台关注我：[https://www.wdssmq.com/guestbook.html#其他出没站点和信息](https://www.wdssmq.com/guestbook.html#%E5%85%B6%E4%BB%96%E5%87%BA%E6%B2%A1%E5%9C%B0%E7%82%B9%E5%92%8C%E4%BF%A1%E6%81%AF "在更多平台关注我")

更多「小代码」：[https://cn.bing.com/search?q=小代码+沉冰浮水](https://cn.bing.com/search?q=%E5%B0%8F%E4%BB%A3%E7%A0%81+%E6%B2%89%E5%86%B0%E6%B5%AE%E6%B0%B4 "小代码 沉冰浮水 - 必应搜索")

<!-- ##################################### -->

## 主题说明

Grunt + SASS + Bulma，免费。。

**一个代码注释超级多和你唯一应该抄的主题！**

初学者可以通过本主题来学习主题开发。

mzERK · 沉冰浮水/ZBP\_THEME - 码云 - 开源中国

[https://gitee.com/wdssmq/ZBP\_THEME/tree/master/mzERK](<https://gitee.com/wdssmq/ZBP_THEME/tree/master/mzERK> "mzERK · 沉冰浮水/ZBP_THEME - 码云 - 开源中国")

## 相关推荐

zblog 插件开发演示 - Z-Blog 应用中心

[https://app.zblogcn.com/?id=18072](<https://app.zblogcn.com/?id=18072> "zblog 插件开发演示 - Z-Blog 应用中心")


 
【开发者】大概算是进阶建议贴-开发者中心-ZBlogger 技术交流中心

[https://bbs.zblogcn.com/thread-101310.html](<https://bbs.zblogcn.com/thread-101310.html>)


 
初学者 Teaching 计划-开发者中心-ZBlogger 技术交流中心

[https://bbs.zblogcn.com/thread-102975.html](<https://bbs.zblogcn.com/thread-102975.html>)

## 更新日志

2024-08-16：极验插件适配；评论分页条调整；清理未使用的 CSS 样式；

2023-02-13：w3c 规范；

2021-09-04：注释更新；

2021-03-14：JS 接口；

2021-03-08：完善注释；

2021-01-31：导航样式；

2021-01-28：评论接口名更新；

2020-12-07：教学注释更新；

2020-11-24：代码内增加注释说明；

2020-09-20：[fix.]再一次被零宽空白坑了；以及感谢天兴帮找到的 Bug。

2020-09-19：[alt.]细微调整文件上传代码；

2020-05-20：附加导航增加主题ID用于标识，仅对新创建有效；

2020-04-25：canonical 规范；w3c 规范

2019-11-27：404 模板调整；

2019-07-06：导航、Logo 细节调整；添加 RSS 图标链接；

2019-07-04：开发模式常开反而发现不了某些 Bug；

\------

![001](<https://app.zblogcn.com/zb_users/upload/2019/07/201907041562216238717223.png> "001")

