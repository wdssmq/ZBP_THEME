<?php
#注册插件
RegisterPlugin("mzERK", "ActivePlugin_mzERK");

/**
 *  这是个正式的可以拿来用的主题，请在理解代码意图的基础上使用。
 *  星星数量为建议掌握的顺序，超少优先级越高
 */

// zblog插件开发演示（HelloZBlog） - Z-Blog 应用中心
// https://app.zblogcn.com/?id=18072
// 一个试图教你写插件的插件

// 【开发者】大概算是进阶建议贴-开发者中心-ZBlogger技术交流中心
// https://bbs.zblogcn.com/thread-101310.html

// 初学者Teaching计划-开发者中心-ZBlogger技术交流中心
// https://bbs.zblogcn.com/thread-102975.html
function ActivePlugin_mzERK()
{
  // 接口挂载 - 侧栏模块保存时防止 FileName 大小写变化
  Add_Filter_Plugin('Filter_Plugin_Module_Save', 'mzERK_KeepModFileName');
  // ★
  // 接口挂载相关说明请见HelloZBlog插件
  Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu', 'mzERK_AddMenu');
  Add_Filter_Plugin('Filter_Plugin_ViewList_Core', 'mzERK_canonical');
  Add_Filter_Plugin('Filter_Plugin_ViewComment_Template', 'mzERK_ViewCMT');
}

// 接口函数 - 侧栏模块保存时防止 FileName 大小写变化
function mzERK_KeepModFileName(&$mod)
{
  // 如果 $mod->FileName 为全小写
  if ("mzerk-nav" === $mod->FileName) {
    // 将 $mod->FileName 恢复为原始值
    $mod->FileName = "mzERK-Nav";
  }
}

// ★★
// 针对列表页的canonical标记链接，对所谓的SEO很重要
// 虽然发现过并不生效的时候
function mzERK_canonical()
{
  global $zbp;
  $fpargs = func_get_args();
  $url = $fpargs[7]->UrlRule;
  $zbp->fullcurrenturl = $url->Make();
}
// ★
// 后台顶部导航增加主题配置入口
function mzERK_AddMenu(&$m)
{
  global $zbp;
  $m[] = MakeTopMenu("root", '<b>主题配置</b>', $zbp->host . "zb_users/theme/mzERK/main.php", "", "mzERK", "icon-grid-1x2-fill");
  // ★★
  // 字体图标一览
  // https://static.zblogcn.com/image/icon/demo.html
}
// ★★★★
// 评论并没有用传统的嵌套模式，看不懂的话可以不用
function mzERK_ViewCMT(&$template)
{
  global $zbp;
  $type = $template->GetTags('type');
  // $comments = $template->GetTags('comments');
  $comment = $template->GetTags('comment');
  if ($comment->ParentID > 0) {
    $commentParent = $zbp->GetCommentByID($comment->ParentID);
    $template->SetTags('MizuPaN', $commentParent->Author->StaticName);
    $template->SetTemplate("comment-reply");
  }
}
function InstallPlugin_mzERK()
{
  global $zbp;
  // ★
  // 在适当的时候可以主动调用模板编译
  $zbp->BuildTemplate();

  // ★★★
  // 重建侧栏分类模块
  // [5楼] https://bbs.zblogcn.com/thread-101310.html
  $zbp->LoadCategories();
  $zbp->AddBuildModule("catalog");
  // $zbp->AddBuildModule("tags");
  $zbp->BuildModule();

  // ★★★
  // 重建tag模块，因为本主题没有用ul>li列表
  // [5楼] https://bbs.zblogcn.com/thread-101310.html
  $mod = $zbp->modulesbyfilename["tags"];
  $mod->Type = "div";
  $mod->Build();
  $mod->Save();

  // ★★★★
  // 复制分类模块作为导航，会自动判断是否存在，存在则不会创建
  $mod = $zbp->modulesbyfilename["catalog"];
  $mod->Name = "「mzERK」分类导航";
  $mod->FileName = "mzERK-Nav";
  $mod->HtmlID = $mod->FileName;
  // ★★★★
  // 使用链接管理插件接管编辑
  $mod->Source = "plugin_LinksManage";
  $mod->ID = 0;
  $mod->Save();

  // ★★
  // 用户上传的图片存放在 usr 下，升级不会覆盖，首次安装的用户需要先创建文件夹并放入预置文件
  // 预置文件对应在 var 下
  // 【开发者】1.6新增应用打包时排除文件（夹）功能专贴-开发者中心-ZBlogger技术交流中心
  // https://bbs.zblogcn.com/thread-102780.html
  // https://gitee.com/wdssmq/ZBP_THEME/blob/master/mzERK/zbignore.txt
  $filesList = array("logo", "fav");
  foreach ($filesList as $key => $value) {
    $uFile = mzERK_Path("u-{$value}");
    $vFile = mzERK_Path("v-{$value}");
    if (!is_file($uFile)) {
      @mkdir(dirname($uFile));
      copy($vFile, $uFile);
    }
  }

  // ★
  // 配置项已经成为标配了，，然后初始化下数据会比较好；
  if (!$zbp->HasConfig('mzERK')) {
    $zbp->Config('mzERK')->version = 1;
    $zbp->Config('mzERK')->description = "请在[主题配置]中设置首页描述";
    $zbp->Config('mzERK')->keywords = "";
    $zbp->SaveConfig('mzERK');
  }
}
// ★★★★
// 插件升级时执行，要同时考虑新装用户
function UpdatePlugin_mzERK()
{
  global $zbp;

  // // 判断版本号↓

  //$version = $zbp->Config('mzERK')->version;
  //if ($version !== 1.1) {
  //  升级操作
  //  $zbp->Config('mzERK')->version = 1.1;
  //  $zbp->SaveConfig('mzERK');
  //}

  // // 或者单独判断某个字段是否存在 ↓
  // // cfg_haskey ← 单独使用

  // if (!$zbp->Config('mzERK')->HasKey("newKey")) {
  //   $zbp->Config('mzERK')->newKey = 'value';
  //   $zbp->SaveConfig('mzERK');
  // }
}
// 旧版兼容
function mzERK_Updated()
{
  UpdatePlugin_mzERK();
}
// 这个函数可以用代码片段快速生成↓↓
// https://github.com/wdssmq/HelloZBlog/tree/master/docs#代码片段
// https://bbs.zblogcn.com/thread-101310.html#484331
function mzERK_Path($file, $t = 'path')
{
  global $zbp;
  $result = $zbp->$t . 'zb_users/theme/mzERK/';
  // 重要：
  // 请好奇下为什么会有「v-」「u-」对应的两项
  // usr 下文件在打包成 zba 时会被排除，防止更新主题时被覆盖掉；
  // 【开发者】1.6新增应用打包时排除文件（夹）功能专贴-开发者中心-ZBlogger技术交流中心
  // https://bbs.zblogcn.com/thread-102780.html
  // https://gitee.com/wdssmq/ZBP_THEME/blob/master/mzERK/zbignore.txt
  switch ($file) {
    case 'u-logo':
      return $result . 'usr/logo.png';
      break;
    case 'v-logo':
      return $result . 'var/logo.png';
      break;
      // logo.png ↑↑
    case 'u-fav':
      return $result . 'usr/favicon.ico';
      break;
    case 'v-fav':
      return $result . 'var/favicon.ico';
      break;
      // favicon.ico ↑↑
    case 'usr':
      return $result . 'usr/';
      break;
    case 'var':
      return $result . 'var/';
      break;
      // 目录 ↑↑
    case 'main':
      return $result . 'main.php';
      break;
      // main.php ↑↑
    default:
      return $result . $file;
  }
}
function UninstallPlugin_mzERK()
{
  global $zbp;
  // ★★★
  // 切换其他主题时恢复tag模块类型
  $mod = $zbp->modulesbyfilename["tags"];
  $mod->Type = "ul";
  $mod->Build();
  $mod->Save();
}
