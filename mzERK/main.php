<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action = 'root';
if (!$zbp->CheckRights($action)) {
  $zbp->ShowError(6);
  die();
}
if (!$zbp->CheckPlugin('mzERK')) {
  $zbp->ShowError(48);
  die();
}

$blogtitle = 'mzERK';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';

// zblog插件开发演示 - Z-Blog 应用中心
// https://app.zblogcn.com/?id=18072
// 一个试图教你写插件的插件

// 【开发者】大概算是进阶建议贴-开发者中心-ZBlogger技术交流中心
// https://bbs.zblogcn.com/thread-101310.html

// 初学者Teaching计划-开发者中心-ZBlogger技术交流中心
// https://bbs.zblogcn.com/thread-102975.html

$act = GetVars("act", "GET");
if ($act === "save") {
  // ↓↓安全验证，需要配合表单中的csrfToken
  CheckIsRefererValid();
  foreach ($_POST as $key => $value) {
    // 跳过不需要保存的表单字段
    if ("csrfToken" === $key) {
      continue;
    }
    $zbp->Config("mzERK")->$key = trim($value);
  }
  // 保存配置项
  $zbp->SaveConfig("mzERK");

  // 上传相关开始
  $extList = "png|jpg|jpeg|ico"; // 允许上传的文件后缀
  foreach ($_FILES as $key => $value) {
    if ($_FILES[$key]['error'] === 0) {
      $ext = GetFileExt($_FILES[$key]['name']);
      $file = mzERK_Path("u-{$key}"); // 根据字段返回实际文件路径，参考mzERK主题
      // 只允许覆盖已有文件，不允许增加新文件
      if (!HasNameInString($extList, $ext) || !is_file($file)) {
        continue;
      }
      // 接受上传文件
      if (is_uploaded_file($_FILES[$key]['tmp_name'])) {
        move_uploaded_file($_FILES[$key]['tmp_name'], $file);
      }
    }
  }
  // 上传结束

  // 重建模板，即使功能上不需要习惯性写上也不会损失什么
  $zbp->BuildTemplate();
  // 保存后给出操作成功的提示
  $zbp->SetHint('good');
  // 重定向一次页面，不然地址栏会是main.php?act=save&csrfToken=cbb7ce8bd23
  // 一是视觉上乱，二是刷新页面会有重复提交的提示
  Redirect('./main.php');
}
// 个人习惯在这里调用一次
InstallPlugin_mzERK();
// 拼接logo图片的url，加上时间参数保证上传后配置页能直接看到新图，前台可能要强制刷新
$logo = mzERK_Path("u-logo", "host") . "?" . time();
$favicon = mzERK_Path("u-fav", "host") . "?" . time();
?>
<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle; ?></div>
  <div class="SubMenu clearfix">
  </div>
  <div id="divMain2" class="clearfix">
    <div class="content-box">
      <ul class="content-box-tabs clearfix">
        <li><a href="#base" class="current">基础设置</a></li>
        <li><a href="#about" class="xnxf-about-btn tab-btn">关于</a></li>
      </ul>
      <div class="content-box-content">
        <div class="tab-content default-tab" id="base">
          <!-- 对于需要上传图片的表单，要加上：enctype="multipart/form-data" -->
          <!-- csrfToken 字段放在了下边的input里，也可以在action里用相应函数 -->
          <!-- action="<?php echo BuildSafeURL("main.php?act=save"); ?>" -->
          <form method="post" action="main.php?act=save" enctype="multipart/form-data">
            <input type="hidden" name="csrfToken" value="<?php echo $zbp->GetCSRFToken() ?>">
            <table class="tableFull tableBorder">
              <tr>
                <th class="td10">
                  <p><b>配置名称</b></p>
                </th>
                <th>配置选项</th>
                <th>备注</th>
              </tr>
              <tr>
                <td>Logo</td>
                <td>
                  <input type="file" name="logo" size="60">&nbsp;&nbsp;<img src="<?php echo $logo; ?>" alt="logo" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
                </td>
                <td>建议高度26px</td>
              </tr>
              <tr>
                <td>favicon.ico</td>
                <td>
                  <input type="file" name="fav" size="60">&nbsp;&nbsp;<img src="<?php echo $favicon; ?>" alt="favicon" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
                </td>
                <td><b>注意：如果博客根目录下存favicon.ico则此处上传无效</b></td>
              </tr>
              <tr>
              <tr>
                <td>关键词</td>
                <td>
                  <?php
                  ZbpForm::text("keywords", $zbp->Config("mzERK")->keywords, "89%");
                  ?>
                </td>
                <td>英文逗号分隔</td>
              </tr>
              <tr>
                <td>站点描述</td>
                <td>
                  <?php
                  ZbpForm::textarea("description", $zbp->config("mzERK")->description, "89%", "5em");
                  ?>
                </td>
                <td></td>
              </tr>
            </table>
            <input type="submit" class="button" value="<?php echo $lang['msg']['submit'] ?>" /><a href="javascript:;" onclick="location.reload();">刷新</a>
          </form>
          <p>提示：更新图片后请在前台按ctrl+f5更新缓存；</p>
        </div>
        <div class="tab-content" id="about">
          <?php require "about.php"; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
