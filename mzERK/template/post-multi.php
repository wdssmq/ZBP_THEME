{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
{/php}
<section class="post cate{$article.Category.ID} auth{$article.Author.ID} off-is-clearfix box content">
  <h2 class="post-title title is-5 noborder"><a class="a-color a-shadow" href="{$article.Url}" title="{$article.Title}">{$article.Title}</a><span data-id="{$article.ID}" class="is-size-7 js-edt is-pulled-right"></span></h2>
  <p class="post-meta has-text-grey-light">
    <span class="info_author info_ico"><a class="has-text-grey-light" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a></span> |
    <span class="info_date info_ico"><a class="post-time   has-text-grey-light" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$article.Time('Y-m-d')}</a></span> |
    <span class="info_category info_ico"><a class="has-text-grey-light" href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a></span> |
    <span class="info_comment info_ico"><a class="has-text-grey-light" href="{$article.Url}#comment" title="{$article.Title}">{$article.CommNums}条留言</a></span> |
    <span class="info_views info_ico">{$article.ViewNums}</span>
  </p>
  <div class="post-body">{$article.Intro}</div>
  <p class="post-meta tags">{foreach $article.Tags as $tag}<a class="post-tag tag" href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>{/foreach}<span title="请为本文设置合适的标签" class="post-tag tag c-help">设置Tag是个好习惯</span></p>
</section>
