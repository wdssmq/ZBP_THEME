<nav id="menu" class="navbar">
  <div class="container">
    <!-- <div class="navbar-brand"></div> -->
    <ul class="navbar-menu is-active is-hidden-mobile is-paddingless">
      {module:navbar}
      <li class="navbar-item navbar-end">
        <a href="{$host}feed.php" target="_blank" class="s-icon feed noborder" title="欢迎订阅{$name}" ><span class="hover s-icon feed2"></span></a>
      </li>
    </ul>
  </div>
</nav>
<div class="hero">
  <div class="hero-body is2-paddingless">
    <div class="container">
      <h1 id="blog-title" class="title is-marginless">
        <a href="{$host}" title="{$name}"><span class="is-sr-only">{$name}</span><img src="{$host}zb_users/theme/mzERK/usr/logo.png" alt="{$name}"></a>
      </h1>
      <h2 class="subtitle is-hidden">
        {$subname}
      </h2>
    </div>
  </div>
</div>
<nav id="nav" class="navbar">
  <div class="container">
    <!-- <div class="navbar-brand"></div> -->
    <div class="navbar-burger" data-target="#mzERK-Nav">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <ul class="navbar-menu" id="mzERK-Nav">
      {$modules['mzERK-Nav'].Content}
    </ul>
  </div>
</nav>
