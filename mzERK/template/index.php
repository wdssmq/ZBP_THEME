{* Template Name: 首页及列表页 * Template Type: index|list *}
<?php
// ----------------------------------------------------------------
// [3楼：不要跨文件闭合HTML标签]
// https://bbs.zblogcn.com/thread-101310.html#484040
// ----------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="{$lang['lang_bcp47']}">
{template:header}
<body class="{$type}">
{template:hero}
<!-- <section class="section"> -->
<div class="container">
  <div class="columns">
    <main class="column is-three-quarters col-70">
      {if count($articles) == 0}
        <div class="box has-text-centered">
          <h2 class="is-size-4-desktop">
            <span>当前列表为空 - {$name}</span>
          </h2>
          <p class="has-text-centered">
            <img src="{$host}zb_users/theme/{$theme}/var/moe-piu.png" alt="moe-piu" width="236" height="236" />
          </p>
          <h3 class="is-size-5-desktop">
            <a href="{$host}" title="返回首页">返回首页</a>
            <span>或者</span>
            <a href="javascript:;" onclick="history.go(-1);" title="返回上页">返回上页</a>
          </h3>
        </div>
      {else}
        {foreach $articles as $article}
        {if $article.IsTop && $page=='1'}
          {template:post-istop}
        {else}
          {template:post-multi}
        {/if}
        {/foreach}
        {template:pagebar}
      {/if}
    </main>
    <aside class="column is-one-quarter col-30">{template:sidebar}</aside>
  </div>
</div>
<!-- </section> -->
{template:footer}
</body>
</html>
