<!DOCTYPE html>
<html lang="{$lang['lang_bcp47']}">
{template:header}

<body class="{$type}">
  {template:hero}
  <div class="container">
    <div class="columns">
      <main class="column is-three-quarters col-70">
        {if $article.Type==ZC_POST_TYPE_ARTICLE}
        {template:post-single}
        {else}
        {template:post-page}
        {/if}
      </main>
      <aside class="column is-one-quarter col-30">{template:sidebar}</aside>
    </div>
  </div>
  {template:footer}
</body>

</html>
