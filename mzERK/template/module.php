<section class="mod box {$module.FileName}" id="{$module.HtmlID}">
<!-- {$module.FileName} -->
{if (!$module.IsHideTitle)&&($module.Name)&&($module.FileName!=='searchpanel')}
<h3 class="mod-hd">{$module.Name}</h3>
{else}
<h3 class="mod-hd is-hidden">{$module.Name}</h3>
{/if}
{if $module.Type=='div'}
  <div class="mod-bd">
  {if $module.FileName=='searchpanel'}
    {template:module-searchpanel}
  {else}
    {$module.Content}
  {/if}
  </div>
{else}
  <ul class="mod-bd noborder">
    {$module.Content}
  </ul>
{/if}
</section>
