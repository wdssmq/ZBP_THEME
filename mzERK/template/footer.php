<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p class="a-ccc">{$copyright} | Powered By <a href="http://www.zblogcn.com/" rel="nofollow" title="Powered By Z-BlogPHP" target="_blank">Z-BlogPHP</a></p>
      <!-- 短一点可以像下边这样，都是可以的，自己根据主题调整排版样式 -->
      <!-- 也可以额外加上主题的声明信息：Theme By …… -->
      <!--
        <p class="a-ccc">{$copyright} | Powered By {$zblogphphtml}</p>
      -->
    </div>
  </div>
</footer>
<!-- <button class="backTop" title="回到顶部" type="button" style="display: block;"><svg fill="currentColor" viewBox="0 0 24 24" width="24" height="24"><path d="M16.036 19.59a1 1 0 0 1-.997.995H9.032a.996.996 0 0 1-.997-.996v-7.005H5.03c-1.1 0-1.36-.633-.578-1.416L11.33 4.29a1.003 1.003 0 0 1 1.412 0l6.878 6.88c.782.78.523 1.415-.58 1.415h-3.004v7.005z"></path></svg></button> -->
<div id="fixed-box" class="{$type}">
  <a class="home" href="{$host}" title="回到首页"><span class="is-sr-only">回到首页</span></a>
  <a class="comment" href="#comment" title="发表评论"><span class="is-sr-only">发表评论</span></a>
  <a class="backTop" href="javascript:;" title="回到顶部"><span class="is-sr-only">回到顶部</span></a>
</div>
<script src="{$host}zb_system/script/jquery-2.2.4.min.js?v=16437287"></script>
<script src="{$host}zb_system/script/zblogphp.js?v=16437287"></script>
<script src="{$host}zb_system/script/c_html_js_add.php"></script>
<script src="{$host}zb_users/theme/mzERK/script/script.min.js?v=16437287"></script>
{$footer}
<!--主题开发者不要省$footer,$header同理-->
<?php
// ----------------------------------------------------------------
// [3楼：不要跨文件闭合HTML标签]
// https://bbs.zblogcn.com/thread-101310.html#484040
// ----------------------------------------------------------------
?>
