{if $socialcomment}
{$socialcomment}
{else}
<div class="comments">
  <label id="AjaxCommentBegin"></label>
  {if $article.CommNums>0}
  <!--评论输出-->
  {foreach $comments as $key => $comment}{template:comment}{/foreach}
  {/if}
  <label id="AjaxCommentEnd"></label>
</div>
<!--评论翻页条输出-->
<div class="cmt-pagebar-box">
{template:pagebar}
</div>
<!--评论框-->
{template:comment-post}
{/if}
