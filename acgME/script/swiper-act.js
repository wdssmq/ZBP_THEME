function temputre(t) {
  let e = 0;
  switch (t) {
    case 0:
      e = 0;
      break;
    case 1:
      e = 81;
      break;
    case 2:
      e = 162;
      break;
    case 3:
      e = 243;
      break;
    case 4:
      e = 324;
  }
  $(".tempeture span").css("top", e);
}
$(document).ready(function() {
  // return;
  let t = $(".gallery-left .swiper-slide"),
    e = new Swiper(".gallery-right", {
      autoplay: 5e3,
      mode: "vertical",
      loop: !0,
      onSlideChangeStart: function(e) {
        let a = e.activeIndex - 1;
        (a = 5 == a ? 0 : a),
        t.removeClass("swiper-slide-active"),
        t.eq(a).addClass("swiper-slide-active"),
        temputre(a);
        let tx = $(".gallery-right .swiper-slide-active img");
        if (tx.hasClass("loading")) {
          tx.attr("src", tx.data("original")).removeClass("loading");
          return;
        }
      },
    });
  t.hover(
    function() {
      let t = $(this).index() + 1;
      e.slideTo(t, 100), e.stopAutoplay();
    },
    function() {
      e.startAutoplay();
    },
  );
});
