<?php
#注册插件
RegisterPlugin("acgME", "ActivePlugin_acgME");

if (!class_exists("Thumb")) {
  class Thumb
  {
    static function Thumbs()
    {
      return func_get_arg(0);
    }
  }
}

function ActivePlugin_acgME()
{
  Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu', 'acgME_AddMenu');
  Add_Filter_Plugin('Filter_Plugin_Post_Thumbs', 'acgME_Thumbs');
  // Add_Filter_Plugin('Filter_Plugin_ViewPost_Template', 'acgME_Main');
  Add_Filter_Plugin('Filter_Plugin_ViewList_Template', 'acgME_Main');
  Add_Filter_Plugin('Filter_Plugin_Zbp_BuildTemplate', 'acgME_BuidTemp');
  Add_Filter_Plugin('Filter_Plugin_Cmd_Ajax', 'acgME_Ajax');
  Add_Filter_Plugin('Filter_Plugin_ViewComment_Template', 'acgME_ViewCMT');
  Add_Filter_Plugin('Filter_Plugin_ViewList_Core', 'acgME_canonical');
}
function acgME_canonical()
{
  global $zbp;
  $fpargs = func_get_args();
  $url = $fpargs[7]->UrlRule;
  $zbp->fullcurrenturl = $url->Make();
}
function acgME_Creatdir($path)
{
  if (!is_dir($path)) {
    if (acgME_Creatdir(dirname($path))) {
      @mkdir($path, 0755);
      return true;
    }
  } else {
    return true;
  }
}
function acgME_ViewCMT(&$template)
{
  global $zbp;
  $type = $template->GetTags('type');
  // $comments = $template->GetTags('comments');
  $comment = $template->GetTags('comment');
  if ($comment->ParentID > 0) {
    $commentParent = $zbp->GetCommentByID($comment->ParentID);
    $template->SetTags('MizuPaN', array($comment->ParentID => $commentParent->Author->StaticName));
    $template->SetTemplate("comment-reply");
  }
}
function acgME_AddMenu(&$m)
{
  global $zbp;
  $m[] = MakeTopMenu("root", '主题配置', $zbp->host . "zb_users/theme/acgME/main.php", "", "acgME");
}
function acgME_Main(&$template)
{
  global $zbp;
  if ($tag = $template->GetTags('articles')) {
    foreach ($tag as $article) {
      acgME_SetIMG($article);
    }
  }
  // else if ($article = $template->GetTags('article')) {
  //   acgME_SetIMG($article);
  // }
}
function acgME_BuidTemp(&$templates)
{
  global $zbp;
  // 幻灯片
  $templates['n-slide'] = "";
  if (!$zbp->template->HasTemplate("m-slide")) {
    return;
  }

  $uFile = acgME_Path("u-slide");
  if (!is_file($uFile)) {
    return;
  }
  $slides = json_decode(file_get_contents($uFile));
  foreach ($slides as $key => &$slide) {
    $slide->Img = str_replace('{$host}', $zbp->host, $slide->Img);
    $slide->Img_142x80 = Thumb::Thumbs([$slide->Img], 142, 80, 1, 0)[0];
    $slide->Img_647x404 = Thumb::Thumbs([$slide->Img], 647, 404, 1, 0)[0];
  }
  $zbp->template->SetTags('slides', $slides);
  $templates['n-slide'] = $zbp->template->Output("m-slide");
  $templates['n-slide'] .= '{php}$footer .= \'<script src="' . $zbp->host . 'zb_users/theme/acgME/script/swiper-3.4.2.jquery.min.js"></script>\'{/php}';
  $templates['n-slide'] .= '{php}$footer .= \'<script src="' . $zbp->host . 'zb_users/theme/acgME/script/swiper-act.js"></script>\'{/php}';
}
function acgME_Ajax($src)
{
  global $zbp;
  if ($src === "acgME") {
    $q = GetVars('q', 'GET');
    if (empty($q)) {
      $q = "acgME";
    }
    $w = array();
    $w[] = array('search', 'log_Content', 'log_Intro', 'log_Title', $q);

    $articles = $zbp->GetArticleList(
      '',
      $w,
      array('log_PostTime' => 'DESC'),
      10
    );
    $WhiteList = array(
      'ID' => null,
      'Title' => null,
      'Url' => null,
      'Img' => null
    );
    $objResult = array();
    foreach ($articles as $article) {
      acgME_SetIMG($article);
      // $objResult[]= $article->GetData();
      $objResult[$article->ID] = array_intersect_key($article->GetData(), $WhiteList);
      $objResult[$article->ID]["Url"] = $article->Url;
    }
    JsonReturn($objResult);
  }
}
function acgME_Thumbs($article, &$all_images, $width, $height, $count, $clip)
{
  $rndNum = $article->ID % 19 + 1;
  $rndImg = acgME_Path("v-noimg", "host") . $rndNum . ".jpg";
  // 远程图片失败时
  Thumb::changeDefaultImg($rndImg);
  // 正文内没有图片时
  $all_images[] = $rndImg;
}
function acgME_SetIMG(&$article)
{
  if (!isset($article->Img_640x360)) {
    $article->Img_640x360 = $article->Thumbs(640, 360, 1, 1)[0];
  }
}
function acgME_Path($file, $t = "path")
{
  global $zbp;
  $result = $zbp->$t . "zb_users/theme/acgME/";
  switch ($file) {
    case "u-slide":
      return $result . "usr/slide.json";
      break;
    case "v-slide":
      return $result . "var/slide.json";
      break;
    case "u-logo":
      return $result . "usr/logo.png";
      break;
    case "v-logo":
      return $result . "var/logo.png";
      break;
    case "u-bg":
      return $result . "usr/bg-body.jpg";
      break;
    case "v-bg":
      return $result . "var/bg-body.jpg";
      break;
    case "u-fav":
      return $result . "usr/favicon.ico";
      break;
    case "v-fav":
      return $result . "var/favicon.ico";
      break;
    case "v-noimg":
      return $result . "var/noimg/";
      break;
    case "main":
      return $result . "main.php";
      break;
    default:
      return $result . $file;
  }
}
function InstallPlugin_acgME()
{
  global $zbp;
  $filesList = array("slide", "logo", "bg", "fav");
  foreach ($filesList as $key => $value) {
    $uFile = acgME_Path("u-{$value}");
    $vFile = acgME_Path("v-{$value}");
    if (!is_file($uFile)) {
      @mkdir(dirname($uFile));
      copy($vFile, $uFile);
    }
  }

  // $favicon = ZBP_PATH . "favicon.ico";
  // if (is_file($favicon)) {
  //   copy($favicon, acgME_Path("u-fav"));
  // }

  // 顺序比较重要
  $zbp->BuildTemplate();
  $zbp->AddBuildModule('tags');
  $zbp->BuildModule();

  if (!$zbp->CheckPlugin('LazyLoad')) {
    return;
  }
  copy(acgME_Path("var/loading.gif"), LazyLoad_Path("u-img"));
  // // copy(acgME_Path("var/loading-big.gif"), LazyLoad_Path("u-big"));
  // $css = ".post-img img, .post-body img, .swiper-slide img";
  // if (strpos($zbp->Config('LazyLoad')->LazyLoadImg, $css) === false) {
  //   $zbp->Config('LazyLoad')->LazyLoadImg = $css;
  //   // $zbp->Config('LazyLoad')->templates = 'post-multi,post-istop';
  // }
  // $zbp->SaveConfig('LazyLoad');
}
// 插件升级时执行
function UpdatePlugin_acgME()
{
  global $zbp;
  $zbp->BuildTemplate();
  $version = $zbp->Config('acgME')->version;
  // 2021-03-17
  if (version_compare($version, "1.3.1") < 0) {
    $zbp->template->LoadTemplates();
    $zbp->template->BuildTemplate();
    $zbp->Config('acgME')->version = "1.3.1";
    $zbp->SaveConfig('acgME');
  }
  // 2021-03-17
  $oldDir = "{$zbp->path}zb_users/upload/mini";
  if (is_dir($oldDir)) {
    $zbp->SetHint("tips", "旧版缩略图文件夹（zb_users/upload/mini/）可直接删除");
  }
}
// 旧版兼容
function acgME_Updated()
{
  UpdatePlugin_acgME();
}
function UninstallPlugin_acgME()
{
}
