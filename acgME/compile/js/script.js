$(function() {
  $(".navbar-menu li").each(function() {
    $(this).addClass("navbar-item");
  });
  $(".navbar-burger").click(function() {
    $(".navbar-menu").toggleClass("is-active");
    $(this).toggleClass("is-active");
  });
  let t1, t2;
  $("<ul></ul>")
    .appendTo("body")
    .addClass("sub-menu box");
  $(".navbar-menu li,ul.sub-menu")
    .mouseenter(function() {
      let k = this;
      let subUL = $(k).find("ul");
      $(k)
        .siblings()
        .removeClass("is-active");
      if ($(k).hasClass("sub-menu") || subUL.length > 0) {
        clearTimeout(t2);
      }
      if (subUL.length === 0) {
        return;
      }
      if ($(k).hasClass("is-active") && $("ul.sub-menu").hasClass("is-active"))
        return;
      $("ul.sub-menu").html(subUL.html());
      t1 = setTimeout(function() {
        $(k).addClass("is-active");
        $("ul.sub-menu")
          .addClass("is-active")
          .css({
            top: $(k).offset().top + $(k).outerHeight(),
            left: $(k).offset().left,
            // top: a.pageY + 10 + "px",
            // left: a.pageX + "px"
          });
      }, 150);
    })
    .mouseleave(function() {
      clearTimeout(t1);
      if (!$(this).hasClass("is-active")) return;
      t2 = setTimeout(function() {
        $(".navbar-menu li.is-active,ul.sub-menu").removeClass("is-active");
      }, 350);
    });

  $("#tbCalendar td").hover(
    function() {
      $(this)
        .text()
        .trim() !== "" &&
        $(this).css({
          "box-shadow":
            "1px 3px 4px rgba(10, 10, 10, 0.1),0 0 3px 2px rgba(10, 10, 10, 0.1)",
          "border-radius": "3px",
          "background-color": "#ffffff",
          opacity: "0.5",
        });
    },
    function() {
      $(this).attr("style", "");
    },
  );
  $(".footer .mod")
    .removeClass("box")
    .addClass("column");
  let $sidebar = $("aside.column .mod:last-of-type"),
    offsetTop = 0,
    // copyTop = 0,
    scrollt = 0;
  $(window).scroll(function() {
    if ($sidebar.length === 0) return;
    scrollt = $(window).scrollTop();
    if ($(window).width() < 768) return false;
    if (offsetTop == 0) {
      offsetTop = $sidebar.offset().top;
    }
    let copyTopN = $(".footer").offset().top;
    if (scrollt + $sidebar.outerHeight() > copyTopN - 137) {
      let marTop = parseInt($sidebar.css("marginTop"));
      let rdyTop = copyTopN - offsetTop - $sidebar.outerHeight() - 137;
      $sidebar.stop().animate({
        marginTop: marTop > rdyTop ? marTop : rdyTop,
      });
      return false;
    }
    if (scrollt > offsetTop) {
      $sidebar.stop().animate({
        marginTop: scrollt - offsetTop + 45,
      });
      $(".backTop").fadeIn();
    } else {
      $sidebar.stop().animate({
        marginTop: 15,
      });
      $(".backTop").fadeOut();
    }
    return false;
  });
  $(".backTop").click(function() {
    return (
      $("html,body")
        .animate(
          {
            scrollTop: "0px",
          },
          500,
        )
        .fadeIn(),
      !1
    );
  });
  function fnSrollToEL(target) {
    $("body,html").animate(
      {
        scrollTop: $(target).offset().top - 59,
      },
      500,
    );
  }
  // 评论重写
  zbp.plugin.unbind("comment.reply", "system");
  zbp.plugin.on("comment.reply", "MizuJS", function(id) {
    var me = this;
    this.$("#inpRevID").val(id);
    this.$("#cancel-reply")
      .show()
      .bind("click", function() {
        me.$("#inpRevID").val(0);
        me.$(this).hide();
        fnSrollToEL("#cmt" + id);
        // window.location.hash = "#cmt" + id;
        return false;
      });
    fnSrollToEL("#comment");
    // window.location.hash = "#comment";
  });
  zbp.plugin.unbind("comment.postsuccess", "system");
  zbp.plugin.on("comment.postsuccess", "MizuJS", function(
    formData,
    retString,
    textStatus,
    jqXhr,
  ) {
    var $submit = $("#inpId")
      .parent("form")
      .find(":submit");
    $submit
      .removeClass("loading")
      .removeAttr("disabled")
      .val($submit.data("orig"));
    var data = $.parseJSON(retString);
    if (data.err.code !== 0) {
      alert(data.err.msg);
      throw "ERROR - " + data.err.msg;
    }
    if (formData.replyid == "0") {
      this.$(data.data.html).insertAfter("#AjaxCommentBegin");
    } else {
      this.$(data.data.html).insertAfter("#MizuComment" + formData.replyid);
    }
    fnSrollToEL("#cmt" + data.data.ID);
    // location.hash = "#cmt" + data.data.ID;
    this.$("#txaArticle").val("");
    this.$("#inpRevID").val("");
    this.$("#cancel-reply").hide();
    this.userinfo.saveFromHtml();
    this.userinfo.save();
  });
  if ($(".commentpost").length === 0) return;
  let $str = $("#inpName")
    .val()
    .trim();
  if (!$str) {
    zbp.userinfo.output();
  }
});
