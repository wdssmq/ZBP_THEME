{$MizuPaN[$comment.ID]=$comment.Author.StaticName}
{$MizuPaID[$comment.ID]=$comment.ID}
{if !$comment.Author.HomePage}
{$comment.HomePage = "#cmt" . $comment.ID}
{/if}
<div class="box cmt" id="cmt{$comment.ID}">
  <div class="is-clearfix cmt-head">
    <span class="is-pulled-right"><img class="avatar image is-48x48" src="{$comment.Author.Avatar}" alt="{$comment.Author.StaticName}" width="38" height="38" /></span>
    <b class="cmt-name"><a href="{$comment.Author.HomePage}" rel="external nofollow" target="_blank" title="{$comment.Author.StaticName}">{$comment.Author.StaticName}</a></b><small>&nbsp;发布于&nbsp;{$comment.Time()}&nbsp;&nbsp;</small><br>
    {if isset($comment.Tips)}{$comment.Tips}{else}<a class="cmt-url" href="{$comment.Author.HomePage}" rel="external nofollow noopener noreferrer" target="_blank" title="{$comment.Author.StaticName}">{$comment.Author.HomePage}</a>{/if}
  </div>
  <div class="cmt-body">
    <div class="cmt-content is-clearfix">
      {$comment.Content}
      <span class="cmt-reply is-pulled-right"><a href="javascipt:;" onclick="zbp.comment.reply('{$comment.ID}')" title="回复">回复</a></span>
    </div>
    <div class="cmt-replys">
      <label id="MizuComment{$comment.ID}"></label>
      {foreach $comment.Comments as $comment}{template:comment-reply}{/foreach}
    </div>
  </div>
</div>
