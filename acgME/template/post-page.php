﻿{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
{/php}
<section class="post auth{$article.Author.ID} is-clearfix box content">
  <h2 class="post-title has-text-centered">{$article.Title}</h2>
  <p class="post-meta has-text-grey-light has-text-centered"><a class="has-text-grey-light" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a> | <a class="post-time has-text-grey-light" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$article.Time('Y-m-d')}</a> | <a class="has-text-grey-light" href="{$article.Url}#comment" title="{$article.Title}">{$article.CommNums}条留言</a> | {$article.ViewNums} 次浏览 <span data-id="{$article.ID}" class="js-edt is-pulled-right"></span></p>
  <div class="post-body">{$article.Content}</div>
</section>

{if !$article.IsLock}
{template:comments}
{/if}
