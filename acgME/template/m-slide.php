<div class="slide columns is-gapless">
  <!-- Swiper -->
  <div class="swiper-container gallery-left column is-hidden-touch">
    <div class="swiper-wrapper z">
    {foreach $slides as $slide}
      <div class="swiper-slide">
        <img class="loading slideimg" src="{$host}zb_users/theme/acgME/var/loading.gif" data-original="{$slide->Img_142x80}" alt="{$slide.Title}">
      </div>
      {/foreach}
    </div>
    <div class="tempeture"><span></span></div>
  </div>
  <div class="swiper-container gallery-right column">
    <div class="swiper-wrapper">
    {foreach $slides as $slide}
      <div class="swiper-slide">
      <a class="has-text-centered" href="{$slide->Url}" title="{$slide->Title}" target="_blank">
        <img class="loading" src="{$host}zb_users/theme/acgME/var/loading-big.gif" data-original="{$slide->Img_647x404}" alt="{$slide.Title}">
        <span>{$slide.Title}</span>
      </a>
      </div>
    {/foreach}
    </div>
  </div>
</div>
