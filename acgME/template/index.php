<!DOCTYPE html>
<html lang="{$language}" xml:lang="{$language}">
{template:header}
<body class="{$type}">
<!-- <.container> -->
<div class="container">
  {template:hero}
  <div class="columns">
    <div class="column is-three-quarters col-70">
      {if $type=='index'&&$page=='1'}
      <div class="maincon box">
      {template:n-slide}
      </div>
      {/if}
      {if count($articles) == 0}
        <div class="box has-text-centered">
          <h2 class="is-size-4-desktop">
            <span>当前列表为空 - {$name}</span>
          </h2>
          <p class="has-text-centered">
            <img src="{$host}zb_users/theme/{$theme}/var/moe-smile.gif" alt="moe-smile" width="236" height="236" />
          </p>
          <h3 class="is-size-5-desktop">
            <a href="{$host}" title="返回首页">返回首页</a>
            <span>或者</span>
            <a href="javascript:;" onclick="history.go(-1);" title="返回上页">返回上页</a>
          </h3>
        </div>
      {else}
        <div class="columns is-multiline">
        {foreach $articles as $article}
        {if $article.IsTop&&$page=='1'}
        {template:post-multi}
        {else}
        {template:post-multi}
        {/if}
        {/foreach}
        </div>
      {/if}
      {template:pagebar}
    </div>
    <div class="column is-one-quarter col-30">{template:sidebar}</div>
  </div>
</div>
<!-- </.container> -->
{template:footer}
</body>
</html>
