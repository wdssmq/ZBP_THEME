<!DOCTYPE html>
<html lang="{$language}" xml:lang="{$language}">
{template:header}
<body class="{$type}">
<!-- <.container> -->
<div class="container">
  {template:hero}
  <div class="columns">
    <article class="column is-three-quarters col-70">
    {if $article.Type==ZC_POST_TYPE_ARTICLE}
    {template:post-single}
    {else}
    {template:post-page}
    {/if}
    </article>
    <aside class="column is-one-quarter col-30">{template:sidebar}</aside>
  </div>
</div>
<!-- </.container> -->
{template:footer}
</body>
</html>
