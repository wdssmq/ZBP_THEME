{if count($article.Tags) > 0}
<div class="multi-wrap column is-one-third">
<section class="post-multi cate{$article.Category.ID}  auth{$article.Author.ID}">
<div class="post-img"><img src="http://test.wdssmq.tk/zbp/zb_users/theme/acgME/usr/318378938584.jpg" alt="「我们大家的河合庄」圣地巡礼地图贩售~"></div>
<h2 class="post-title"><a href="{$article.Url}" rel="bookmark" title="{$article.Title}">{$article.Title}</a></h2>
<p class="post-meta is-clearfix">
{if count($article.Tags) > 0}
{$keys = array_keys($article.Tags)}
{$tag = $article.Tags[$keys[0]]}
<a class="post-tag is-pulled-left" href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>
<span class="post-time is-pulled-right" data-time="{$article.Time('Y-m-d H-i')}">{$article.Time('Y-m-d')}</span>
<a class="post-auth is-pulled-right" href="{$article.Author.Url}" title="{$article.Author.StaticName}"> {$article.Author.StaticName}</a>
{/if}
</p>
</section>   
</div>
{/if}
