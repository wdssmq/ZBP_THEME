<footer class="footer">
  <div class="container">
    <div class="columns">
    {template:sidebar2}{template:sidebar3}
    </div>
    <div class="copyright content has-text-centered">
      <p>{$copyright} | Powered By <a href="http://www.zblogcn.com/" rel="nofollow" title="RainbowSoft Z-BlogPHP" target="_blank">Z-BlogPHP</a> | Theme By <a href="http://www.wdssmq.com" target="_blank" title="置百丈玄冰而崩裂，掷须臾池水而漂摇。" rel="nofollow">沉冰浮水</a></p>
    </div>
  </div>
</footer>
<div class="backTop">
  <img src="{$host}zb_users/theme/acgME/style/img/2top.png" alt="返回顶部">
</div>
<script src="{$host}zb_system/script/jquery-2.2.4.min.js?v=581665353"></script>
<script src="{$host}zb_system/script/zblogphp.js?v=581665353"></script>
<script src="{$host}zb_system/script/c_html_js_add.php"></script>
<script src="{$host}zb_users/theme/acgME/script/script.min.js?v=581665353"></script>
{$footer}
