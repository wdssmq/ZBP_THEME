﻿{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
{/php}
<section class="post cate{$article.Category.ID} auth{$article.Author.ID} content box is-clearfix">
  <h2 class="post-title has-text-centered">{$article.Title}</h2>
  <p class="post-meta has-text-grey-light has-text-centered"><a class="has-text-grey-light" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a> | <a class="post-time has-text-grey-light" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$article.Time('Y-m-d')}</a> | <a class="has-text-grey-light" href="{$article.Category.Url}" title="{$article.Category.Name}">{$article.Category.Name}</a> | <a class="has-text-grey-light" href="{$article.Url}#comment" title="{$article.Title}">{$article.CommNums}条留言</a> | {$article.ViewNums} 次浏览 <span data-id="{$article.ID}" class="js-edt is-pulled-right"></span></p>
  <div class="post-body">{$article.Content}</div>
  <p class="post-meta is-pulled-right">{foreach $article.Tags as $tag}<a class="post-tag" href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>&nbsp;{/foreach}</p>
</section>

<div class="box post-nav is-clearfix">
  {if $article.Prev}
  <div class="is-pulled-left">
    <span class="has-text-grey">上一篇</span>
    <a class="a-color a-shadow" href="{$article.Prev.Url}" title="上一篇 {$article.Prev.Title}">{$article.Prev.Title}</a>
  </div>
  {/if}
  {if $article.Next}
  <div class="is-pulled-right has-text-right">
    <span class="has-text-grey">下一篇</span>
    <a class="a-color a-shadow" href="{$article.Next.Url}" title="下一篇 {$article.Next.Title}">{$article.Next.Title}</a>
  </div>
  {/if}
</div>

<section class="box widget is-hidden">
  <h3 class="posttop">相关文章</h3>
  <ul class="list-unstyled">
    {foreach $article.RelatedList as $related}
      <li><a href="{$related.Url}" title="{$related.Title}">{$related.Title}</a> (<span>{$related.Time('Y-m-d')}</span>)</li>
    {/foreach}
  </ul>
</section>

{if !$article.IsLock}
{template:comments}
{/if}
