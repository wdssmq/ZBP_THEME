<div class="columns">
  <div class="column">
    <header class="hero box">
      <div class="hero-body">
      <h1 class="title none-border"><a href="{$host}" title="{$name}"><img src="{$host}zb_users/theme/acgME/usr/logo.png" alt="{$name}"></a></h1>
      <!-- <h2 class="subtitle is-hidden">{$subname}</h2> -->
      </div>
    </header>
    <nav class="navbar box is-paddingless">
      <div class="navbar-burger">
      <span></span>
      <span></span>
      <span></span>
      </div>
      <ul class="navbar-menu">{module:navbar}</ul>
    </nav>
  </div>
</div>
