{if count($article.Tags) >= 0}
<div class="multi-wrap column is-one-third">
<section class="post-multi box cate-{$article.Category.ID} auth-{$article.Author.ID}">
<div class="post-img"><img src="{$article.Img_640x360}" alt="{$article.Title}"></div>
<h2 class="post-title"><a href="{$article.Url}" rel="bookmark" title="{$article.Title}">{$article.Title}</a></h2>
<p class="post-meta is-clearfix">
{if count($article.Tags) > 0}
{$keys = array_keys($article.Tags)}
{$tag = $article.Tags[$keys[0]]}
<a class="post-tag is-pulled-left" href="{$tag.Url}" title="{$tag.Name}" rel="tag">{$tag.Name}</a>
{/if}
{php}
$url = new UrlRule($zbp->option['ZC_DATE_REGEX']);
$url->Rules['{%date%}'] = $article->Time('Y-m-d');
{/php}
<a class="post-time has-text-grey-light is-pulled-right" href="{$url.Make()}" title="{$article.Time('Y-m-d')}" data-time="{$article.Time('Y-m-d H-i')}">{$article.Time('y-m-d')}</a>
<a class="post-auth has-text-grey-light is-pulled-right" href="{$article.Author.Url}" title="{$article.Author.StaticName}">{$article.Author.StaticName}</a>
</p>
</section>
</div>
{/if}
