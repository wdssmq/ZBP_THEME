<div class="mod box {$module.FileName}" id="{$module.HtmlID}">
{if (!$module.IsHideTitle)&&($module.Name)}<h3 class="mod-hd">{$module.Name}</h3>{/if}
{if $module.Type=='div'}<div class="mod-bd">
{if $module.FileName=='searchpanel'}
{template:module-searchpanel}
{else}
{$module.Content}
{/if}
</div>{/if}
{if $module.Type=='ul'}<ul class="mod-bd">{$module.Content}</ul>{/if}
</div>
<!-- {$module.FileName} -->
