{if $socialcomment}
  {$socialcomment}
{else}
  <div class="comments">
    <label id="AjaxCommentBegin"></label>
  {if $article.CommNums>0}
    <!--评论输出-->
    {foreach $comments as $key => $comment}{template:comment}{/foreach}
    <!--评论翻页条输出-->
    {template:pagebar}
  {/if}
    <label id="AjaxCommentEnd"></label>
  </div>
  <!--评论框-->
  {template:commentpost}
{/if}