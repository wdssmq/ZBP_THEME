<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action = 'root';
if (!$zbp->CheckRights($action)) {
  $zbp->ShowError(6);
  die();
}
if (!$zbp->CheckPlugin('acgME')) {
  $zbp->ShowError(48);
  die();
}
$act = GetVars("act", "GET");
if ($act === "update") {
  $strData = GetVars("data", "POST");
  $objData = json_decode($strData);
  if (!empty($objData))
    file_put_contents(acgME_Path("u-slide"), $strData);
  $zbp->BuildTemplate();
  $zbp->SetHint('good');
  die();
}
$blogtitle = '某不科学的ACG向主题';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';

if ($act === "save") {
  CheckIsRefererValid();
  foreach ($_POST as $key => $value) {
    $zbp->Config("acgME")->$key = $value;
  }
  foreach ($_FILES as $key => $value) {
    if ($_FILES[$key]['error'] === 0) {
      $ext = GetFileExt($_FILES[$key]['name']);
      $extList = "png|jpg|jpeg|ico";
      if (!HasNameInString($extList, $ext)) {
        continue;
      }
      if (is_uploaded_file($_FILES[$key]['tmp_name'])) {
        move_uploaded_file($_FILES[$key]['tmp_name'], acgME_Path("u-{$key}"));
      }
    }
  }
  $zbp->SaveConfig("acgME");
  $zbp->SetHint('good');
  Redirect('./main.php');
}
InstallPlugin_acgME();
$uFile = acgME_Path("u-slide");
$obj = json_decode(file_get_contents($uFile));
foreach ($obj as $k1 => &$v1) {
  foreach ($v1 as $k2 => &$v2) {
    $v2 = str_replace('{$host}', $zbp->host, $v2);
    $v2 = str_replace('{$name}', $zbp->name, $v2);
  }
}
$logo = acgME_Path("u-logo", "host") . "?" . time();
$favicon = acgME_Path("u-fav", "host") . "?" . time();
$bgimg = acgME_Path("u-bg", "host") . "?" . time();
?>
<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle; ?></div>
  <div class="SubMenu"></div>
  <div id="divMain2">
    <div class="content-box">
      <ul class="content-box-tabs">
        <li><a href="#tab1" class="current">基础设置</a></li>
        <li><a href="#tab2">幻灯片</a></li>
      </ul>
      <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
          <form method="post" action="main.php?act=save" enctype="multipart/form-data">
            <table class="tableFull tableBorder">
              <tr>
                <th class="td10">
                  <p><b>配置名称</b></p>
                </th>
                <th>配置选项</th>
                <th>备注</th>
              </tr>
              <tr>
                <td>Logo</td>
                <td>
                  <input type="file" name="logo" size="60">&nbsp;&nbsp;<img src="<?php echo $logo; ?>" alt="logo" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
                </td>
                <td>建议高度100px，宽度不限;示例为新蒂小丸子体</td>
              </tr>
              <tr>
                <td>favicon.ico</td>
                <td>
                  <input type="file" name="fav" size="60">&nbsp;&nbsp;<img src="<?php echo $favicon; ?>" alt="favicon" style="max-height: 30px;width: auto;margin-bottom: -8px;padding: 0;">
                </td>
                <td></td>
              </tr>
              <tr>
                <td>背景图</td>
                <td>
                  <input type="file" name="bg" size="60">&nbsp;&nbsp;<a href="<?php echo $bgimg; ?>" target="_blank" rel="noopener noreferrer">点击查看</a>
                </td>
                <td>参考尺寸 1920 x 833</td>
              </tr>
              <tr>
                <td>关键词</td>
                <td>
                  <?php
                  ZbpForm::text("keywords", $zbp->Config("acgME")->keywords, "89%");
                  ?>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>站点描述</td>
                <td>
                  <?php
                  ZbpForm::textarea("description", $zbp->config("acgME")->description, "89%", "5em");
                  ?>
                </td>
                <td></td>
              </tr>
            </table>
            <input type="hidden" name="csrfToken" value="<?php echo $zbp->GetCSRFToken() ?>">
            <input type="submit" class="button" value="<?php echo $lang['msg']['submit'] ?>" /><a href="javascript:;" onclick="location.reload();">刷新</a>
          </form>
        </div>
        <div class="tab-content" id="tab2">
          <table class="tableFull tableBorder">
            <thead>
              <tr>
                <th style="width:3em;">序列</th>
                <th class="td25">标题</th>
                <th class="tdEdit" style="width:3em;">编辑</th>
                <th class="td25">链接</th>
                <th>图片</th>
              </tr>
            </thead>
            <tbody class="ui-sortable">
              <?php foreach ($obj as $key => $v) { ?>
                <?php $id = $key + 1; ?>
                <tr class="js-tr<?php echo $id; ?>">
                  <td class="tdCenter"><?php echo $id; ?></td>
                  <td><?php ZbpForm::text("Title", $v->Title, "100%"); ?></td>
                  <td class="tdCenter"><a data-id="<?php echo $id; ?>" href="javascript:;" class="js-fill"><img src="../../../zb_system/image/admin/page_edit.png" alt="辅助工具"></a></td>
                  <td><?php ZbpForm::text('Url', $v->Url, '100%'); ?></td>
                  <td><?php ZbpForm::text("Img", $v->Img, "100%"); ?></td>
                </tr>
              <?php
            } ?>
            </tbody>
          </table>
          <p><input id="update" type="button" name="update" class="button" value="更新"> <a href="javascript:;" onclick="location.reload();">刷新</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<style media="screen">
  #search-box {
    position: absolute;
    background-color: rgb(253, 253, 253);
    border: 1px solid rgb(204, 204, 204);
    display: none;
    z-index: 2000;
    margin: 5px 0px 10px;
    border-radius: 5px;
    opacity: 0;
    padding: 11px;
  }

  #search-tip {
    line-height: 1em;
    padding-top: 7px;
  }

  .ui-sortable tr .tdCenter {
    cursor: move;
  }
</style>
<div id="search-box" class="ui-draggable">
  <p style="padding:5px;background:#f0f0f0;text-align:center;">
    <input id="search-inp" name="search-inp" style="width:250px;" type="text" placeholder="可使用ID，标题关键词，文件名……" autocomplete="off"><input type="button" class="button" id="search-btn" value="查询">
    <input type="hidden" value="关闭" class="buttons" onclick="$('#search-box').slideToggle();" />
  </p>
  <div id="search-con">
    <select style="width:100%;" id="search-result" name="search-result" size="7" multiple="multiple"></select>
  </div>
  <div id="search-view"></div>
  <div id="search-tip" style="float:left;">点击空白处并按住可移动,双击关闭</div>
  <div id="search-do" style="float:right;"><input disabled id="search-fill" type="button" value="填充到" class="buttons search-fill" />
    <input style=" width: 3em;" type="text" value="1" id="fill-id" />
  </div>
</div>
<script src="admin/script.js" charset="utf-8"></script>
<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
