<?php
RegisterPlugin("vue_TestType", "ActivePlugin_vue_TestType");

// 挂载接口
function ActivePlugin_vue_TestType()
{
  global $zbp;
  Add_Filter_Plugin('Filter_Plugin_Zbp_BuildTemplate', 'vue_TestType_GenTpl');
  Add_Filter_Plugin('Filter_Plugin_Index_Begin', 'vue_TestType_Display');
  // 判断是否启用跨域
  $allowCORS = $zbp->Config('vue_TestType')->allowCORS;
  if ($allowCORS) {
    Add_Filter_Plugin('Filter_Plugin_API_Pre_Response', 'vue_TestType_CORS');
  }
}

// 接口函数：构建模板
function vue_TestType_GenTpl(&$templates)
{
  global $zbp;
  vue_TestType_GenBinScript();

  $pathDist = vue_TestType_Path("dist", "host");
  $file = vue_TestType_Path("dist/index.html");
  $index = file_get_contents($file);

  $binScript = '<script src="' . vue_TestType_Path("bin/script.js?v=202307101011", "host") . '"></script>';
  $index = str_replace("<!-- binScript -->", $binScript, $index);
  $index = str_replace("{{Title}}", $zbp->name, $index);
  $index = preg_replace('/(src|href)="\//', "$1=\"{$pathDist}", $index);
  $index = preg_replace('/assets\/([^"]+)/', "assets/$1?v=202307101011", $index);

  // 静态资源路径
  $pathMode = $zbp->Config('vue_TestType')->pathMode;
  if ($pathMode === "cookies_path") {
    $index = str_replace($zbp->host, $zbp->cookiespath, $index);
  }

  $templates["index"] = $index;
}

// 接口函数：前台页面拦截输出
function vue_TestType_Display()
{
  global $zbp;
  $break = GetVars("vue_break", "GET");
  if ($break !== null) {
    return;
  }
  $zbp->template->Display("index");
  die();
}

// 接口函数：跨域允许
function vue_TestType_CORS()
{
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
  header('Access-Control-Allow-Headers: Content-Type, Authorization');
}

// 启用时执行
function InstallPlugin_vue_TestType()
{
  global $zbp;
  vue_TestType_GenBinScript();
  $zbp->BuildTemplate();
  // 判断是否启用 API
  if ($zbp->option['ZC_API_ENABLE'] == false) {
    $zbp->SetHint("tips hint_always", "API 功能未启用，vue_TestType 主题需要启用 API 功能才能正常使用。");
  }
}

// 升级时执行
function UpdatePlugin_vue_TestType()
{
  global $zbp;
  vue_TestType_GenBinScript();
  $zbp->BuildTemplate();
}

// 停用时执行
function UninstallPlugin_vue_TestType()
{
  global $zbp;
  $IsActived = $zbp->Config('vue_TestType')->IsActived;

  if (true != $IsActived) {
    return true;
  }

  $zbp->option['ZC_STATIC_MODE'] = $zbp->Config('vue_TestType')->BAK_ZC_STATIC_MODE;
  $zbp->option['ZC_ARTICLE_REGEX'] = $zbp->Config('vue_TestType')->BAK_ZC_ARTICLE_REGEX;
  $zbp->option['ZC_PAGE_REGEX'] = $zbp->Config('vue_TestType')->BAK_ZC_PAGE_REGEX;
  $zbp->option['ZC_INDEX_REGEX'] = $zbp->Config('vue_TestType')->BAK_ZC_INDEX_REGEX;
  $zbp->option['ZC_CATEGORY_REGEX'] = $zbp->Config('vue_TestType')->BAK_ZC_CATEGORY_REGEX;
  $zbp->option['ZC_TAGS_REGEX'] = $zbp->Config('vue_TestType')->BAK_ZC_TAGS_REGEX;
  $zbp->option['ZC_DATE_REGEX'] = $zbp->Config('vue_TestType')->BAK_ZC_DATE_REGEX;
  $zbp->option['ZC_AUTHOR_REGEX'] = $zbp->Config('vue_TestType')->BAK_ZC_AUTHOR_REGEX;
  $zbp->option['ZC_SEARCH_REGEX'] = $zbp->Config('vue_TestType')->BAK_ZC_SEARCH_REGEX;

  $zbp->SaveOption();
}

// ------------------------

// 附加函数：生成 bin/script.js
function vue_TestType_GenBinScript()
{
  global $zbp;
  $dir = vue_TestType_Path("bin");
  if (!is_dir($dir)) {
    @mkdir($dir, 0755);
  }
  $dist = vue_TestType_Path("dist", "host");
  $copyright = str_replace("'", "\'", $zbp->option['ZC_BLOG_COPYRIGHT']);
  $script = <<<EOF
window.bloghost='{$zbp->host}';
window.vuedist='{$dist}';
window.cookiespath='{$zbp->cookiespath}';
window.copyright='{$copyright}';
EOF;
  $file = vue_TestType_Path("bin/script.js");
  file_put_contents($file, $script);
}

// 附加函数：获取路径
function vue_TestType_Path($file, $t = 'path')
{
  global $zbp;
  $result = $zbp->$t . 'zb_users/theme/vue_TestType/';
  switch ($file) {
    case 'dist':
      return $result . 'dist/';
      break;
    case 'bin':
      return $result . 'bin/';
      break;
    case 'main':
      return $result . 'main.php';
      break;
    default:
      return $result . $file;
  }
}

// 附加函数：备份并重写静态路由
function vue_TestType_BakSTATIC()
{
  global $zbp;

  $IsActived = $zbp->Config('vue_TestType')->IsActived;
  if (true == $IsActived) {
    return true;
  }

  $zbp->Config('vue_TestType')->BAK_ZC_STATIC_MODE = $zbp->option['ZC_STATIC_MODE'];
  $zbp->Config('vue_TestType')->BAK_ZC_ARTICLE_REGEX = $zbp->option['ZC_ARTICLE_REGEX'];
  $zbp->Config('vue_TestType')->BAK_ZC_PAGE_REGEX = $zbp->option['ZC_PAGE_REGEX'];
  $zbp->Config('vue_TestType')->BAK_ZC_INDEX_REGEX = $zbp->option['ZC_INDEX_REGEX'];
  $zbp->Config('vue_TestType')->BAK_ZC_CATEGORY_REGEX = $zbp->option['ZC_CATEGORY_REGEX'];
  $zbp->Config('vue_TestType')->BAK_ZC_TAGS_REGEX = $zbp->option['ZC_TAGS_REGEX'];
  $zbp->Config('vue_TestType')->BAK_ZC_DATE_REGEX = $zbp->option['ZC_DATE_REGEX'];
  $zbp->Config('vue_TestType')->BAK_ZC_AUTHOR_REGEX = $zbp->option['ZC_AUTHOR_REGEX'];
  $zbp->Config('vue_TestType')->BAK_ZC_SEARCH_REGEX = $zbp->option['ZC_SEARCH_REGEX'];

  $zbp->option['ZC_STATIC_MODE'] = 'REWRITE';
  $zbp->option['ZC_ARTICLE_REGEX'] = '{%host%}post/{%alias%}.html';
  $zbp->option['ZC_PAGE_REGEX'] = '{%host%}{%alias%}.html';
  $zbp->option['ZC_INDEX_REGEX'] = '{%host%}page/{%page%}/';
  $zbp->option['ZC_CATEGORY_REGEX'] = '{%host%}category/{%alias%}/{%page%}/';
  $zbp->option['ZC_TAGS_REGEX'] = '{%host%}tag/{%alias%}/{%page%}/';
  $zbp->option['ZC_DATE_REGEX'] = '{%host%}date/{%date%}/{%page%}/';
  $zbp->option['ZC_AUTHOR_REGEX'] = '{%host%}author/{%alias%}/{%page%}/';
  $zbp->option['ZC_SEARCH_REGEX'] = '{%host%}?search={%q%}';

  $zbp->Config('vue_TestType')->IsActived = true;
  $zbp->Config('vue_TestType')->Save();

  $zbp->SaveOption();
}
