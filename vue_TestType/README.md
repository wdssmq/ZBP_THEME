## 投喂支持

爱发电：[https://afdian.com/a/wdssmq](https://afdian.com/a/wdssmq "沉冰浮水正在创作和 z-blog 相关或无关的各种有用或没用的代码 | 爱发电")

接受 B 站硬币或发电：[https://space.bilibili.com/44744006/video](https://space.bilibili.com/44744006/video "沉冰浮水的个人空间\_哔哩哔哩\_bilibili")

RSS 订阅：[https://feed.wdssmq.com/](https://feed.wdssmq.com/ "沉冰浮水博客的 RSS 订阅地址") 「[「言说」RSS 是一种态度！！](https://www.wdssmq.com/post/20201231613.html "「言说」RSS 是一种态度！！")」

在更多平台关注我：[https://www.wdssmq.com/guestbook.html#其他出没站点和信息](https://www.wdssmq.com/guestbook.html#%E5%85%B6%E4%BB%96%E5%87%BA%E6%B2%A1%E5%9C%B0%E7%82%B9%E5%92%8C%E4%BF%A1%E6%81%AF "在更多平台关注我")

## 截图

![001.png](<https://app.zblogcn.com/zb_users/upload/2021/07/202107221626945814808750.png> "002.png")

![002.png](<https://app.zblogcn.com/zb_users/upload/2021/07/202107221626945814769544.png> "001.png")

## 更新记录

2023-07-10：增加选项以允许多域名使用；

2023-06-21：手机端触发搜索后收回菜单；

2023-06-19：增加搜索功能；

2023-03-03：启用时检查 API 状态；文章页标题设置；「又过一了年」

2022-03-16：2022 年了，发现自己并没写过完整的分页条功能 Orz；

2021-08-16：调低 Z-BlogPHP 版本要求；

2021-08-11：版权信息获取；

2021-07-30：导航切换显示；

2021-07-28：增加参数以阻止页面中断执行；

2021-07-22：发布；
