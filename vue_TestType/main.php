<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action = 'root';
if (!$zbp->CheckRights($action)) {
  $zbp->ShowError(6);
  die();
}
if (!$zbp->CheckPlugin('vue_TestType')) {
  $zbp->ShowError(48);
  die();
}

$act = GetVars('act', 'GET');
$suc = GetVars('suc', 'GET');
if ($act == 'save') {
  CheckIsRefererValid();
  foreach ($_POST as $key => $val) {
    if (substr($key, 0, 5) == 'read_') {
      continue;
    }
    $zbp->Config('vue_TestType')->$key = trim($val);
  }
  $zbp->SaveConfig('vue_TestType');
  $zbp->BuildTemplate();
  $zbp->SetHint('good');
  Redirect('./main.php' . ($suc == null ? '' : "?act={$suc}"));
}

InstallPlugin_vue_TestType();

$blogtitle = 'vue_TestType';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
?>
<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle; ?></div>
  <div class="SubMenu">
    <a href="main.php" title="首页"><span class="m-left m-now">首页</span></a>
    <?php require "about.php"; ?>
  </div>
  <div id="divMain2">
    <form action="<?php echo BuildSafeURL("main.php?act=save"); ?>" method="post">
      <table width="100%" class="tableBorder">
        <tr>
          <th width="10%">项目</th>
          <th>内容</th>
          <th width="45%">说明</th>
        </tr>
        <tr>
          <td>允许跨域</td>
          <td><?php zbpform::zbradio("allowCORS", $zbp->Config("vue_TestType")->allowCORS); ?></td>
          <td></td>
        </tr>
        <tr>
          <td>静态资源路径</td>
          <td><?php zbpform::select("pathMode", array("full_path" => "完整域名", "cookies_path" => "不带域名"), $zbp->Config("vue_TestType")->pathMode); ?></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td colspan="2"><input type="submit" value="提交" /></td>
        </tr>
      </table>
    </form>
    <p>-----------------------</p>
    <p>vite_zbp_vue_TestType: 基于 Z-BlogPHP API 的博客主题；Vue 3 + Vite；</p>
    <p><a href="https://gitee.com/wdssmq/vite_zbp_vue_TestType" target="_blank" title="vite_zbp_vue_TestType: 基于 Z-BlogPHP API 的博客主题；Vue 3 + Vite；">https://gitee.com/wdssmq/vite_zbp_vue_TestType</a></p>
  </div>
</div>

<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
